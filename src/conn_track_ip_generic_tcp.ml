module type B = sig
  val cur_time_ms : unit -> int64

  module Lookup_table : Lookup_table.S

  type addr

  type tcp_port

  type conn_state =
    | New
    | Established
    | Invalid

  type 'a conn_state_w_key =
    | New of 'a
    | Established of 'a
    | Invalid

  val addr_to_byte_string : addr -> string

  val conn_state_to_conn_state_w_key : 'a -> conn_state -> 'a conn_state_w_key

  type tcp_flag =
    | Syn
    | Ack
    | Syn_and_ack
    | Fin_and_ack
    | No_flags
end

module type S = sig
  type addr

  type tcp_port

  type key

  type conn_state

  type 'a conn_state_w_key

  type tcp_flag =
    | Syn
    | Ack
    | Syn_and_ack
    | Fin_and_ack
    | No_flags

  type tracker

  val make : max_conn:int -> init_size:int -> timeout_ms:int64 -> tracker

  val lookup_conn_state_w_key :
    tracker
    -> reject_new:bool
    -> no_update:bool
    -> src_addr:addr
    -> src_port:tcp_port
    -> dst_addr:addr
    -> dst_port:tcp_port
    -> tcp_flag
    -> key conn_state_w_key
end

module Make (B : B) :
  S
  with type addr := B.addr
   and type tcp_port := B.tcp_port
   and type conn_state := B.conn_state
   and type 'a conn_state_w_key := 'a B.conn_state_w_key
   and type tcp_flag := B.tcp_flag = struct
  include B

  type 'a role_tagged =
    | Initiator of 'a
    | Responder of 'a

  type key =
    { initiator_addr : string
    ; initiator_port : tcp_port
    ; responder_addr : string
    ; responder_port : tcp_port }

  type record =
    { state : conn_state
    ; last_initiator_flag : tcp_flag option
    ; last_responder_flag : tcp_flag option
    ; initiator_finished : bool
    ; responder_finished : bool }

  type tracker = {table : (key, record) Lookup_table.t}

  let make ~(max_conn : int) ~(init_size : int) ~(timeout_ms : int64) : tracker
    =
    { table =
        Lookup_table.create ~timeout_ms:(Some timeout_ms) ~max_size:max_conn
          ~init_size }

  let handle_possibly_established_connection ~(no_update : bool) table key
      record (flag : tcp_flag role_tagged) : key conn_state_w_key =
    let { state = stored_state
        ; last_initiator_flag
        ; last_responder_flag
        ; initiator_finished
        ; responder_finished
        ; _ } =
      record
    in
    match (stored_state, last_initiator_flag, last_responder_flag, flag) with
    (* step 2 of starting handshake *)
    | New, Some Syn, None, Responder Syn_and_ack ->
      if not no_update then
        Lookup_table.add table key
          { state = Established
          ; last_initiator_flag
          ; last_responder_flag = Some Syn_and_ack
          ; initiator_finished
          ; responder_finished };
      Established key
    (* step 3 of starting handshake *)
    | Established, Some Syn, Some Syn_and_ack, Initiator Ack ->
      if not no_update then
        Lookup_table.add table key
          { state = Established
          ; last_initiator_flag = Some Ack
          ; last_responder_flag
          ; initiator_finished
          ; responder_finished };
      Established key
    (* first message after starting handshake, from initiator *)
    | Established, Some Ack, Some Syn_and_ack, Initiator Ack ->
      Established key
    (* first message after starting handshake, from responder *)
    | Established, Some Ack, Some Syn_and_ack, Responder Ack ->
      if not no_update then
        Lookup_table.add table key
          { state = Established
          ; last_initiator_flag
          ; last_responder_flag = Some Ack
          ; initiator_finished
          ; responder_finished };
      Established key
    (* normal conversation *)
    | Established, Some Ack, Some Ack, Initiator Ack
    | Established, Some Ack, Some Ack, Responder Ack ->
      if not no_update then
        Lookup_table.add table key
          { state = stored_state
          ; last_initiator_flag
          ; last_responder_flag
          ; initiator_finished
          ; responder_finished };
      Established key
    (* termination from initiator's side *)
    | Established, Some Ack, Some Ack, Initiator Fin_and_ack ->
      if not no_update then
        Lookup_table.add table key
          { state = stored_state
          ; last_initiator_flag = Some Fin_and_ack
          ; last_responder_flag
          ; initiator_finished
          ; responder_finished };
      Established key
    (* responder can respond with Ack or Fin_and_ack *)
    | Established, Some Fin_and_ack, Some Ack, Responder Ack ->
      if not no_update then
        Lookup_table.add table key
          { state = stored_state
          ; last_initiator_flag
          ; last_responder_flag = Some Ack
          ; initiator_finished = true
          ; responder_finished };
      Established key
    | Established, Some Fin_and_ack, Some Ack, Responder Fin_and_ack ->
      if not no_update then
        Lookup_table.add table key
          { state = stored_state
          ; last_initiator_flag
          ; last_responder_flag = Some Fin_and_ack
          ; initiator_finished = true
          ; responder_finished };
      Established key
    (* termination from responder's side *)
    | Established, Some Ack, Some Ack, Responder Fin_and_ack ->
      if not no_update then
        Lookup_table.add table key
          { state = stored_state
          ; last_initiator_flag
          ; last_responder_flag = Some Fin_and_ack
          ; initiator_finished
          ; responder_finished };
      Established key
    (* initiator can respond with Ack or Fin_and_ack *)
    | Established, Some Ack, Some Fin_and_ack, Initiator Ack ->
      if not no_update then
        Lookup_table.add table key
          { state = stored_state
          ; last_initiator_flag = Some Ack
          ; last_responder_flag
          ; initiator_finished
          ; responder_finished = true };
      Established key
    | Established, Some Ack, Some Fin_and_ack, Initiator Fin_and_ack ->
      if not no_update then
        Lookup_table.add table key
          { state = stored_state
          ; last_initiator_flag = Some Fin_and_ack
          ; last_responder_flag
          ; initiator_finished
          ; responder_finished = true };
      Established key
    (* initiator may send the last Ack *)
    | Established, Some Fin_and_ack, Some Fin_and_ack, Initiator Ack
      when responder_finished ->
      if not no_update then Lookup_table.remove table key;
      Established key
    (* responder may send the last Ack *)
    | Established, Some Fin_and_ack, Some Fin_and_ack, Initiator Ack
      when initiator_finished ->
      if not no_update then Lookup_table.remove table key;
      Established key
    | _ ->
      failwith
        (Printf.sprintf "Test14 %s %s %s %s"
           ( match stored_state with
             | New ->
               "New"
             | Established ->
               "Established"
             | Invalid ->
               "Invalid" )
           ( match last_initiator_flag with
             | None ->
               "None"
             | Some Syn ->
               "Syn"
             | Some Syn_and_ack ->
               "Syn_and_ack"
             | Some Ack ->
               "Ack"
             | Some Fin_and_ack ->
               "Fin_and_ack"
             | Some No_flags ->
               "No_flags" )
           ( match last_responder_flag with
             | None ->
               "None"
             | Some Syn ->
               "Syn"
             | Some Syn_and_ack ->
               "Syn_and_ack"
             | Some Ack ->
               "Ack"
             | Some Fin_and_ack ->
               "Fin_and_ack"
             | Some No_flags ->
               "No_flags" )
           ( match flag with
             | Responder Syn ->
               "R Syn"
             | Responder Syn_and_ack ->
               "R Syn_and_ack"
             | Responder Ack ->
               "R Ack"
             | Responder Fin_and_ack ->
               "R Fin_and_ack"
             | Responder No_flags ->
               "R No_flags"
             | Initiator Syn ->
               "I Syn"
             | Initiator Syn_and_ack ->
               "I Syn_and_ack"
             | Initiator Ack ->
               "I Ack"
             | Initiator Fin_and_ack ->
               "I Fin_and_ack"
             | Initiator No_flags ->
               "I No_flags" ));
      Invalid

  let lookup_conn_state_w_key (tracker : tracker) ~(reject_new : bool)
      ~(no_update : bool) ~(src_addr : addr) ~(src_port : tcp_port)
      ~(dst_addr : addr) ~(dst_port : tcp_port) (flag : tcp_flag) :
    key conn_state_w_key =
    let {table} = tracker in
    let src_addr = addr_to_byte_string src_addr in
    let dst_addr = addr_to_byte_string dst_addr in
    Lookup_table.evict_timed_out table;
    (* check if the pair of addresses and ports are in the database in either direction *)
    let key_src_as_init =
      { initiator_addr = src_addr
      ; initiator_port = src_port
      ; responder_addr = dst_addr
      ; responder_port = dst_port }
    in
    let key_dst_as_init =
      { initiator_addr = dst_addr
      ; initiator_port = dst_port
      ; responder_addr = src_addr
      ; responder_port = src_port }
    in
    match
      ( Lookup_table.find_opt table key_src_as_init
      , Lookup_table.find_opt table key_dst_as_init )
    with
    | None, None -> (
        if (* possibly new *)
          reject_new then Invalid
        else
          match flag with
          (* step 1 of starting handshake *)
          | Syn ->
            if not no_update then
              Lookup_table.add table key_src_as_init
                { state = New
                ; last_initiator_flag = Some Syn
                ; last_responder_flag = None
                ; initiator_finished = false
                ; responder_finished = false };
            New key_src_as_init
          | _ ->
            Invalid )
    | Some record, None ->
      handle_possibly_established_connection ~no_update table key_src_as_init
        record (Initiator flag)
    | None, Some record ->
      handle_possibly_established_connection ~no_update table key_dst_as_init
        record (Responder flag)
    | Some _, Some _ ->
      (* this state should not be reachable, cleanse everything *)
      Lookup_table.remove table key_src_as_init;
      Lookup_table.remove table key_dst_as_init;
      Invalid
end
