module type B = sig
  include Tree_base_extended.S

  type 'a direction =
    | A_to_B of 'a
    | B_to_A of 'a

  type addr

  type port = int
end

module type S = sig
  type side_A_B
end

module Make (B : B) : S = struct
  include B

  type side_A_B =
    { side_A_addr : addr
    ; side_B_addr : addr
    ; side_B_port_start : port
    ; side_B_port_end_exc : port
    ; side_B_port_selector : unit -> port option }

  let make ?(pred : int -> bool = fun _ -> true) ~(side_A_addr : addr)
      ~(side_B_addr : addr) ~(side_B_port_start : port)
      ~(side_B_port_end_exc : port) () : side_A_B =
    let side_B_port_selector =
      Misc_utils.make_random_generator_exc_opt ~pred ~from:side_B_port_start
        side_B_port_end_exc
    in
    { side_A_addr
    ; side_B_addr
    ; side_B_port_start
    ; side_B_port_end_exc
    ; side_B_port_selector }

  let get_side_B_port (t : side_A_B) : port option = t.side_B_port_selector ()
end
