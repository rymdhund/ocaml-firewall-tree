let make_wrap_around_counter_exc ?(from : int = 0) (to_exc : int) : unit -> int
  =
  assert (from < to_exc);
  let upper_bound_exc = to_exc - from in
  let counter = ref 0 in
  fun () ->
    let v = !counter in
    counter := (v + 1) mod upper_bound_exc;
    from + v

let make_random_generator_exc_opt ?(pred : int -> bool = fun _ -> true)
    ?(max_tries : int = 5) ~(from : int) (to_exc : int) : unit -> int option =
  assert (from < to_exc);
  let upper_bound_exc = to_exc - from in
  fun () ->
    let v = ref None in
    for _ = 0 to max_tries - 1 do
      match !v with
      | Some _ ->
        ()
      | None ->
        let x = Random.int upper_bound_exc in
        if pred x then v := Some x
    done;
    !v

let make_random_generator_exc ~(from : int) (to_exc : int) : unit -> int =
  assert (from < to_exc);
  let upper_bound_exc = to_exc - from in
  fun () -> Random.int upper_bound_exc

let make_wrap_around_counter_exc_dynamic_ub ?(from : int = 0)
    (to_exc : unit -> int) : unit -> int =
  let to_exc = to_exc () in
  assert (from < to_exc);
  let upper_bound_exc = to_exc - from in
  let counter = ref 0 in
  fun () ->
    let v = !counter in
    counter := (v + 1) mod upper_bound_exc;
    from + v

let make_random_generator_exc_opt_dynamic_ub
    ?(pred : int -> bool = fun _ -> true) ?(max_tries : int = 5) ~(from : int)
    (to_exc : unit -> int) : unit -> int option =
  fun () ->
  let to_exc = to_exc () in
  assert (from < to_exc);
  let upper_bound_exc = to_exc - from in
  let v = ref None in
  for _ = 0 to max_tries - 1 do
    match !v with
    | Some _ ->
      ()
    | None ->
      let x = Random.int upper_bound_exc in
      if pred x then v := Some x
  done;
  !v

let make_random_generator_exc_dynamic_ub ~(from : int) (to_exc : unit -> int) :
  unit -> int =
  fun () ->
  let to_exc = to_exc () in
  assert (from < to_exc);
  let upper_bound_exc = to_exc - from in
  Random.int upper_bound_exc

let byte_string_to_hex_string (s : string) : string =
  Hex.show (Hex.of_string s)

let byte_string_to_hex_dump_lines ?(prefix_each_line : string = "")
    (s : string) : string list =
  Hex.hexdump_s (Hex.of_string s)
  |> String.split_on_char '\n'
  |> List.filter (fun s -> s <> "")
  |> List.map (fun s -> prefix_each_line ^ s)

let byte_string_to_hex_dump ?(prefix_each_line : string = "") (s : string) :
  string =
  byte_string_to_hex_dump_lines ~prefix_each_line s |> String.concat "\n"

let bool_to_int (b : bool) : int = if b then 1 else 0

let exit_if_not_zero (x : int) : unit = if x <> 0 then exit x

let alist_to_hashtbl (l : ('a * 'b) list) : ('a, 'b) Hashtbl.t =
  let len = List.length l in
  let table = Hashtbl.create ~random:true len in
  List.iter (fun (k, v) -> Hashtbl.add table k v) l;
  table

let swap_alist (l : ('a * 'b) list) : ('b * 'a) list =
  List.map (fun (x, y) -> (y, x)) l

let unwrap_opt (v : 'a option) : 'a =
  match v with Some v -> v | None -> failwith "Value is None"

let list_chunks_of (l : 'a list) ~(length : int) : 'a list list =
  List.map List.rev
    (List.fold_left
       (fun acc v ->
          match acc with
          | [] ->
            [[v]]
          | l :: ll ->
            if List.length l < length then (v :: l) :: ll else [v] :: l :: ll
       )
       [] l)
