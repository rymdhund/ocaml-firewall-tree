module type B = Tree_base.B

include Tree
module Selectors = Selectors
module Modifiers = Modifiers
module Scanners = Scanners
module Addr_utils = Addr_utils

module Mock_tree_base : B = Mock_tree_base
