(*$ #use "src/tree_base_extended.cinaps"
    #use "src/pdu.cinaps"
  $*)
module type S = sig
  type decision

  type netif

  val cur_time_ms : unit -> int64

  type tcp_port = int

  type udp_port = int

  module Ether : sig
    include Tree_base.Ether

    (*$ print_update_header_fun_sig update_header_words.(0);
        print_update_header_byte_string_fun_sig update_header_words.(0)
    *)
    val update_ether_header :
      ?src_addr:ether_addr
      -> ?dst_addr:ether_addr
      -> ether_header
      -> ether_header

    val update_ether_header_byte_string :
      ?src_addr:string -> ?dst_addr:string -> ether_header -> ether_header

    (*$*)
  end

  type 'a subnet_mask =
    | Mask_addr of 'a
    | Mask_bit_count of int

  module IPv4 : sig
    include Tree_base.IPv4

    val ipv4_addr_to_net_addr :
      addr:ipv4_addr -> mask:ipv4_addr subnet_mask -> ipv4_addr

    (*$ print_update_header_fun_sig update_header_words.(1);
        print_update_header_byte_string_fun_sig update_header_words.(1)
    *)
    val update_ipv4_header :
      ?src_addr:ipv4_addr -> ?dst_addr:ipv4_addr -> ipv4_header -> ipv4_header

    val update_ipv4_header_byte_string :
      ?src_addr:string -> ?dst_addr:string -> ipv4_header -> ipv4_header

    (*$*)
  end

  module IPv6 : sig
    include Tree_base.IPv6

    val ipv6_addr_to_net_addr :
      addr:ipv6_addr -> mask:ipv6_addr subnet_mask -> ipv6_addr

    (*$ print_update_header_fun_sig update_header_words.(2);
        print_update_header_byte_string_fun_sig update_header_words.(2)
    *)
    val update_ipv6_header :
      ?src_addr:ipv6_addr -> ?dst_addr:ipv6_addr -> ipv6_header -> ipv6_header

    val update_ipv6_header_byte_string :
      ?src_addr:string -> ?dst_addr:string -> ipv6_header -> ipv6_header

    (*$*)
  end

  module ICMPv4 : sig
    include Tree_base.ICMPv4 with type ipv4_addr := IPv4.ipv4_addr

    (*$ print_update_header_fun_sig update_header_words.(3)
    *)
    val update_icmpv4_header :
      ?icmpv4_type:icmpv4_type -> icmpv4_header -> icmpv4_header

    (*$*)
  end

  module ICMPv6 : sig
    include Tree_base.ICMPv6 with type ipv6_addr := IPv6.ipv6_addr

    (*$ print_update_header_fun_sig update_header_words.(4)
    *)
    val update_icmpv6_header :
      ?icmpv6_type:icmpv6_type -> icmpv6_header -> icmpv6_header

    (*$*)
  end

  module TCP : sig
    include Tree_base.TCP

    (*$ print_update_header_fun_sig update_header_words.(5)
    *)
    val update_tcp_header :
      ?src_port:tcp_port
      -> ?dst_port:tcp_port
      -> ?ack:bool
      -> ?rst:bool
      -> ?syn:bool
      -> ?fin:bool
      -> tcp_header
      -> tcp_header

    (*$*)
  end

  module UDP : sig
    include Tree_base.UDP

    (*$ print_update_header_fun_sig update_header_words.(6)
    *)
    val update_udp_header :
      ?src_port:udp_port -> ?dst_port:udp_port -> udp_header -> udp_header

    (*$*)
  end

  open Ether
  open IPv4
  open IPv6
  open ICMPv4
  open ICMPv6
  open TCP
  open UDP

  module PDU : sig
    (*$ print_type_pdu_def ()
    *)
    type pdu =
      | Layer2 of layer2_pdu
      | Layer3 of layer3_pdu
      | Layer4 of layer4_pdu

    (* OSI Layer 2 *)
    and layer2_pdu = Ether of ether_frame

    and ether_frame =
      | Ether_frame of {header : ether_header; payload : ether_payload}

    and ether_payload =
      | Ether_payload_raw of ether_payload_raw
      | Ether_payload_encap of layer3_pdu

    (* OSI Layer 3 *)
    and layer3_pdu =
      | IPv4 of ipv4_pkt
      | IPv6 of ipv6_pkt

    and ipv4_pkt = IPv4_pkt of {header : ipv4_header; payload : ipv4_payload}

    and ipv6_pkt = IPv6_pkt of {header : ipv6_header; payload : ipv6_payload}

    and icmpv4_pkt =
      | ICMPv4_pkt of {header : icmpv4_header; payload : icmpv4_payload}

    and icmpv6_pkt =
      | ICMPv6_pkt of {header : icmpv6_header; payload : icmpv6_payload}

    and ipv4_payload =
      | IPv4_payload_raw of ipv4_payload_raw
      | IPv4_payload_icmp of icmpv4_pkt
      | IPv4_payload_encap of layer4_pdu

    and ipv6_payload =
      | IPv6_payload_raw of ipv6_payload_raw
      | IPv6_payload_icmp of icmpv6_pkt
      | IPv6_payload_encap of layer4_pdu

    and icmpv4_payload = ICMPv4_payload_raw of icmpv4_payload_raw

    and icmpv6_payload = ICMPv6_payload_raw of icmpv6_payload_raw

    (* OSI Layer 4 *)
    and layer4_pdu =
      | TCP of tcp_pdu
      | UDP of udp_pdu

    and tcp_pdu = TCP_pdu of {header : tcp_header; payload : tcp_payload}

    and udp_pdu = UDP_pdu of {header : udp_header; payload : udp_payload}

    and tcp_payload = TCP_payload_raw of tcp_payload_raw

    and udp_payload = UDP_payload_raw of udp_payload_raw

    (*$*)
  end

  open PDU

  module PDU_to : sig
    val layer2_pdu_to_layer3_pdu : layer2_pdu -> layer3_pdu option

    val layer3_pdu_to_layer4_pdu : layer3_pdu -> layer4_pdu option

    val layer2_pdu : pdu -> layer2_pdu option

    val layer3_pdu : pdu -> layer3_pdu option

    val layer4_pdu : pdu -> layer4_pdu option

    (*$ let words =
        [ "ether", "ether_frame"
        ; "ipv4", "ipv4_pkt"
        ; "ipv6", "ipv6_pkt"
        ; "icmpv4", "icmpv4_pkt"
        ; "icmpv6", "icmpv6_pkt"
        ; "tcp", "tcp_pdu"
        ; "udp", "udp_pdu"
        ]
      in
      List.iter
        (fun (proto, pdu_con) ->
           Printf.printf {|
               val %s : pdu -> %s option
             |}
             pdu_con pdu_con;

           Printf.printf {|
               val %s_header : pdu -> %s_header option
             |}
             proto proto;

           Printf.printf {|
               val %s_payload : pdu -> %s_payload option
             |}
             proto proto;
        )
        words
    *)
    val ether_frame : pdu -> ether_frame option

    val ether_header : pdu -> ether_header option

    val ether_payload : pdu -> ether_payload option

    val ipv4_pkt : pdu -> ipv4_pkt option

    val ipv4_header : pdu -> ipv4_header option

    val ipv4_payload : pdu -> ipv4_payload option

    val ipv6_pkt : pdu -> ipv6_pkt option

    val ipv6_header : pdu -> ipv6_header option

    val ipv6_payload : pdu -> ipv6_payload option

    val icmpv4_pkt : pdu -> icmpv4_pkt option

    val icmpv4_header : pdu -> icmpv4_header option

    val icmpv4_payload : pdu -> icmpv4_payload option

    val icmpv6_pkt : pdu -> icmpv6_pkt option

    val icmpv6_header : pdu -> icmpv6_header option

    val icmpv6_payload : pdu -> icmpv6_payload option

    val tcp_pdu : pdu -> tcp_pdu option

    val tcp_header : pdu -> tcp_header option

    val tcp_payload : pdu -> tcp_payload option

    val udp_pdu : pdu -> udp_pdu option

    val udp_header : pdu -> udp_header option

    val udp_payload : pdu -> udp_payload option

    (*$*)
  end

  module PDU_map : sig
    (*$ let words =
        [ "ether", "ether_frame"
        ; "ipv4", "ipv4_pkt"
        ; "ipv6", "ipv6_pkt"
        ; "icmpv4", "icmpv4_pkt"
        ; "icmpv6", "icmpv6_pkt"
        ; "tcp", "tcp_pdu"
        ; "udp", "udp_pdu"
        ]
      in
      for i=2 to 4 do
        Printf.printf {|
            val layer%d_pdu : (layer%d_pdu -> layer%d_pdu) -> pdu -> pdu
          |}
          i i i
      done;
      List.iter
        (fun (proto, pdu_con) ->
           Printf.printf {|
               val %s : (%s -> %s) -> pdu -> pdu
             |}
             pdu_con pdu_con pdu_con;

           Printf.printf {|
               val %s_header : (%s_header -> %s_header) -> pdu -> pdu
             |}
             proto proto proto;

           Printf.printf {|
               val %s_payload : (%s_payload -> %s_payload) -> pdu -> pdu
             |}
             proto proto proto;
        )
        words;
    *)
    val layer2_pdu : (layer2_pdu -> layer2_pdu) -> pdu -> pdu

    val layer3_pdu : (layer3_pdu -> layer3_pdu) -> pdu -> pdu

    val layer4_pdu : (layer4_pdu -> layer4_pdu) -> pdu -> pdu

    val ether_frame : (ether_frame -> ether_frame) -> pdu -> pdu

    val ether_header : (ether_header -> ether_header) -> pdu -> pdu

    val ether_payload : (ether_payload -> ether_payload) -> pdu -> pdu

    val ipv4_pkt : (ipv4_pkt -> ipv4_pkt) -> pdu -> pdu

    val ipv4_header : (ipv4_header -> ipv4_header) -> pdu -> pdu

    val ipv4_payload : (ipv4_payload -> ipv4_payload) -> pdu -> pdu

    val ipv6_pkt : (ipv6_pkt -> ipv6_pkt) -> pdu -> pdu

    val ipv6_header : (ipv6_header -> ipv6_header) -> pdu -> pdu

    val ipv6_payload : (ipv6_payload -> ipv6_payload) -> pdu -> pdu

    val icmpv4_pkt : (icmpv4_pkt -> icmpv4_pkt) -> pdu -> pdu

    val icmpv4_header : (icmpv4_header -> icmpv4_header) -> pdu -> pdu

    val icmpv4_payload : (icmpv4_payload -> icmpv4_payload) -> pdu -> pdu

    val icmpv6_pkt : (icmpv6_pkt -> icmpv6_pkt) -> pdu -> pdu

    val icmpv6_header : (icmpv6_header -> icmpv6_header) -> pdu -> pdu

    val icmpv6_payload : (icmpv6_payload -> icmpv6_payload) -> pdu -> pdu

    val tcp_pdu : (tcp_pdu -> tcp_pdu) -> pdu -> pdu

    val tcp_header : (tcp_header -> tcp_header) -> pdu -> pdu

    val tcp_payload : (tcp_payload -> tcp_payload) -> pdu -> pdu

    val udp_pdu : (udp_pdu -> udp_pdu) -> pdu -> pdu

    val udp_header : (udp_header -> udp_header) -> pdu -> pdu

    val udp_payload : (udp_payload -> udp_payload) -> pdu -> pdu

    (*$*)
  end

  module PDU_replace : sig
    (*$ let words =
        [ "ether", "ether_frame"
        ; "ipv4", "ipv4_pkt"
        ; "ipv6", "ipv6_pkt"
        ; "icmpv4", "icmpv4_pkt"
        ; "icmpv6", "icmpv6_pkt"
        ; "tcp", "tcp_pdu"
        ; "udp", "udp_pdu"
        ]
      in
      for i=2 to 4 do
        Printf.printf {|
            val layer%d_pdu : layer%d_pdu -> pdu -> pdu
          |}
          i i
      done;
      List.iter
        (fun (proto, pdu_con) ->
           Printf.printf {|
               val %s : %s -> pdu -> pdu
             |}
             pdu_con pdu_con;

           Printf.printf {|
               val %s_header : %s_header -> pdu -> pdu
             |}
             proto proto;

           Printf.printf {|
               val %s_payload : %s_payload -> pdu -> pdu
             |}
             proto proto;
        )
        words;
    *)
    val layer2_pdu : layer2_pdu -> pdu -> pdu

    val layer3_pdu : layer3_pdu -> pdu -> pdu

    val layer4_pdu : layer4_pdu -> pdu -> pdu

    val ether_frame : ether_frame -> pdu -> pdu

    val ether_header : ether_header -> pdu -> pdu

    val ether_payload : ether_payload -> pdu -> pdu

    val ipv4_pkt : ipv4_pkt -> pdu -> pdu

    val ipv4_header : ipv4_header -> pdu -> pdu

    val ipv4_payload : ipv4_payload -> pdu -> pdu

    val ipv6_pkt : ipv6_pkt -> pdu -> pdu

    val ipv6_header : ipv6_header -> pdu -> pdu

    val ipv6_payload : ipv6_payload -> pdu -> pdu

    val icmpv4_pkt : icmpv4_pkt -> pdu -> pdu

    val icmpv4_header : icmpv4_header -> pdu -> pdu

    val icmpv4_payload : icmpv4_payload -> pdu -> pdu

    val icmpv6_pkt : icmpv6_pkt -> pdu -> pdu

    val icmpv6_header : icmpv6_header -> pdu -> pdu

    val icmpv6_payload : icmpv6_payload -> pdu -> pdu

    val tcp_pdu : tcp_pdu -> pdu -> pdu

    val tcp_header : tcp_header -> pdu -> pdu

    val tcp_payload : tcp_payload -> pdu -> pdu

    val udp_pdu : udp_pdu -> pdu -> pdu

    val udp_header : udp_header -> pdu -> pdu

    val udp_payload : udp_payload -> pdu -> pdu

    (*$*)
  end

  module Unsafe_flat_PDU : sig
    type layer2_header = Ether_header of ether_header

    and layer2_data = Ether_data_raw of ether_payload_raw

    and layer3_header =
      | IPv4_header of ipv4_header
      | IPv6_header of ipv6_header

    and layer3_data =
      | IPv4_data_raw of ipv4_payload_raw
      | IPv4_data_icmp of icmpv4_pkt
      | IPv6_data_raw of ipv6_payload_raw
      | IPv6_data_icmp of icmpv6_pkt

    and layer4_header =
      | TCP_header of tcp_header
      | UDP_header of udp_header

    and layer4_data =
      | TCP_data_raw of tcp_payload_raw
      | UDP_data_raw of udp_payload_raw

    type flat_pdu =
      { layer2_header : layer2_header option
      ; layer3_header : layer3_header option
      ; layer4_header : layer4_header option
      ; layer2_data : layer2_data option
      ; layer3_data : layer3_data option
      ; layer4_data : layer4_data option }

    val blank_flat_pdu : flat_pdu

    val flatten : pdu -> flat_pdu

    val unflatten : flat_pdu -> pdu option

    val unflatten_layer2 : flat_pdu -> layer2_pdu option

    val unflatten_layer3 : flat_pdu -> layer3_pdu option

    val unflatten_layer4 : flat_pdu -> layer4_pdu option

    val merge_flat_pdu_use_left_on_conflict : flat_pdu -> flat_pdu -> flat_pdu

    val merge_flat_pdu_use_right_on_conflict : flat_pdu -> flat_pdu -> flat_pdu

    val merge_pdu_use_left_on_conflict : pdu -> pdu -> pdu

    val merge_pdu_use_right_on_conflict : pdu -> pdu -> pdu

    val resolve_encap_issue_prefer_raw : flat_pdu -> flat_pdu

    val resolve_encap_issue_prefer_encap : flat_pdu -> flat_pdu
  end

  module To_debug_string : sig
    (*$ let words =
          [ "ether_addr"
          ; "ipv4_addr"
          ; "ipv6_addr"
          ; "tcp_port"
          ; "udp_port"
          ; "ether_header"
          ; "ether_payload"
          ; "ether_frame"
          ; "layer2_pdu"
          ; "ipv4_header"
          ; "ipv4_payload"
          ; "ipv4_pkt"
          ; "ipv6_header"
          ; "ipv6_payload"
          ; "ipv6_pkt"
          ; "icmpv4_type"
          ; "icmpv4_header"
          ; "icmpv4_payload"
          ; "icmpv4_pkt"
          ; "icmpv6_type"
          ; "icmpv6_header"
          ; "icmpv6_payload"
          ; "icmpv6_pkt"
          ; "layer3_pdu"
          ; "tcp_header"
          ; "tcp_payload"
          ; "tcp_pdu"
          ; "udp_header"
          ; "udp_payload"
          ; "udp_pdu"
          ; "layer4_pdu"
          ]
        in
        List.iter
          (fun ty ->
             Printf.printf {|
                 val %s : %s -> string
               |}
               ty ty
          )
          words
    *)
    val ether_addr : ether_addr -> string

    val ipv4_addr : ipv4_addr -> string

    val ipv6_addr : ipv6_addr -> string

    val tcp_port : tcp_port -> string

    val udp_port : udp_port -> string

    val ether_header : ether_header -> string

    val ether_payload : ether_payload -> string

    val ether_frame : ether_frame -> string

    val layer2_pdu : layer2_pdu -> string

    val ipv4_header : ipv4_header -> string

    val ipv4_payload : ipv4_payload -> string

    val ipv4_pkt : ipv4_pkt -> string

    val ipv6_header : ipv6_header -> string

    val ipv6_payload : ipv6_payload -> string

    val ipv6_pkt : ipv6_pkt -> string

    val icmpv4_type : icmpv4_type -> string

    val icmpv4_header : icmpv4_header -> string

    val icmpv4_payload : icmpv4_payload -> string

    val icmpv4_pkt : icmpv4_pkt -> string

    val icmpv6_type : icmpv6_type -> string

    val icmpv6_header : icmpv6_header -> string

    val icmpv6_payload : icmpv6_payload -> string

    val icmpv6_pkt : icmpv6_pkt -> string

    val layer3_pdu : layer3_pdu -> string

    val tcp_header : tcp_header -> string

    val tcp_payload : tcp_payload -> string

    val tcp_pdu : tcp_pdu -> string

    val udp_header : udp_header -> string

    val udp_payload : udp_payload -> string

    val udp_pdu : udp_pdu -> string

    val layer4_pdu : layer4_pdu -> string

    (*$*)
    val flat_pdu : Unsafe_flat_PDU.flat_pdu -> string

    val pdu : pdu -> string
  end

  module Print_debug : sig
    val print_flat_pdu_debug : Unsafe_flat_PDU.flat_pdu -> unit

    val print_pdu_debug : pdu -> unit
  end

  module Lookup_table : Lookup_table.S
end

module Make (B : Tree_base.B) :
  S
  (*$ print_tree_base_extended_type_constraints ()
  *)
  with type decision = B.decision
   and type netif = B.netif
   and type Ether.ether_addr = B.Ether.ether_addr
   and type Ether.ether_header = B.Ether.ether_header
   and type Ether.ether_payload_raw = B.Ether.ether_payload_raw
   and type IPv4.ipv4_addr = B.IPv4.ipv4_addr
   and type IPv4.ipv4_header = B.IPv4.ipv4_header
   and type IPv4.ipv4_payload_raw = B.IPv4.ipv4_payload_raw
   and type IPv6.ipv6_addr = B.IPv6.ipv6_addr
   and type IPv6.ipv6_header = B.IPv6.ipv6_header
   and type IPv6.ipv6_payload_raw = B.IPv6.ipv6_payload_raw
   and type ICMPv4.icmpv4_type = B.ICMPv4.icmpv4_type
   and type ICMPv4.icmpv4_header = B.ICMPv4.icmpv4_header
   and type ICMPv4.icmpv4_payload_raw = B.ICMPv4.icmpv4_payload_raw
   and type ICMPv6.icmpv6_type = B.ICMPv6.icmpv6_type
   and type ICMPv6.icmpv6_header = B.ICMPv6.icmpv6_header
   and type ICMPv6.icmpv6_payload_raw = B.ICMPv6.icmpv6_payload_raw
   and type TCP.tcp_header = B.TCP.tcp_header
   and type TCP.tcp_payload_raw = B.TCP.tcp_payload_raw
   and type UDP.udp_header = B.UDP.udp_header
   and type UDP.udp_payload_raw = B.UDP.udp_payload_raw (*$*) = struct
  type decision = B.decision

  type netif = B.netif

  let cur_time_ms = B.cur_time_ms

  type tcp_port = int

  type udp_port = int

  module Ether = struct
    include B.Ether

    (*$ print_update_header_fun_def update_header_words.(0)
    *)
    let update_ether_header ?(src_addr : ether_addr option)
        ?(dst_addr : ether_addr option) (header : ether_header) : ether_header
      =
      update_ether_header_ ~src_addr ~dst_addr header

    (*$*)

    (*$ print_update_header_byte_string_fun_def update_header_words.(0) *)
    let update_ether_header_byte_string ?(src_addr : string option)
        ?(dst_addr : string option) (header : ether_header) : ether_header =
      update_ether_header_byte_string_ ~src_addr ~dst_addr header

    (*$*)
  end

  type 'a subnet_mask =
    | Mask_addr of 'a
    | Mask_bit_count of int

  module IPv4 = struct
    include B.IPv4

    (*$ let words =
          [ "ipv4"
          ]
        in
        List.iter
          (fun proto ->
             Printf.printf {|
                 let %s_addr_to_net_addr ~(addr : %s_addr) ~(mask : %s_addr subnet_mask) : %s_addr =
                   let addr = %s_addr_to_byte_string addr in
                   match mask with
                   | Mask_addr mask ->
                     let mask = %s_addr_to_byte_string mask in
                     addr
                     |> Addr_utils.string_and mask
                     |> byte_string_to_%s_addr
                   | Mask_bit_count n ->
                     addr
                     |> Addr_utils.string_mask_first_n_bits n
                     |> byte_string_to_%s_addr
               |}
               proto proto proto proto
               proto
               proto
               proto
               proto
          )
          words;

        print_update_header_fun_def update_header_words.(1);
        print_update_header_byte_string_fun_def update_header_words.(1)
    *)
    let ipv4_addr_to_net_addr ~(addr : ipv4_addr)
        ~(mask : ipv4_addr subnet_mask) : ipv4_addr =
      let addr = ipv4_addr_to_byte_string addr in
      match mask with
      | Mask_addr mask ->
        let mask = ipv4_addr_to_byte_string mask in
        addr |> Addr_utils.string_and mask |> byte_string_to_ipv4_addr
      | Mask_bit_count n ->
        addr
        |> Addr_utils.string_mask_first_n_bits n
        |> byte_string_to_ipv4_addr

    let update_ipv4_header ?(src_addr : ipv4_addr option)
        ?(dst_addr : ipv4_addr option) (header : ipv4_header) : ipv4_header =
      update_ipv4_header_ ~src_addr ~dst_addr header

    let update_ipv4_header_byte_string ?(src_addr : string option)
        ?(dst_addr : string option) (header : ipv4_header) : ipv4_header =
      update_ipv4_header_byte_string_ ~src_addr ~dst_addr header

    (*$*)
  end

  module IPv6 = struct
    include B.IPv6

    (*$ let words =
          [ "ipv6"
          ]
        in
        List.iter
          (fun proto ->
             Printf.printf {|
                 let %s_addr_to_net_addr ~(addr : %s_addr) ~(mask : %s_addr subnet_mask) : %s_addr =
                   let addr = %s_addr_to_byte_string addr in
                   match mask with
                   | Mask_addr mask ->
                     let mask = %s_addr_to_byte_string mask in
                     addr
                     |> Addr_utils.string_and mask
                     |> byte_string_to_%s_addr
                   | Mask_bit_count n ->
                     addr
                     |> Addr_utils.string_mask_first_n_bits n
                     |> byte_string_to_%s_addr
               |}
               proto proto proto proto
               proto
               proto
               proto
               proto
          )
          words;

        print_update_header_fun_def update_header_words.(2);
        print_update_header_byte_string_fun_def update_header_words.(2)
    *)
    let ipv6_addr_to_net_addr ~(addr : ipv6_addr)
        ~(mask : ipv6_addr subnet_mask) : ipv6_addr =
      let addr = ipv6_addr_to_byte_string addr in
      match mask with
      | Mask_addr mask ->
        let mask = ipv6_addr_to_byte_string mask in
        addr |> Addr_utils.string_and mask |> byte_string_to_ipv6_addr
      | Mask_bit_count n ->
        addr
        |> Addr_utils.string_mask_first_n_bits n
        |> byte_string_to_ipv6_addr

    let update_ipv6_header ?(src_addr : ipv6_addr option)
        ?(dst_addr : ipv6_addr option) (header : ipv6_header) : ipv6_header =
      update_ipv6_header_ ~src_addr ~dst_addr header

    let update_ipv6_header_byte_string ?(src_addr : string option)
        ?(dst_addr : string option) (header : ipv6_header) : ipv6_header =
      update_ipv6_header_byte_string_ ~src_addr ~dst_addr header

    (*$*)
  end

  module ICMPv4 = struct
    include B.ICMPv4

    (*$ print_update_header_fun_def update_header_words.(3)
    *)
    let update_icmpv4_header ?(icmpv4_type : icmpv4_type option)
        (header : icmpv4_header) : icmpv4_header =
      update_icmpv4_header_ icmpv4_type header

    (*$*)
  end

  module ICMPv6 = struct
    include B.ICMPv6

    (*$ print_update_header_fun_def update_header_words.(4)
    *)
    let update_icmpv6_header ?(icmpv6_type : icmpv6_type option)
        (header : icmpv6_header) : icmpv6_header =
      update_icmpv6_header_ icmpv6_type header

    (*$*)
  end

  module TCP = struct
    include B.TCP

    (*$ print_update_header_fun_def update_header_words.(5)
    *)
    let update_tcp_header ?(src_port : tcp_port option)
        ?(dst_port : tcp_port option) ?(ack : bool option) ?(rst : bool option)
        ?(syn : bool option) ?(fin : bool option) (header : tcp_header) :
      tcp_header =
      update_tcp_header_ ~src_port ~dst_port ~ack ~rst ~syn ~fin header

    (*$*)
  end

  module UDP = struct
    include B.UDP

    (*$ print_update_header_fun_def update_header_words.(6)
    *)
    let update_udp_header ?(src_port : udp_port option)
        ?(dst_port : udp_port option) (header : udp_header) : udp_header =
      update_udp_header_ ~src_port ~dst_port header

    (*$*)
  end

  open Ether
  open IPv4
  open IPv6
  open ICMPv4
  open ICMPv6
  open TCP
  open UDP

  module PDU = struct
    (*$ print_type_pdu_def ()
    *)
    type pdu =
      | Layer2 of layer2_pdu
      | Layer3 of layer3_pdu
      | Layer4 of layer4_pdu

    (* OSI Layer 2 *)
    and layer2_pdu = Ether of ether_frame

    and ether_frame =
      | Ether_frame of {header : ether_header; payload : ether_payload}

    and ether_payload =
      | Ether_payload_raw of ether_payload_raw
      | Ether_payload_encap of layer3_pdu

    (* OSI Layer 3 *)
    and layer3_pdu =
      | IPv4 of ipv4_pkt
      | IPv6 of ipv6_pkt

    and ipv4_pkt = IPv4_pkt of {header : ipv4_header; payload : ipv4_payload}

    and ipv6_pkt = IPv6_pkt of {header : ipv6_header; payload : ipv6_payload}

    and icmpv4_pkt =
      | ICMPv4_pkt of {header : icmpv4_header; payload : icmpv4_payload}

    and icmpv6_pkt =
      | ICMPv6_pkt of {header : icmpv6_header; payload : icmpv6_payload}

    and ipv4_payload =
      | IPv4_payload_raw of ipv4_payload_raw
      | IPv4_payload_icmp of icmpv4_pkt
      | IPv4_payload_encap of layer4_pdu

    and ipv6_payload =
      | IPv6_payload_raw of ipv6_payload_raw
      | IPv6_payload_icmp of icmpv6_pkt
      | IPv6_payload_encap of layer4_pdu

    and icmpv4_payload = ICMPv4_payload_raw of icmpv4_payload_raw

    and icmpv6_payload = ICMPv6_payload_raw of icmpv6_payload_raw

    (* OSI Layer 4 *)
    and layer4_pdu =
      | TCP of tcp_pdu
      | UDP of udp_pdu

    and tcp_pdu = TCP_pdu of {header : tcp_header; payload : tcp_payload}

    and udp_pdu = UDP_pdu of {header : udp_header; payload : udp_payload}

    and tcp_payload = TCP_payload_raw of tcp_payload_raw

    and udp_payload = UDP_payload_raw of udp_payload_raw

    (*$*)
  end

  open PDU

  module PDU_to = struct
    let layer2_pdu_to_layer3_pdu (pdu : layer2_pdu) : layer3_pdu option =
      match pdu with
      | Ether (Ether_frame {payload; _}) -> (
          match payload with
          | Ether_payload_raw _ ->
            None
          | Ether_payload_encap d ->
            Some d )

    let layer3_pdu_to_layer4_pdu (pdu : layer3_pdu) : layer4_pdu option =
      match pdu with
      | IPv4 (IPv4_pkt {payload; _}) -> (
          match payload with
          | IPv4_payload_raw _ ->
            None
          | IPv4_payload_icmp _ ->
            None
          | IPv4_payload_encap d ->
            Some d )
      | IPv6 (IPv6_pkt {payload; _}) -> (
          match payload with
          | IPv6_payload_raw _ ->
            None
          | IPv6_payload_icmp _ ->
            None
          | IPv6_payload_encap d ->
            Some d )

    let layer2_pdu (pdu : pdu) : layer2_pdu option =
      match pdu with Layer2 d -> Some d | _ -> None

    let layer3_pdu (pdu : pdu) : layer3_pdu option =
      match pdu with
      | Layer2 _ -> (
          match layer2_pdu pdu with
          | Some d -> (
              match layer2_pdu_to_layer3_pdu d with
              | Some d ->
                Some d
              | None ->
                None )
          | _ ->
            None )
      | Layer3 p ->
        Some p
      | Layer4 _ ->
        None

    let layer4_pdu (pdu : pdu) : layer4_pdu option =
      match pdu with
      | Layer2 _ | Layer3 _ -> (
          match layer3_pdu pdu with
          | Some d -> (
              match layer3_pdu_to_layer4_pdu d with
              | Some d ->
                Some d
              | None ->
                None )
          | _ ->
            None )
      | Layer4 p ->
        Some p

    (*$ let words =
        [ "layer2", "Ether",  "Ether_frame", "ether",  "ether_frame"
        ; "layer3", "IPv4",   "IPv4_pkt",    "ipv4",   "ipv4_pkt"
        ; "layer3", "IPv6",   "IPv6_pkt",    "ipv6",   "ipv6_pkt"
        ; "layer4", "TCP",    "TCP_pdu",     "tcp",    "tcp_pdu"
        ; "layer4", "UDP",    "UDP_pdu",     "udp",    "udp_pdu"
        ]
      in
      List.iter
        (fun (layer, layer_pdu_con, pdu_con, proto_name, pdu_ty) ->
           Printf.printf {|
               let %s (pdu : pdu) : %s option =
                 match %s_pdu pdu with
                 | Some (%s x) -> Some x
                 | _ -> None
             |}
             pdu_ty pdu_ty
             layer
             layer_pdu_con;

           Printf.printf {|
               let %s_header (pdu : pdu) : %s_header option =
                 match %s_pdu pdu with
                 | Some (%s (%s {header; _})) -> Some header
                 | _ -> None
             |}
             proto_name proto_name
             layer
             layer_pdu_con pdu_con;

           Printf.printf {|
               let %s_payload (pdu : pdu) : %s_payload option =
                 match %s_pdu pdu with
                 | Some (%s (%s {payload; _})) -> Some payload
                 | _ -> None
             |}
             proto_name proto_name
             layer
             layer_pdu_con pdu_con;
        )
        words;

      let words =
        [ 4
        ; 6
        ]
      in
      List.iter
        (fun ver ->
           Printf.printf {|
               let icmpv%d_pkt (pdu : pdu) : icmpv%d_pkt option =
                 match layer3_pdu pdu with
                 | Some (IPv%d (IPv%d_pkt
                                  {payload = IPv%d_payload_icmp x; _})) -> Some x
                 | _ -> None
             |}
             ver ver
             ver ver
             ver;

           Printf.printf {|
               let icmpv%d_header (pdu : pdu) : icmpv%d_header option =
                 match layer3_pdu pdu with
                 | Some
                   (IPv%d
                      (IPv%d_pkt
                         {payload =
                            IPv%d_payload_icmp
                              (ICMPv%d_pkt {header; _}); _})) -> Some header
                 | _ -> None
             |}
             ver ver
             ver
             ver
             ver
             ver;

           Printf.printf {|
               let icmpv%d_payload (pdu : pdu) : icmpv%d_payload option =
                 match layer3_pdu pdu with
                 | Some
                   (IPv%d
                      (IPv%d_pkt
                         {payload =
                            IPv%d_payload_icmp
                              (ICMPv%d_pkt {payload; _}); _})) -> Some payload
                 | _ -> None
             |}
             ver ver
             ver
             ver
             ver
             ver;
        )
        words
    *)
    let ether_frame (pdu : pdu) : ether_frame option =
      match layer2_pdu pdu with Some (Ether x) -> Some x | _ -> None

    let ether_header (pdu : pdu) : ether_header option =
      match layer2_pdu pdu with
      | Some (Ether (Ether_frame {header; _})) ->
        Some header
      | _ ->
        None

    let ether_payload (pdu : pdu) : ether_payload option =
      match layer2_pdu pdu with
      | Some (Ether (Ether_frame {payload; _})) ->
        Some payload
      | _ ->
        None

    let ipv4_pkt (pdu : pdu) : ipv4_pkt option =
      match layer3_pdu pdu with Some (IPv4 x) -> Some x | _ -> None

    let ipv4_header (pdu : pdu) : ipv4_header option =
      match layer3_pdu pdu with
      | Some (IPv4 (IPv4_pkt {header; _})) ->
        Some header
      | _ ->
        None

    let ipv4_payload (pdu : pdu) : ipv4_payload option =
      match layer3_pdu pdu with
      | Some (IPv4 (IPv4_pkt {payload; _})) ->
        Some payload
      | _ ->
        None

    let ipv6_pkt (pdu : pdu) : ipv6_pkt option =
      match layer3_pdu pdu with Some (IPv6 x) -> Some x | _ -> None

    let ipv6_header (pdu : pdu) : ipv6_header option =
      match layer3_pdu pdu with
      | Some (IPv6 (IPv6_pkt {header; _})) ->
        Some header
      | _ ->
        None

    let ipv6_payload (pdu : pdu) : ipv6_payload option =
      match layer3_pdu pdu with
      | Some (IPv6 (IPv6_pkt {payload; _})) ->
        Some payload
      | _ ->
        None

    let tcp_pdu (pdu : pdu) : tcp_pdu option =
      match layer4_pdu pdu with Some (TCP x) -> Some x | _ -> None

    let tcp_header (pdu : pdu) : tcp_header option =
      match layer4_pdu pdu with
      | Some (TCP (TCP_pdu {header; _})) ->
        Some header
      | _ ->
        None

    let tcp_payload (pdu : pdu) : tcp_payload option =
      match layer4_pdu pdu with
      | Some (TCP (TCP_pdu {payload; _})) ->
        Some payload
      | _ ->
        None

    let udp_pdu (pdu : pdu) : udp_pdu option =
      match layer4_pdu pdu with Some (UDP x) -> Some x | _ -> None

    let udp_header (pdu : pdu) : udp_header option =
      match layer4_pdu pdu with
      | Some (UDP (UDP_pdu {header; _})) ->
        Some header
      | _ ->
        None

    let udp_payload (pdu : pdu) : udp_payload option =
      match layer4_pdu pdu with
      | Some (UDP (UDP_pdu {payload; _})) ->
        Some payload
      | _ ->
        None

    let icmpv4_pkt (pdu : pdu) : icmpv4_pkt option =
      match layer3_pdu pdu with
      | Some (IPv4 (IPv4_pkt {payload = IPv4_payload_icmp x; _})) ->
        Some x
      | _ ->
        None

    let icmpv4_header (pdu : pdu) : icmpv4_header option =
      match layer3_pdu pdu with
      | Some
          (IPv4
             (IPv4_pkt
                {payload = IPv4_payload_icmp (ICMPv4_pkt {header; _}); _})) ->
        Some header
      | _ ->
        None

    let icmpv4_payload (pdu : pdu) : icmpv4_payload option =
      match layer3_pdu pdu with
      | Some
          (IPv4
             (IPv4_pkt
                {payload = IPv4_payload_icmp (ICMPv4_pkt {payload; _}); _})) ->
        Some payload
      | _ ->
        None

    let icmpv6_pkt (pdu : pdu) : icmpv6_pkt option =
      match layer3_pdu pdu with
      | Some (IPv6 (IPv6_pkt {payload = IPv6_payload_icmp x; _})) ->
        Some x
      | _ ->
        None

    let icmpv6_header (pdu : pdu) : icmpv6_header option =
      match layer3_pdu pdu with
      | Some
          (IPv6
             (IPv6_pkt
                {payload = IPv6_payload_icmp (ICMPv6_pkt {header; _}); _})) ->
        Some header
      | _ ->
        None

    let icmpv6_payload (pdu : pdu) : icmpv6_payload option =
      match layer3_pdu pdu with
      | Some
          (IPv6
             (IPv6_pkt
                {payload = IPv6_payload_icmp (ICMPv6_pkt {payload; _}); _})) ->
        Some payload
      | _ ->
        None

    (*$*)
  end

  module Unsafe_flat_PDU = struct
    (* flat pdu is an unsafe representation of the pdu
       only used internally
    *)

    exception Incorrect_pdu_layout

    type layer2_header = Ether_header of ether_header

    and layer2_data = Ether_data_raw of ether_payload_raw

    and layer3_header =
      | IPv4_header of ipv4_header
      | IPv6_header of ipv6_header

    and layer3_data =
      | IPv4_data_raw of ipv4_payload_raw
      | IPv4_data_icmp of icmpv4_pkt
      | IPv6_data_raw of ipv6_payload_raw
      | IPv6_data_icmp of icmpv6_pkt

    and layer4_header =
      | TCP_header of tcp_header
      | UDP_header of udp_header

    and layer4_data =
      | TCP_data_raw of tcp_payload_raw
      | UDP_data_raw of udp_payload_raw

    type flat_pdu =
      { layer2_header : layer2_header option
      ; layer3_header : layer3_header option
      ; layer4_header : layer4_header option
      ; layer2_data : layer2_data option
      ; layer3_data : layer3_data option
      ; layer4_data : layer4_data option }

    let blank_flat_pdu =
      { layer2_header = None
      ; layer3_header = None
      ; layer4_header = None
      ; layer2_data = None
      ; layer3_data = None
      ; layer4_data = None }

    let flatten (pdu : pdu) : flat_pdu =
      let rec aux flat pdu =
        match pdu with
        | Layer2
            (Ether (Ether_frame {header; payload = Ether_payload_encap pdu}))
          ->
          aux
            {flat with layer2_header = Some (Ether_header header)}
            (Layer3 pdu)
        | Layer2
            (Ether (Ether_frame {header; payload = Ether_payload_raw data})) ->
          { flat with
            layer2_header = Some (Ether_header header)
          ; layer2_data = Some (Ether_data_raw data) }
        | Layer3 (IPv4 (IPv4_pkt {header; payload = IPv4_payload_raw data})) ->
          { flat with
            layer3_header = Some (IPv4_header header)
          ; layer3_data = Some (IPv4_data_raw data) }
        | Layer3 (IPv4 (IPv4_pkt {header; payload = IPv4_payload_icmp pkt})) ->
          { flat with
            layer3_header = Some (IPv4_header header)
          ; layer3_data = Some (IPv4_data_icmp pkt) }
        | Layer3 (IPv4 (IPv4_pkt {header; payload = IPv4_payload_encap pdu}))
          ->
          aux
            {flat with layer3_header = Some (IPv4_header header)}
            (Layer4 pdu)
        | Layer3 (IPv6 (IPv6_pkt {header; payload = IPv6_payload_raw data})) ->
          { flat with
            layer3_header = Some (IPv6_header header)
          ; layer3_data = Some (IPv6_data_raw data) }
        | Layer3 (IPv6 (IPv6_pkt {header; payload = IPv6_payload_icmp pkt})) ->
          { flat with
            layer3_header = Some (IPv6_header header)
          ; layer3_data = Some (IPv6_data_icmp pkt) }
        | Layer3 (IPv6 (IPv6_pkt {header; payload = IPv6_payload_encap pdu}))
          ->
          aux
            {flat with layer3_header = Some (IPv6_header header)}
            (Layer4 pdu)
        | Layer4 (TCP (TCP_pdu {header; payload = TCP_payload_raw data})) ->
          { flat with
            layer4_header = Some (TCP_header header)
          ; layer4_data = Some (TCP_data_raw data) }
        | Layer4 (UDP (UDP_pdu {header; payload = UDP_payload_raw data})) ->
          { flat with
            layer4_header = Some (UDP_header header)
          ; layer4_data = Some (UDP_data_raw data) }
      in
      aux blank_flat_pdu pdu

    let unflatten (flat : flat_pdu) : pdu option =
      let rec aux flat =
        match flat with
        | {layer2_header = Some header; layer2_data = Some p; _} -> (
            match (header, p) with
            | Ether_header header, Ether_data_raw p ->
              Layer2
                (Ether (Ether_frame {header; payload = Ether_payload_raw p})) )
        | {layer2_header = Some header; layer2_data = None; _} -> (
            let new_flat = {flat with layer2_header = None} in
            match header with
            | Ether_header header ->
              Layer2
                (Ether
                   (Ether_frame
                      { header
                      ; payload =
                          Ether_payload_encap
                            (Misc_utils.unwrap_opt
                               (PDU_to.layer3_pdu (aux new_flat))) })) )
        | {layer3_header = Some header; layer3_data = Some p; _} -> (
            match (header, p) with
            | IPv4_header header, IPv4_data_raw p ->
              Layer3 (IPv4 (IPv4_pkt {header; payload = IPv4_payload_raw p}))
            | IPv4_header header, IPv4_data_icmp p ->
              Layer3 (IPv4 (IPv4_pkt {header; payload = IPv4_payload_icmp p}))
            | IPv6_header header, IPv6_data_raw p ->
              Layer3 (IPv6 (IPv6_pkt {header; payload = IPv6_payload_raw p}))
            | IPv6_header header, IPv6_data_icmp p ->
              Layer3 (IPv6 (IPv6_pkt {header; payload = IPv6_payload_icmp p}))
            | _ ->
              raise Incorrect_pdu_layout )
        | {layer3_header = Some header; layer3_data = None; _} -> (
            let new_flat = {flat with layer3_header = None} in
            match header with
            | IPv4_header header ->
              Layer3
                (IPv4
                   (IPv4_pkt
                      { header
                      ; payload =
                          IPv4_payload_encap
                            (Misc_utils.unwrap_opt
                               (PDU_to.layer4_pdu (aux new_flat))) }))
            | IPv6_header header ->
              Layer3
                (IPv6
                   (IPv6_pkt
                      { header
                      ; payload =
                          IPv6_payload_encap
                            (Misc_utils.unwrap_opt
                               (PDU_to.layer4_pdu (aux new_flat))) })) )
        | {layer4_header = Some header; layer4_data = Some p; _} -> (
            match (header, p) with
            | TCP_header header, TCP_data_raw p ->
              Layer4 (TCP (TCP_pdu {header; payload = TCP_payload_raw p}))
            | UDP_header header, UDP_data_raw p ->
              Layer4 (UDP (UDP_pdu {header; payload = UDP_payload_raw p}))
            | _ ->
              raise Incorrect_pdu_layout )
        | {layer4_header = Some _; layer4_data = None; _} ->
          raise Incorrect_pdu_layout
        | _ ->
          raise Incorrect_pdu_layout
      in
      let check_for_blank flat =
        match flat with
        | { layer2_header = None
          ; layer3_header = None
          ; layer4_header = None
          ; layer2_data = None
          ; layer3_data = None
          ; layer4_data = None } ->
          None
        | _ ->
          Some (aux flat)
      in
      check_for_blank flat

    let unflatten_layer2 (flat : flat_pdu) : layer2_pdu option =
      match unflatten flat with Some p -> PDU_to.layer2_pdu p | None -> None

    let unflatten_layer3 (flat : flat_pdu) : layer3_pdu option =
      let flat = {flat with layer2_header = None; layer2_data = None} in
      match unflatten flat with Some p -> PDU_to.layer3_pdu p | None -> None

    let unflatten_layer4 (flat : flat_pdu) : layer4_pdu option =
      let flat =
        { flat with
          layer2_header = None
        ; layer2_data = None
        ; layer3_header = None
        ; layer3_data = None }
      in
      match unflatten flat with Some p -> PDU_to.layer4_pdu p | None -> None

    let merge_flat_pdu_use_left_on_conflict (left : flat_pdu)
        (right : flat_pdu) : flat_pdu =
      let stick_with_left : bool ref = ref false in
      let layer2_header, layer2_data =
        match (left.layer2_header, right.layer2_header) with
        | None, None ->
          (None, None)
        | Some _, None ->
          (left.layer2_header, left.layer2_data)
        | None, Some _ ->
          (right.layer2_header, right.layer2_data)
        | Some _, Some _ ->
          stick_with_left := true;
          (left.layer2_header, left.layer2_data)
      in
      let layer3_header, layer3_data =
        if !stick_with_left then (left.layer3_header, left.layer3_data)
        else
          match (left.layer3_header, right.layer3_header) with
          | None, None ->
            (None, None)
          | Some _, None ->
            (left.layer3_header, left.layer3_data)
          | None, Some _ ->
            (right.layer3_header, right.layer3_data)
          | Some _, Some _ ->
            stick_with_left := true;
            (left.layer3_header, left.layer3_data)
      in
      let layer4_header, layer4_data =
        if !stick_with_left then (left.layer4_header, left.layer4_data)
        else
          match (left.layer4_header, right.layer4_header) with
          | None, None ->
            (None, None)
          | Some _, None ->
            (left.layer4_header, left.layer4_data)
          | None, Some _ ->
            (right.layer4_header, right.layer4_data)
          | Some _, Some _ ->
            stick_with_left := true;
            (left.layer4_header, left.layer4_data)
      in
      { layer2_header
      ; layer3_header
      ; layer4_header
      ; layer2_data
      ; layer3_data
      ; layer4_data }

    let merge_flat_pdu_use_right_on_conflict (left : flat_pdu)
        (right : flat_pdu) : flat_pdu =
      merge_flat_pdu_use_left_on_conflict right left

    let merge_pdu_use_left_on_conflict (l : pdu) (r : pdu) : pdu =
      let flat_l = flatten l in
      let flat_r = flatten r in
      let pdu_opt =
        merge_flat_pdu_use_left_on_conflict flat_l flat_r |> unflatten
      in
      Misc_utils.unwrap_opt pdu_opt

    let merge_pdu_use_right_on_conflict (l : pdu) (r : pdu) : pdu =
      merge_pdu_use_left_on_conflict r l

    let resolve_encap_issue_prefer_raw (flat : flat_pdu) : flat_pdu =
      let rec aux acc flat =
        match flat with
        | { layer2_header = Some _ as layer2_header
          ; layer2_data = Some _ as layer2_data
          ; _ } ->
          {acc with layer2_header; layer2_data}
        | {layer2_header = Some _ as layer2_header; layer2_data = None; _} ->
          aux {acc with layer2_header} {flat with layer2_header = None}
        | { layer3_header = Some _ as layer3_header
          ; layer3_data = Some _ as layer3_data
          ; _ } ->
          {acc with layer3_header; layer3_data}
        | {layer3_header = Some _ as layer3_header; layer3_data = None; _} ->
          aux {acc with layer3_header} {flat with layer3_header = None}
        | { layer4_header = Some _ as layer4_header
          ; layer4_data = Some _ as layer4_data
          ; _ } ->
          {acc with layer4_header; layer4_data}
        | {layer4_header = Some _ as layer4_header; layer4_data = None; _} ->
          aux {acc with layer4_header} {flat with layer4_header = None}
        | _ ->
          acc
      in
      aux blank_flat_pdu flat

    let resolve_encap_issue_prefer_encap (flat : flat_pdu) : flat_pdu =
      let rec aux acc flat =
        match flat with
        | { layer2_header = Some _ as layer2_header
          ; layer2_data = _
          ; layer3_header = Some _ as layer3_header
          ; _ } ->
          aux
            {acc with layer2_header; layer3_header}
            {flat with layer2_header = None}
        | { layer2_header = Some _ as layer2_header
          ; layer2_data = _ as layer2_data
          ; layer3_header = None
          ; _ } ->
          {acc with layer2_header; layer2_data}
        | { layer3_header = Some _ as layer3_header
          ; layer3_data = _
          ; layer4_header = Some _ as layer4_header
          ; _ } ->
          aux
            {acc with layer3_header; layer4_header}
            {flat with layer3_header = None}
        | { layer3_header = Some _ as layer3_header
          ; layer3_data = _ as layer3_data
          ; layer4_header = None
          ; _ } ->
          {acc with layer3_header; layer3_data}
        | { layer4_header = Some _ as layer4_header
          ; layer4_data = _ as layer4_data
          ; _ } ->
          {acc with layer4_header; layer4_data}
        | _ ->
          acc
      in
      aux blank_flat_pdu flat
  end

  module PDU_map = struct
    let layer2_pdu (f : layer2_pdu -> layer2_pdu) (pdu : pdu) : pdu =
      match pdu with
      | Layer2 pdu ->
        Layer2 (f pdu)
      | Layer3 _ ->
        pdu
      | Layer4 _ ->
        pdu

    (*$ for i=3 to 4 do
            Printf.printf {|
                let layer%d_pdu (f : layer%d_pdu -> layer%d_pdu) (pdu : pdu) : pdu =
                  match PDU_to.layer%d_pdu pdu with
                  | None -> pdu
                  | Some p ->
                    Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu (Layer%d (f p))
              |}
              i i i
              i
              i;
          done
    *)
    let layer3_pdu (f : layer3_pdu -> layer3_pdu) (pdu : pdu) : pdu =
      match PDU_to.layer3_pdu pdu with
      | None ->
        pdu
      | Some p ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu (Layer3 (f p))

    let layer4_pdu (f : layer4_pdu -> layer4_pdu) (pdu : pdu) : pdu =
      match PDU_to.layer4_pdu pdu with
      | None ->
        pdu
      | Some p ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu (Layer4 (f p))

    (*$*)

    (*$ let words =
          [ "layer2", "Layer2", "Ether", "Ether_frame", "ether", "ether_frame"
          ; "layer3", "Layer3", "IPv4",   "IPv4_pkt",    "ipv4",   "ipv4_pkt"
          ; "layer3", "Layer3", "IPv6",   "IPv6_pkt",    "ipv6",   "ipv6_pkt"
          ; "layer4", "Layer4", "TCP",    "TCP_pdu",     "tcp",    "tcp_pdu"
          ; "layer4", "Layer4", "UDP",    "UDP_pdu",     "udp",    "udp_pdu"
          ]
        in
        List.iter
          (fun (layer, layer_con, layer_pdu_con, pdu_con, proto_name, pdu_ty) ->
            Printf.printf {|
                let %s (f : %s -> %s) (pdu : pdu) : pdu =
                  match PDU_to.%s_pdu pdu with
                  | Some (%s p) ->
                    Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu (%s (%s (f p)))
                  | _ -> pdu
              |}
              pdu_ty pdu_ty pdu_ty
              layer
              layer_pdu_con
              layer_con layer_pdu_con;

            Printf.printf {|
                let %s_header (f : %s_header -> %s_header) (pdu : pdu) : pdu =
                  match PDU_to.%s_pdu pdu with
                  | Some (%s (%s {header; payload})) ->
                    Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
                      (%s (%s (%s {header = f header; payload})))
                  | _ -> pdu
              |}
              proto_name proto_name proto_name
              layer
              layer_pdu_con pdu_con
              layer_con layer_pdu_con pdu_con;

            Printf.printf {|
                let %s_payload (f : %s_payload -> %s_payload) (pdu : pdu) :
                pdu =
                  match PDU_to.%s_pdu pdu with
                  | Some (%s (%s {header; payload})) ->
                    Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
                      (%s (%s (%s {header; payload = f payload})))
                  | _ -> pdu
              |}
              proto_name proto_name proto_name
              layer
              layer_pdu_con pdu_con
              layer_con layer_pdu_con pdu_con;
          )
          words;
        let words =
          [ 4
          ; 6
          ]
        in
        List.iter
          (fun ver ->
            Printf.printf {|
               let icmpv%d_pkt (f : icmpv%d_pkt -> icmpv%d_pkt) (pdu : pdu) : pdu =
                 match PDU_to.layer3_pdu pdu with
                 | Some (IPv%d (IPv%d_pkt
                                  {header; payload = IPv%d_payload_icmp p})) ->
                    Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
                      (Layer3 (IPv%d (IPv%d_pkt {header; payload = IPv%d_payload_icmp (f p)})))
                 | _ -> pdu
              |}
              ver ver ver
              ver ver
              ver
              ver ver ver;

           Printf.printf {|
               let icmpv%d_header (f : icmpv%d_header -> icmpv%d_header) (pdu : pdu) : pdu =
                 match PDU_to.layer3_pdu pdu with
                 | Some
                   (IPv%d
                      (IPv%d_pkt
                         {header = ipv4_header;
                          payload =
                            IPv%d_payload_icmp
                              (ICMPv%d_pkt {header; payload})})) ->
                   Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
                     (Layer3
                        (IPv%d
                          (IPv%d_pkt
                            {header = ipv4_header;
                              payload =
                                IPv%d_payload_icmp
                                  (ICMPv%d_pkt {header = f header; payload})})))
                 | _ -> pdu
             |}
             ver ver ver
             ver
             ver
             ver
             ver
             ver
             ver
             ver
             ver;

           Printf.printf {|
               let icmpv%d_payload (f : icmpv%d_payload -> icmpv%d_payload) (pdu : pdu) : pdu =
                 match PDU_to.layer3_pdu pdu with
                 | Some
                   (IPv%d
                      (IPv%d_pkt
                         {header = ipv4_header;
                          payload =
                            IPv%d_payload_icmp
                              (ICMPv%d_pkt {header; payload})})) ->
                   Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
                     (Layer3
                        (IPv%d
                          (IPv%d_pkt
                            {header = ipv4_header;
                              payload =
                                IPv%d_payload_icmp
                                  (ICMPv%d_pkt {header; payload = f payload})})))
                 | _ -> pdu
             |}
             ver ver ver
             ver
             ver
             ver
             ver
             ver
             ver
             ver
             ver;
          )
          words
    *)
    let ether_frame (f : ether_frame -> ether_frame) (pdu : pdu) : pdu =
      match PDU_to.layer2_pdu pdu with
      | Some (Ether p) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer2 (Ether (f p)))
      | _ ->
        pdu

    let ether_header (f : ether_header -> ether_header) (pdu : pdu) : pdu =
      match PDU_to.layer2_pdu pdu with
      | Some (Ether (Ether_frame {header; payload})) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer2 (Ether (Ether_frame {header = f header; payload})))
      | _ ->
        pdu

    let ether_payload (f : ether_payload -> ether_payload) (pdu : pdu) : pdu =
      match PDU_to.layer2_pdu pdu with
      | Some (Ether (Ether_frame {header; payload})) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer2 (Ether (Ether_frame {header; payload = f payload})))
      | _ ->
        pdu

    let ipv4_pkt (f : ipv4_pkt -> ipv4_pkt) (pdu : pdu) : pdu =
      match PDU_to.layer3_pdu pdu with
      | Some (IPv4 p) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer3 (IPv4 (f p)))
      | _ ->
        pdu

    let ipv4_header (f : ipv4_header -> ipv4_header) (pdu : pdu) : pdu =
      match PDU_to.layer3_pdu pdu with
      | Some (IPv4 (IPv4_pkt {header; payload})) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer3 (IPv4 (IPv4_pkt {header = f header; payload})))
      | _ ->
        pdu

    let ipv4_payload (f : ipv4_payload -> ipv4_payload) (pdu : pdu) : pdu =
      match PDU_to.layer3_pdu pdu with
      | Some (IPv4 (IPv4_pkt {header; payload})) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer3 (IPv4 (IPv4_pkt {header; payload = f payload})))
      | _ ->
        pdu

    let ipv6_pkt (f : ipv6_pkt -> ipv6_pkt) (pdu : pdu) : pdu =
      match PDU_to.layer3_pdu pdu with
      | Some (IPv6 p) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer3 (IPv6 (f p)))
      | _ ->
        pdu

    let ipv6_header (f : ipv6_header -> ipv6_header) (pdu : pdu) : pdu =
      match PDU_to.layer3_pdu pdu with
      | Some (IPv6 (IPv6_pkt {header; payload})) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer3 (IPv6 (IPv6_pkt {header = f header; payload})))
      | _ ->
        pdu

    let ipv6_payload (f : ipv6_payload -> ipv6_payload) (pdu : pdu) : pdu =
      match PDU_to.layer3_pdu pdu with
      | Some (IPv6 (IPv6_pkt {header; payload})) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer3 (IPv6 (IPv6_pkt {header; payload = f payload})))
      | _ ->
        pdu

    let tcp_pdu (f : tcp_pdu -> tcp_pdu) (pdu : pdu) : pdu =
      match PDU_to.layer4_pdu pdu with
      | Some (TCP p) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer4 (TCP (f p)))
      | _ ->
        pdu

    let tcp_header (f : tcp_header -> tcp_header) (pdu : pdu) : pdu =
      match PDU_to.layer4_pdu pdu with
      | Some (TCP (TCP_pdu {header; payload})) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer4 (TCP (TCP_pdu {header = f header; payload})))
      | _ ->
        pdu

    let tcp_payload (f : tcp_payload -> tcp_payload) (pdu : pdu) : pdu =
      match PDU_to.layer4_pdu pdu with
      | Some (TCP (TCP_pdu {header; payload})) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer4 (TCP (TCP_pdu {header; payload = f payload})))
      | _ ->
        pdu

    let udp_pdu (f : udp_pdu -> udp_pdu) (pdu : pdu) : pdu =
      match PDU_to.layer4_pdu pdu with
      | Some (UDP p) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer4 (UDP (f p)))
      | _ ->
        pdu

    let udp_header (f : udp_header -> udp_header) (pdu : pdu) : pdu =
      match PDU_to.layer4_pdu pdu with
      | Some (UDP (UDP_pdu {header; payload})) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer4 (UDP (UDP_pdu {header = f header; payload})))
      | _ ->
        pdu

    let udp_payload (f : udp_payload -> udp_payload) (pdu : pdu) : pdu =
      match PDU_to.layer4_pdu pdu with
      | Some (UDP (UDP_pdu {header; payload})) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer4 (UDP (UDP_pdu {header; payload = f payload})))
      | _ ->
        pdu

    let icmpv4_pkt (f : icmpv4_pkt -> icmpv4_pkt) (pdu : pdu) : pdu =
      match PDU_to.layer3_pdu pdu with
      | Some (IPv4 (IPv4_pkt {header; payload = IPv4_payload_icmp p})) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer3
             (IPv4 (IPv4_pkt {header; payload = IPv4_payload_icmp (f p)})))
      | _ ->
        pdu

    let icmpv4_header (f : icmpv4_header -> icmpv4_header) (pdu : pdu) : pdu =
      match PDU_to.layer3_pdu pdu with
      | Some
          (IPv4
             (IPv4_pkt
                { header = ipv4_header
                ; payload = IPv4_payload_icmp (ICMPv4_pkt {header; payload}) }))
        ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer3
             (IPv4
                (IPv4_pkt
                   { header = ipv4_header
                   ; payload =
                       IPv4_payload_icmp
                         (ICMPv4_pkt {header = f header; payload}) })))
      | _ ->
        pdu

    let icmpv4_payload (f : icmpv4_payload -> icmpv4_payload) (pdu : pdu) : pdu
      =
      match PDU_to.layer3_pdu pdu with
      | Some
          (IPv4
             (IPv4_pkt
                { header = ipv4_header
                ; payload = IPv4_payload_icmp (ICMPv4_pkt {header; payload}) }))
        ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer3
             (IPv4
                (IPv4_pkt
                   { header = ipv4_header
                   ; payload =
                       IPv4_payload_icmp
                         (ICMPv4_pkt {header; payload = f payload}) })))
      | _ ->
        pdu

    let icmpv6_pkt (f : icmpv6_pkt -> icmpv6_pkt) (pdu : pdu) : pdu =
      match PDU_to.layer3_pdu pdu with
      | Some (IPv6 (IPv6_pkt {header; payload = IPv6_payload_icmp p})) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer3
             (IPv6 (IPv6_pkt {header; payload = IPv6_payload_icmp (f p)})))
      | _ ->
        pdu

    let icmpv6_header (f : icmpv6_header -> icmpv6_header) (pdu : pdu) : pdu =
      match PDU_to.layer3_pdu pdu with
      | Some
          (IPv6
             (IPv6_pkt
                { header = ipv4_header
                ; payload = IPv6_payload_icmp (ICMPv6_pkt {header; payload}) }))
        ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer3
             (IPv6
                (IPv6_pkt
                   { header = ipv4_header
                   ; payload =
                       IPv6_payload_icmp
                         (ICMPv6_pkt {header = f header; payload}) })))
      | _ ->
        pdu

    let icmpv6_payload (f : icmpv6_payload -> icmpv6_payload) (pdu : pdu) : pdu
      =
      match PDU_to.layer3_pdu pdu with
      | Some
          (IPv6
             (IPv6_pkt
                { header = ipv4_header
                ; payload = IPv6_payload_icmp (ICMPv6_pkt {header; payload}) }))
        ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer3
             (IPv6
                (IPv6_pkt
                   { header = ipv4_header
                   ; payload =
                       IPv6_payload_icmp
                         (ICMPv6_pkt {header; payload = f payload}) })))
      | _ ->
        pdu

    (*$*)
  end

  module PDU_replace = struct
    let layer2_pdu (r : layer2_pdu) (pdu : pdu) : pdu =
      match pdu with Layer2 _ -> Layer2 r | Layer3 _ -> pdu | Layer4 _ -> pdu

    (*$ for i=3 to 4 do
          Printf.printf {|
              let layer%d_pdu (r : layer%d_pdu) (pdu : pdu) : pdu =
                match PDU_to.layer%d_pdu pdu with
                | None -> pdu
                | Some _ ->
                  Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu (Layer%d r)
            |}
            i i
            i
            i;
        done
    *)
    let layer3_pdu (r : layer3_pdu) (pdu : pdu) : pdu =
      match PDU_to.layer3_pdu pdu with
      | None ->
        pdu
      | Some _ ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu (Layer3 r)

    let layer4_pdu (r : layer4_pdu) (pdu : pdu) : pdu =
      match PDU_to.layer4_pdu pdu with
      | None ->
        pdu
      | Some _ ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu (Layer4 r)

    (*$*)

    (*$ let words =
        [ "layer2", "Layer2", "Ether", "Ether_frame", "ether", "ether_frame"
        ; "layer3", "Layer3", "IPv4",   "IPv4_pkt",    "ipv4",   "ipv4_pkt"
        ; "layer3", "Layer3", "IPv6",   "IPv6_pkt",    "ipv6",   "ipv6_pkt"
        ; "layer4", "Layer4", "TCP",    "TCP_pdu",     "tcp",    "tcp_pdu"
        ; "layer4", "Layer4", "UDP",    "UDP_pdu",     "udp",    "udp_pdu"
        ]
      in
      List.iter
        (fun (layer, layer_con, layer_pdu_con, pdu_con, proto_name, pdu_ty) ->
           Printf.printf {|
               let %s (r : %s) (pdu : pdu) : pdu =
                 match PDU_to.%s_pdu pdu with
                 | Some (%s _) ->
                   Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu (%s (%s r))
                 | _ -> pdu
             |}
             pdu_ty pdu_ty
             layer
             layer_pdu_con
             layer_con layer_pdu_con;

           Printf.printf {|
               let %s_header (r : %s_header) (pdu : pdu) : pdu =
                 match PDU_to.%s_pdu pdu with
                 | Some (%s (%s {payload; _})) ->
                   Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
                     (%s (%s (%s {header = r; payload})))
                 | _ -> pdu
             |}
             proto_name proto_name
             layer
             layer_pdu_con pdu_con
             layer_con layer_pdu_con pdu_con;

           Printf.printf {|
               let %s_payload (r : %s_payload) (pdu : pdu) : pdu =
                 match PDU_to.%s_pdu pdu with
                 | Some (%s (%s {header; _})) ->
                   Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
                     (%s (%s (%s {header; payload = r})))
                 | _ -> pdu
             |}
             proto_name proto_name
             layer
             layer_pdu_con pdu_con
             layer_con layer_pdu_con pdu_con;
        )
        words;
      let words =
        [ 4
        ; 6
        ]
      in
      List.iter
        (fun ver ->
            Printf.printf {|
               let icmpv%d_pkt (r : icmpv%d_pkt) (pdu : pdu) : pdu =
                 match PDU_to.layer3_pdu pdu with
                 | Some (IPv%d (IPv%d_pkt
                                  {header; payload = IPv%d_payload_icmp _})) ->
                    Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
                      (Layer3 (IPv%d (IPv%d_pkt {header; payload = IPv%d_payload_icmp r})))
                 | _ -> pdu
              |}
              ver ver
              ver ver
              ver
              ver ver ver;

           Printf.printf {|
               let icmpv%d_header (r : icmpv%d_header) (pdu : pdu) : pdu =
                 match PDU_to.layer3_pdu pdu with
                 | Some
                   (IPv%d
                      (IPv%d_pkt
                         {header = ipv4_header;
                          payload =
                            IPv%d_payload_icmp
                              (ICMPv%d_pkt {payload; _})})) ->
                   Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
                     (Layer3
                        (IPv%d
                          (IPv%d_pkt
                            {header = ipv4_header;
                              payload =
                                IPv%d_payload_icmp
                                  (ICMPv%d_pkt {header = r; payload})})))
                 | _ -> pdu
             |}
             ver ver
             ver
             ver
             ver
             ver
             ver
             ver
             ver
             ver;

           Printf.printf {|
               let icmpv%d_payload (r : icmpv%d_payload) (pdu : pdu) : pdu =
                 match PDU_to.layer3_pdu pdu with
                 | Some
                   (IPv%d
                      (IPv%d_pkt
                         {header = ipv4_header;
                          payload =
                            IPv%d_payload_icmp
                              (ICMPv%d_pkt {header; _})})) ->
                   Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
                     (Layer3
                        (IPv%d
                          (IPv%d_pkt
                            {header = ipv4_header;
                              payload =
                                IPv%d_payload_icmp
                                  (ICMPv%d_pkt {header; payload = r})})))
                 | _ -> pdu
             |}
             ver ver
             ver
             ver
             ver
             ver
             ver
             ver
             ver
             ver;
        )
        words
    *)
    let ether_frame (r : ether_frame) (pdu : pdu) : pdu =
      match PDU_to.layer2_pdu pdu with
      | Some (Ether _) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer2 (Ether r))
      | _ ->
        pdu

    let ether_header (r : ether_header) (pdu : pdu) : pdu =
      match PDU_to.layer2_pdu pdu with
      | Some (Ether (Ether_frame {payload; _})) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer2 (Ether (Ether_frame {header = r; payload})))
      | _ ->
        pdu

    let ether_payload (r : ether_payload) (pdu : pdu) : pdu =
      match PDU_to.layer2_pdu pdu with
      | Some (Ether (Ether_frame {header; _})) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer2 (Ether (Ether_frame {header; payload = r})))
      | _ ->
        pdu

    let ipv4_pkt (r : ipv4_pkt) (pdu : pdu) : pdu =
      match PDU_to.layer3_pdu pdu with
      | Some (IPv4 _) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu (Layer3 (IPv4 r))
      | _ ->
        pdu

    let ipv4_header (r : ipv4_header) (pdu : pdu) : pdu =
      match PDU_to.layer3_pdu pdu with
      | Some (IPv4 (IPv4_pkt {payload; _})) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer3 (IPv4 (IPv4_pkt {header = r; payload})))
      | _ ->
        pdu

    let ipv4_payload (r : ipv4_payload) (pdu : pdu) : pdu =
      match PDU_to.layer3_pdu pdu with
      | Some (IPv4 (IPv4_pkt {header; _})) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer3 (IPv4 (IPv4_pkt {header; payload = r})))
      | _ ->
        pdu

    let ipv6_pkt (r : ipv6_pkt) (pdu : pdu) : pdu =
      match PDU_to.layer3_pdu pdu with
      | Some (IPv6 _) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu (Layer3 (IPv6 r))
      | _ ->
        pdu

    let ipv6_header (r : ipv6_header) (pdu : pdu) : pdu =
      match PDU_to.layer3_pdu pdu with
      | Some (IPv6 (IPv6_pkt {payload; _})) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer3 (IPv6 (IPv6_pkt {header = r; payload})))
      | _ ->
        pdu

    let ipv6_payload (r : ipv6_payload) (pdu : pdu) : pdu =
      match PDU_to.layer3_pdu pdu with
      | Some (IPv6 (IPv6_pkt {header; _})) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer3 (IPv6 (IPv6_pkt {header; payload = r})))
      | _ ->
        pdu

    let tcp_pdu (r : tcp_pdu) (pdu : pdu) : pdu =
      match PDU_to.layer4_pdu pdu with
      | Some (TCP _) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu (Layer4 (TCP r))
      | _ ->
        pdu

    let tcp_header (r : tcp_header) (pdu : pdu) : pdu =
      match PDU_to.layer4_pdu pdu with
      | Some (TCP (TCP_pdu {payload; _})) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer4 (TCP (TCP_pdu {header = r; payload})))
      | _ ->
        pdu

    let tcp_payload (r : tcp_payload) (pdu : pdu) : pdu =
      match PDU_to.layer4_pdu pdu with
      | Some (TCP (TCP_pdu {header; _})) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer4 (TCP (TCP_pdu {header; payload = r})))
      | _ ->
        pdu

    let udp_pdu (r : udp_pdu) (pdu : pdu) : pdu =
      match PDU_to.layer4_pdu pdu with
      | Some (UDP _) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu (Layer4 (UDP r))
      | _ ->
        pdu

    let udp_header (r : udp_header) (pdu : pdu) : pdu =
      match PDU_to.layer4_pdu pdu with
      | Some (UDP (UDP_pdu {payload; _})) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer4 (UDP (UDP_pdu {header = r; payload})))
      | _ ->
        pdu

    let udp_payload (r : udp_payload) (pdu : pdu) : pdu =
      match PDU_to.layer4_pdu pdu with
      | Some (UDP (UDP_pdu {header; _})) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer4 (UDP (UDP_pdu {header; payload = r})))
      | _ ->
        pdu

    let icmpv4_pkt (r : icmpv4_pkt) (pdu : pdu) : pdu =
      match PDU_to.layer3_pdu pdu with
      | Some (IPv4 (IPv4_pkt {header; payload = IPv4_payload_icmp _})) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer3 (IPv4 (IPv4_pkt {header; payload = IPv4_payload_icmp r})))
      | _ ->
        pdu

    let icmpv4_header (r : icmpv4_header) (pdu : pdu) : pdu =
      match PDU_to.layer3_pdu pdu with
      | Some
          (IPv4
             (IPv4_pkt
                { header = ipv4_header
                ; payload = IPv4_payload_icmp (ICMPv4_pkt {payload; _}) })) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer3
             (IPv4
                (IPv4_pkt
                   { header = ipv4_header
                   ; payload =
                       IPv4_payload_icmp (ICMPv4_pkt {header = r; payload})
                   })))
      | _ ->
        pdu

    let icmpv4_payload (r : icmpv4_payload) (pdu : pdu) : pdu =
      match PDU_to.layer3_pdu pdu with
      | Some
          (IPv4
             (IPv4_pkt
                { header = ipv4_header
                ; payload = IPv4_payload_icmp (ICMPv4_pkt {header; _}) })) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer3
             (IPv4
                (IPv4_pkt
                   { header = ipv4_header
                   ; payload =
                       IPv4_payload_icmp (ICMPv4_pkt {header; payload = r})
                   })))
      | _ ->
        pdu

    let icmpv6_pkt (r : icmpv6_pkt) (pdu : pdu) : pdu =
      match PDU_to.layer3_pdu pdu with
      | Some (IPv6 (IPv6_pkt {header; payload = IPv6_payload_icmp _})) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer3 (IPv6 (IPv6_pkt {header; payload = IPv6_payload_icmp r})))
      | _ ->
        pdu

    let icmpv6_header (r : icmpv6_header) (pdu : pdu) : pdu =
      match PDU_to.layer3_pdu pdu with
      | Some
          (IPv6
             (IPv6_pkt
                { header = ipv4_header
                ; payload = IPv6_payload_icmp (ICMPv6_pkt {payload; _}) })) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer3
             (IPv6
                (IPv6_pkt
                   { header = ipv4_header
                   ; payload =
                       IPv6_payload_icmp (ICMPv6_pkt {header = r; payload})
                   })))
      | _ ->
        pdu

    let icmpv6_payload (r : icmpv6_payload) (pdu : pdu) : pdu =
      match PDU_to.layer3_pdu pdu with
      | Some
          (IPv6
             (IPv6_pkt
                { header = ipv4_header
                ; payload = IPv6_payload_icmp (ICMPv6_pkt {header; _}) })) ->
        Unsafe_flat_PDU.merge_pdu_use_right_on_conflict pdu
          (Layer3
             (IPv6
                (IPv6_pkt
                   { header = ipv4_header
                   ; payload =
                       IPv6_payload_icmp (ICMPv6_pkt {header; payload = r})
                   })))
      | _ ->
        pdu

    (*$*)
  end

  module To_debug_string = struct
    open Unsafe_flat_PDU

    let data_hex_heading = "                   | data"

    let data_hex_prefix = "                   |      "

    let ether_addr (addr : ether_addr) : string =
      let open Linked_queue in
      let raw = ether_addr_to_byte_string addr in
      let queue = create () in
      String.iter
        (fun c -> push (Printf.sprintf "%02X" (Char.code c)) queue)
        raw;
      String.concat ":" (to_list queue)

    let ipv4_addr (addr : ipv4_addr) : string =
      let open Linked_queue in
      let raw = ipv4_addr_to_byte_string addr in
      let queue = create () in
      String.iter (fun c -> push (string_of_int (Char.code c)) queue) raw;
      String.concat "." (to_list queue)

    let ipv6_addr (addr : ipv6_addr) : string =
      let open Linked_queue in
      let raw = ipv6_addr_to_byte_string addr in
      let queue = create () in
      String.iter
        (fun c -> push (Printf.sprintf "%02X" (Char.code c)) queue)
        raw;
      String.concat ":"
        (List.map (String.concat "")
           (Misc_utils.list_chunks_of (to_list queue) ~length:2))

    let tcp_port (port : tcp_port) : string = Printf.sprintf "%5d" port

    let udp_port (port : udp_port) : string = Printf.sprintf "%5d" port

    let rec ether_header (header : ether_header) : string =
      let src_addr = ether_header_to_src_addr header in
      let dst_addr = ether_header_to_dst_addr header in
      String.concat "\n"
        [ Printf.sprintf "Layer 2 | Ethernet | src  %s" (ether_addr src_addr)
        ; Printf.sprintf "                   | dst  %s" (ether_addr dst_addr)
        ]

    and ether_payload (payload : ether_payload) : string =
      match payload with
      | Ether_payload_raw p ->
        String.concat "\n"
          ( data_hex_heading
            :: Misc_utils.byte_string_to_hex_dump_lines
              ~prefix_each_line:data_hex_prefix
              (ether_payload_raw_to_byte_string p) )
      | Ether_payload_encap p ->
        layer3_pdu p

    and ether_frame (Ether_frame frame) : string =
      String.concat "\n"
        [ether_header frame.header; ether_payload frame.payload]

    and layer2_pdu (pdu : layer2_pdu) : string =
      match pdu with Ether p -> ether_frame p

    and ipv4_header (header : ipv4_header) : string =
      let src_addr = ipv4_header_to_src_addr header in
      let dst_addr = ipv4_header_to_dst_addr header in
      String.concat "\n"
        [ Printf.sprintf "Layer 3 | IPv4     | src  %s" (ipv4_addr src_addr)
        ; Printf.sprintf "                   | dst  %s" (ipv4_addr dst_addr) ]

    and ipv4_payload (payload : ipv4_payload) : string =
      match payload with
      | IPv4_payload_raw p ->
        String.concat "\n"
          ( data_hex_heading
            :: Misc_utils.byte_string_to_hex_dump_lines
              ~prefix_each_line:data_hex_prefix
              (ipv4_payload_raw_to_byte_string p) )
      | IPv4_payload_icmp p ->
        icmpv4_pkt p
      | IPv4_payload_encap p ->
        layer4_pdu p

    and ipv4_pkt (IPv4_pkt pkt) : string =
      String.concat "\n" [ipv4_header pkt.header; ipv4_payload pkt.payload]

    and ipv6_header (header : ipv6_header) : string =
      let src_addr = ipv6_header_to_src_addr header in
      let dst_addr = ipv6_header_to_dst_addr header in
      String.concat "\n"
        [ Printf.sprintf "Layer 3 | IPv6     | src  %s" (ipv6_addr src_addr)
        ; Printf.sprintf "                   | dst  %s" (ipv6_addr dst_addr) ]

    and ipv6_payload (payload : ipv6_payload) : string =
      match payload with
      | IPv6_payload_raw p ->
        String.concat "\n"
          ( data_hex_heading
            :: Misc_utils.byte_string_to_hex_dump_lines
              ~prefix_each_line:data_hex_prefix
              (ipv6_payload_raw_to_byte_string p) )
      | IPv6_payload_icmp p ->
        icmpv6_pkt p
      | IPv6_payload_encap p ->
        layer4_pdu p

    and ipv6_pkt (IPv6_pkt pkt) : string =
      String.concat "\n" [ipv6_header pkt.header; ipv6_payload pkt.payload]

    and icmpv4_type (ty : icmpv4_type) : string =
      match ty with
      | ICMPv4_Echo_reply {id; seq} ->
        let id = Misc_utils.byte_string_to_hex_string id in
        Printf.sprintf "Echo reply, id %s, seq %d" id seq
      | ICMPv4_Destination_unreachable ->
        "Destination unreachable"
      | ICMPv4_Source_quench ->
        "Source quench"
      | ICMPv4_Redirect ->
        "Redirect"
      | ICMPv4_Echo_request {id; seq} ->
        let id = Misc_utils.byte_string_to_hex_string id in
        Printf.sprintf "Echo request, id %s, seq %d" id seq
      | ICMPv4_Time_exceeded ->
        "Time exceeded"
      | ICMPv4_Parameter_problem ->
        "Parameter problem"
      | ICMPv4_Timestamp_request {id; seq} ->
        let id = Misc_utils.byte_string_to_hex_string id in
        Printf.sprintf "Timestamp request, id %s, seq %d" id seq
      | ICMPv4_Timestamp_reply {id; seq} ->
        let id = Misc_utils.byte_string_to_hex_string id in
        Printf.sprintf "Timestamp reply, id %s, seq %d" id seq
      | ICMPv4_Information_request {id; seq} ->
        let id = Misc_utils.byte_string_to_hex_string id in
        Printf.sprintf "Information request, id %s, seq %d" id seq
      | ICMPv4_Information_reply {id; seq} ->
        let id = Misc_utils.byte_string_to_hex_string id in
        Printf.sprintf "Information reply, id %s, seq %d" id seq

    and icmpv4_header (header : icmpv4_header) : string =
      let ty = icmpv4_header_to_icmpv4_type header in
      Printf.sprintf "Layer 3 | ICMPv4   | type %s" (icmpv4_type ty)

    and icmpv4_payload (payload : icmpv4_payload) : string =
      match payload with
      | ICMPv4_payload_raw p ->
        String.concat "\n"
          ( data_hex_heading
            :: Misc_utils.byte_string_to_hex_dump_lines
              ~prefix_each_line:data_hex_prefix
              (icmpv4_payload_raw_to_byte_string p) )

    and icmpv4_pkt (ICMPv4_pkt pkt) : string =
      String.concat "\n" [icmpv4_header pkt.header; icmpv4_payload pkt.payload]

    and icmpv6_type (ty : icmpv6_type) : string =
      match ty with
      | ICMPv6_Echo_reply ->
        "Echo reply"
      | ICMPv6_Echo_request ->
        "Echo request"

    and icmpv6_header (header : icmpv6_header) : string =
      let ty = icmpv6_header_to_icmpv6_type header in
      Printf.sprintf "Layer 3 | ICMPv6   | src  %s" (icmpv6_type ty)

    and icmpv6_payload (payload : icmpv6_payload) : string =
      match payload with
      | ICMPv6_payload_raw p ->
        String.concat "\n"
          ( data_hex_heading
            :: Misc_utils.byte_string_to_hex_dump_lines
              ~prefix_each_line:data_hex_prefix
              (icmpv6_payload_raw_to_byte_string p) )

    and icmpv6_pkt (ICMPv6_pkt pkt) : string =
      String.concat "\n" [icmpv6_header pkt.header; icmpv6_payload pkt.payload]

    and layer3_pdu (pdu : layer3_pdu) : string =
      match pdu with IPv4 p -> ipv4_pkt p | IPv6 p -> ipv6_pkt p

    and tcp_header (header : tcp_header) : string =
      let src_port = tcp_header_to_src_port header in
      let dst_port = tcp_header_to_dst_port header in
      String.concat "\n"
        [ Printf.sprintf "Layer 4 | TCP      | src  %5d" src_port
        ; Printf.sprintf "                   | dst  %5d" dst_port
        ; Printf.sprintf "                   | ack %d rst %d syn %d fin %d"
            (Misc_utils.bool_to_int (tcp_header_to_ack_flag header))
            (Misc_utils.bool_to_int (tcp_header_to_rst_flag header))
            (Misc_utils.bool_to_int (tcp_header_to_syn_flag header))
            (Misc_utils.bool_to_int (tcp_header_to_fin_flag header)) ]

    and tcp_payload (payload : tcp_payload) : string =
      match payload with
      | TCP_payload_raw p ->
        String.concat "\n"
          ( data_hex_heading
            :: Misc_utils.byte_string_to_hex_dump_lines
              ~prefix_each_line:data_hex_prefix
              (tcp_payload_raw_to_byte_string p) )

    and tcp_pdu (TCP_pdu pdu : tcp_pdu) : string =
      String.concat "\n" [tcp_header pdu.header; tcp_payload pdu.payload]

    and udp_header (header : udp_header) : string =
      let src_port = udp_header_to_src_port header in
      let dst_port = udp_header_to_dst_port header in
      String.concat "\n"
        [ Printf.sprintf "Layer 4 | UDP      | src  %5d" src_port
        ; Printf.sprintf "                   | dst  %5d" dst_port ]

    and udp_payload (payload : udp_payload) : string =
      match payload with
      | UDP_payload_raw p ->
        String.concat "\n"
          ( data_hex_heading
            :: Misc_utils.byte_string_to_hex_dump_lines
              ~prefix_each_line:data_hex_prefix
              (udp_payload_raw_to_byte_string p) )

    and udp_pdu (UDP_pdu pdu : udp_pdu) : string =
      String.concat "\n" [udp_header pdu.header; udp_payload pdu.payload]

    and layer4_pdu (pdu : layer4_pdu) : string =
      match pdu with TCP p -> tcp_pdu p | UDP p -> udp_pdu p

    let flat_pdu (flat : flat_pdu) : string =
      let queue = Linked_queue.create () in
      let push s = Linked_queue.push s queue in
      ( match flat.layer2_header with
        | Some (Ether_header header) ->
          push (ether_header header)
        | None ->
          () );
      ( match flat.layer2_data with
        | Some (Ether_data_raw data) ->
          let data_hex_lines =
            Misc_utils.byte_string_to_hex_dump_lines
              ~prefix_each_line:data_hex_prefix
              (ether_payload_raw_to_byte_string data)
          in
          push data_hex_heading;
          List.iter (fun s -> push s) data_hex_lines
        | None ->
          () );
      ( match flat.layer3_header with
        | Some (IPv4_header header) ->
          push (ipv4_header header)
        | Some (IPv6_header header) ->
          push (ipv6_header header)
        | None ->
          () );
      ( match flat.layer3_data with
        | Some (IPv4_data_raw data) ->
          let data_hex_lines =
            Misc_utils.byte_string_to_hex_dump_lines
              ~prefix_each_line:data_hex_prefix
              (ipv4_payload_raw_to_byte_string data)
          in
          push (Printf.sprintf "                   | data");
          List.iter (fun s -> push s) data_hex_lines
        | Some (IPv4_data_icmp pkt) ->
          push (icmpv4_pkt pkt)
        | Some (IPv6_data_raw data) ->
          let data_hex_lines =
            Misc_utils.byte_string_to_hex_dump_lines
              ~prefix_each_line:data_hex_prefix
              (ipv6_payload_raw_to_byte_string data)
          in
          push (Printf.sprintf "                   | data");
          List.iter (fun s -> push s) data_hex_lines
        | Some (IPv6_data_icmp pkt) ->
          push (icmpv6_pkt pkt)
        | None ->
          () );
      ( match flat.layer4_header with
        | Some (TCP_header header) ->
          push (tcp_header header)
        | Some (UDP_header header) ->
          push (udp_header header)
        | None ->
          () );
      ( match flat.layer4_data with
        | Some (TCP_data_raw data) ->
          let data_hex_lines =
            Misc_utils.byte_string_to_hex_dump_lines
              ~prefix_each_line:data_hex_prefix
              (tcp_payload_raw_to_byte_string data)
          in
          push (Printf.sprintf "                   | data");
          List.iter (fun s -> push s) data_hex_lines
        | Some (UDP_data_raw data) ->
          let data_hex_lines =
            Misc_utils.byte_string_to_hex_dump_lines
              ~prefix_each_line:data_hex_prefix
              (udp_payload_raw_to_byte_string data)
          in
          push (Printf.sprintf "                   | data");
          List.iter (fun s -> push s) data_hex_lines
        | None ->
          () );
      String.concat "\n" (Linked_queue.to_list queue)

    let pdu (pdu : pdu) : string =
      let flat = Unsafe_flat_PDU.flatten pdu in
      flat_pdu flat
  end

  module Print_debug = struct
    let print_flat_pdu_debug (flat : Unsafe_flat_PDU.flat_pdu) : unit =
      print_endline (To_debug_string.flat_pdu flat)

    let print_pdu_debug (pdu : pdu) : unit =
      print_endline (To_debug_string.pdu pdu)
  end

  module Lookup_table = Lookup_table.Make (B)
end
