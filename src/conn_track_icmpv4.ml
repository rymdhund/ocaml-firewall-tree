(*$ #use "src/tree_base.cinaps"
  $*)
module type B = sig
  val cur_time_ms : unit -> int64

  module Lookup_table : Lookup_table.S

  type addr

  val addr_to_byte_string : addr -> string

  type conn_state =
    | New
    | Established
    | Invalid

  type 'a conn_state_w_key =
    | New of 'a
    | Established of 'a
    | Invalid

  val conn_state_to_conn_state_w_key : 'a -> conn_state -> 'a conn_state_w_key

  (*$ print_icmpv4_type_def ()
  *)
  type icmpv4_type =
    | ICMPv4_Echo_reply of {id : string; seq : int}
    | ICMPv4_Destination_unreachable
    | ICMPv4_Source_quench
    | ICMPv4_Redirect
    | ICMPv4_Echo_request of {id : string; seq : int}
    | ICMPv4_Time_exceeded
    | ICMPv4_Parameter_problem
    | ICMPv4_Timestamp_request of {id : string; seq : int}
    | ICMPv4_Timestamp_reply of {id : string; seq : int}
    | ICMPv4_Information_request of {id : string; seq : int}
    | ICMPv4_Information_reply of {id : string; seq : int}

  (*$*)
end

module type S = sig
  type addr

  type key

  type conn_state

  type 'a conn_state_w_key

  type tracker

  type icmpv4_type

  val make : max_conn:int -> init_size:int -> timeout_ms:int64 -> tracker

  val lookup_conn_state_w_key :
    tracker
    -> reject_new:bool
    -> no_update:bool
    -> src_addr:addr
    -> dst_addr:addr
    -> icmpv4_type
    -> key conn_state_w_key
end

module Make (B : B) :
  S
  with type addr := B.addr
   and type conn_state := B.conn_state
   and type 'a conn_state_w_key := 'a B.conn_state_w_key
   and type icmpv4_type := B.icmpv4_type = struct
  include B

  type req_rep =
    { requester_addr : string
    ; replier_addr : string
    ; id : string
    ; seq : int }

  type key =
    | Echo_req_rep of req_rep
    | Timestamp_req_rep of req_rep
    | Information_req_rep of req_rep
    | Notify of
        { ty : icmpv4_type
        ; sender_addr : string
        ; recipient_addr : string }

  type record = unit

  type tracker = {table : (key, record) Lookup_table.t}

  let make ~(max_conn : int) ~(init_size : int) ~(timeout_ms : int64) : tracker
    =
    { table =
        Lookup_table.create ~timeout_ms:(Some timeout_ms) ~max_size:max_conn
          ~init_size }

  let lookup_conn_state_w_key (tracker : tracker) ~(reject_new : bool)
      ~(no_update : bool) ~(src_addr : addr) ~(dst_addr : addr)
      (ty : icmpv4_type) : key conn_state_w_key =
    let {table} = tracker in
    Lookup_table.evict_timed_out table;
    let src_addr = addr_to_byte_string src_addr in
    let dst_addr = addr_to_byte_string dst_addr in
    match ty with
    | ICMPv4_Echo_reply {id; seq} -> (
        let key =
          Echo_req_rep
            {requester_addr = dst_addr; replier_addr = src_addr; id; seq}
        in
        match Lookup_table.find_opt table key with
        | None ->
          Invalid
        | Some _ ->
          Established key )
    | ICMPv4_Echo_request {id; seq} ->
      let key =
        Echo_req_rep
          {requester_addr = src_addr; replier_addr = dst_addr; id; seq}
      in
      if reject_new then Invalid
      else (
        if not no_update then Lookup_table.add table key ();
        New key )
    | ICMPv4_Timestamp_request {id; seq} ->
      let key =
        Timestamp_req_rep
          {requester_addr = src_addr; replier_addr = dst_addr; id; seq}
      in
      if reject_new then Invalid
      else (
        if not no_update then Lookup_table.add table key ();
        New key )
    | ICMPv4_Timestamp_reply {id; seq} -> (
        let key =
          Timestamp_req_rep
            {requester_addr = dst_addr; replier_addr = src_addr; id; seq}
        in
        match Lookup_table.find_opt table key with
        | None ->
          Invalid
        | Some _ ->
          Established key )
    | ICMPv4_Information_request {id; seq} when not (reject_new || no_update)
      ->
      let key =
        Information_req_rep
          {requester_addr = src_addr; replier_addr = dst_addr; id; seq}
      in
      if reject_new then Invalid
      else (
        if not no_update then Lookup_table.add table key ();
        New key )
    | ICMPv4_Information_reply {id; seq} -> (
        let key =
          Information_req_rep
            {requester_addr = dst_addr; replier_addr = src_addr; id; seq}
        in
        match Lookup_table.find_opt table key with
        | None ->
          Invalid
        | Some _ ->
          Established key )
    | ICMPv4_Destination_unreachable
    | ICMPv4_Redirect
    | ICMPv4_Source_quench
    | ICMPv4_Time_exceeded
    | ICMPv4_Parameter_problem ->
      let key =
        Notify {ty; sender_addr = src_addr; recipient_addr = dst_addr}
      in
      if reject_new then Invalid
      else (
        if not no_update then Lookup_table.add table key ();
        New key )
    | _ ->
      Invalid
end
