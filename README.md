# ocaml-firewall-tree

[Documentation](https://darrenldl.gitlab.io/ocaml-firewall-tree/doc/) [Coverage](https://darrenldl.gitlab.io/ocaml-firewall-tree/coverage/)

ocaml-firewall-tree is a firewall library written in OCaml that allows easy implementation of a tree based (or tree-rule) firewall with no restriction on the choice of network stack implementation

The inception of this library was heavily inspired by [Thomas Leonard's blog post](http://roscidus.com/blog/blog/2016/01/01/a-unikernel-firewall-for-qubesos/) on using MirageOS as a QubesOS firewall VM (the project is named [qubes-mirage-firewall](https://github.com/talex5/qubes-mirage-firewall)). However, the design was not carried over to this project as qubes-mirage-firewall was quite specific and not very extensible.

The current design draws inspiration from iptables and pf for its set of capabilities. The overall intuition hinges on building an internal network/circuit/pipeline for processing packets. While tree-rule firewall is not a new idea, and has been explored by various people, the tree design of this project was developed independently.

## Progress

- [x] Predicate tree

- [x] Predicate evaluation

- [x] Filtering

- [x] TCP connection tracking

- [x] UDP connection tracking

- [ ] ICMP connection tracking

- [ ] ICMPv6 connection tracking

- [x] Load balancing

      - [x] PDU based

      - [x] Connection based

- [ ] NAT

      - [x] Static / One-to-one

      - [ ] Dynamic / Many-to-many

      - [ ] Overloading / Many-to-one

- [ ] Port forwarding

- [ ] Testing related

      - [x] PDU generators for QCheck

      - [x] Tree base tests

      - [x] Tree base extended tests

      - [x] `check_outcomes` function for checking design of firewall tree

      - [ ] Routing

      - [ ] Selectors

      - [ ] Modifiers

- [x] Routing logic unit

## Main features

- Testable

  - Rule check

    - The library provides `check_outcomes` function which takes a firewall tree and predicate and uses QCheck to generate traffic to test the firewall outcome against the provided predicate
    - Depending on the complexity of your predicate, this may or may not be useful, but is still useful for catching obvious errors

  - Internal tests

    - Not sure if the module provided to the functor is written correctly? The functor also derives internal tests to thoroughly test the derived module itself which you can call easily.
    - The library itself is tested this way actually, see `tests/run_all_tests.ml`, snippet attached below
    - ```ocaml
      module M = Firewall_tree.Make (Firewall_tree.Mock_tree_base)
      
      let () =
        M.Test.run_all_tests_qcheck ();
      ```

- Powerful

  - The functor provides myriad usual firewall primitives, such as

    - NAT

    - Load balancing

    - Routing

    - Connection tracking

    - Filtering

  - A predicate expression tree is also provided to allow easy specification of conditions, such as

    - Matching IP address

    - Within IP address range

    - From a certain input interface

  - The predicate tree also allows specification of custom predicates, allowing easy composition

- Flexible

  - You can choose to pass an Ethernet frame, IP packet, or TCP/UDP PDU to the tree, the primitives will take the layer at which the PDU resides into account
    - This property applies to subtrees as well, so you can designate a subtree to deal with layer 4 traffic, another subtree to deal with layer 2 traffic and so on

- Robust

  - All primitives are designed with robustness and long run time in mind, i.e. bounded memory usage

- Extensible

  - The library facilitates implemention of custom components
    - Helper functions which deal with repetitive tasks such as decapsulation are provided

    - Primitives are designed with mixing and matching in mind

    - Primitives are designed to be user-friendly, this means they are simple to invoke and not error prone to use

## Architecture

## Example

## Contributions

Contributions are welcome. Note that by submitting contributions, you agree to license your work under the same license used by this project (MIT).

This project uses `ocp-indent`, `ocamlformat` and `cinaps` for automated code formatting and code generation

Prior to submitting a pull/merge request, please use `make format` to ensure your submission is of the same style

It is recommended that you utilise cinaps for generation of repetitive code. There are no specific requirements on the styling or formatting of cinaps code outside of being readable.

You can use `make cinaps` to call cinaps. Formatting is reapplied via the Makefile, you don't have to use `make format` afterwards.

## Acknowledgement

I'd like to thank [Andrew Hall](https://github.com/andrew-m-h) for his time on discussion of various aspects of the library, such as user experience, netwoking challenges, and architecture

I'd also like to thank the people who provided comments, feedback and pointers on the OCaml forum threads, as seen [here](https://discuss.ocaml.org/t/access-to-tcp-header-fields-in-mirageos/3497) and [here](https://discuss.ocaml.org/t/firewall-tree-request-for-nat-api-naming-advice-preference/3557)

## References

The connection tracking code is based on the following documents/tutorials

- ["IPtables: Connection tracking" by James C. Stephens](https://web.archive.org/web/20010610025001/http://www.cs.princeton.edu/~jns/security/iptables/iptables_conntrack.html)

- ["Iptables Tutorial" by Oskar Andreasson](https://www.frozentux.net/iptables-tutorial/iptables-tutorial.html)

## License

This project is distributed under the MIT license as stated in the LICENSE file
