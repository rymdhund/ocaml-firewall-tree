let make_qcheck_test ?(count : int = 100_000) ~(name : string)
    (arb : 'a QCheck.arbitrary) (f : 'a -> bool) : QCheck.Test.t =
  QCheck.Test.make ~count ~name arb f

let qcheck_tests_to_ounit2_test ~(name : string) (tests : QCheck.Test.t list) :
  OUnit2.test =
  OUnit2.(name >::: List.map QCheck_ounit.to_ounit2_test tests)

let run_qcheck_tests_as_ounit2_test ~(name : string)
    (tests : QCheck.Test.t list) : unit =
  OUnit2.run_test_tt_main (qcheck_tests_to_ounit2_test ~name tests)

let exit_if_not_zero (x : int) : unit = if x <> 0 then exit x
