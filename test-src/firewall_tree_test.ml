(*$ #use "test-src/firewall_tree_test.cinaps"
  $*)
module type S = sig
  (*$ let words =
        [ "ether", "ether_frame", ["addr"]
        ; "ipv4", "ipv4_pkt", ["addr"]
        ; "ipv6", "ipv6_pkt", ["addr"]
        ; "icmpv4", "icmpv4_pkt", ["type"]
        ; "icmpv6", "icmpv6_pkt", ["type"]
        ; "tcp", "tcp_pdu", ["port"]
        ; "udp", "udp_pdu", ["port"]
        ]
      in
      for i=2 to 4 do
        Printf.printf {|
            type layer%d_pdu
          |}
          i;
      done;
      List.iter
        (fun (proto, pdu, fields) ->
            List.iter
              (fun field ->
                Printf.printf {|
                    type %s_%s
                  |}
                  proto field;
              )
              fields;

            Printf.printf {|
                type %s_header
              |}
              proto;

            Printf.printf {|
                type %s_payload
              |}
              proto;

            Printf.printf {|
                type %s
              |}
              pdu;
        )
        words
  *)
  type layer2_pdu

  type layer3_pdu

  type layer4_pdu

  type ether_addr

  type ether_header

  type ether_payload

  type ether_frame

  type ipv4_addr

  type ipv4_header

  type ipv4_payload

  type ipv4_pkt

  type ipv6_addr

  type ipv6_header

  type ipv6_payload

  type ipv6_pkt

  type icmpv4_type

  type icmpv4_header

  type icmpv4_payload

  type icmpv4_pkt

  type icmpv6_type

  type icmpv6_header

  type icmpv6_payload

  type icmpv6_pkt

  type tcp_port

  type tcp_header

  type tcp_payload

  type tcp_pdu

  type udp_port

  type udp_header

  type udp_payload

  type udp_pdu

  (*$*)
  type pdu

  module PDU_QCheck_components : sig
    (*$ let words =
          [ "ether", "ether_frame", ["addr"]
          ; "ipv4", "ipv4_pkt", ["addr"]
          ; "ipv6", "ipv6_pkt", ["addr"]
          ; "icmpv4", "icmpv4_pkt", ["type"]
          ; "icmpv6", "icmpv6_pkt", ["type"]
          ; "tcp", "tcp_pdu", ["port"]
          ; "udp", "udp_pdu", ["port"]
          ]
        in
        for i=2 to 4 do
          Printf.printf {|
              val arbitrary_layer%d_pdu : layer%d_pdu QCheck.arbitrary
            |}
            i i;
        done;
        List.iter
          (fun (proto, pdu, fields) ->
             List.iter
               (fun field ->
                  Printf.printf {|
                      val arbitrary_%s_%s : %s_%s QCheck.arbitrary
                    |}
                    proto field proto field;
               )
               fields;

             Printf.printf {|
                 val arbitrary_%s_header : %s_header QCheck.arbitrary
               |}
               proto proto;

             Printf.printf {|
                 val arbitrary_%s_payload : %s_payload QCheck.arbitrary
               |}
               proto proto;

             Printf.printf {|
                 val arbitrary_%s : %s QCheck.arbitrary
               |}
               pdu pdu;
          )
          words
    *)
    val arbitrary_layer2_pdu : layer2_pdu QCheck.arbitrary

    val arbitrary_layer3_pdu : layer3_pdu QCheck.arbitrary

    val arbitrary_layer4_pdu : layer4_pdu QCheck.arbitrary

    val arbitrary_ether_addr : ether_addr QCheck.arbitrary

    val arbitrary_ether_header : ether_header QCheck.arbitrary

    val arbitrary_ether_payload : ether_payload QCheck.arbitrary

    val arbitrary_ether_frame : ether_frame QCheck.arbitrary

    val arbitrary_ipv4_addr : ipv4_addr QCheck.arbitrary

    val arbitrary_ipv4_header : ipv4_header QCheck.arbitrary

    val arbitrary_ipv4_payload : ipv4_payload QCheck.arbitrary

    val arbitrary_ipv4_pkt : ipv4_pkt QCheck.arbitrary

    val arbitrary_ipv6_addr : ipv6_addr QCheck.arbitrary

    val arbitrary_ipv6_header : ipv6_header QCheck.arbitrary

    val arbitrary_ipv6_payload : ipv6_payload QCheck.arbitrary

    val arbitrary_ipv6_pkt : ipv6_pkt QCheck.arbitrary

    val arbitrary_icmpv4_type : icmpv4_type QCheck.arbitrary

    val arbitrary_icmpv4_header : icmpv4_header QCheck.arbitrary

    val arbitrary_icmpv4_payload : icmpv4_payload QCheck.arbitrary

    val arbitrary_icmpv4_pkt : icmpv4_pkt QCheck.arbitrary

    val arbitrary_icmpv6_type : icmpv6_type QCheck.arbitrary

    val arbitrary_icmpv6_header : icmpv6_header QCheck.arbitrary

    val arbitrary_icmpv6_payload : icmpv6_payload QCheck.arbitrary

    val arbitrary_icmpv6_pkt : icmpv6_pkt QCheck.arbitrary

    val arbitrary_tcp_port : tcp_port QCheck.arbitrary

    val arbitrary_tcp_header : tcp_header QCheck.arbitrary

    val arbitrary_tcp_payload : tcp_payload QCheck.arbitrary

    val arbitrary_tcp_pdu : tcp_pdu QCheck.arbitrary

    val arbitrary_udp_port : udp_port QCheck.arbitrary

    val arbitrary_udp_header : udp_header QCheck.arbitrary

    val arbitrary_udp_payload : udp_payload QCheck.arbitrary

    val arbitrary_udp_pdu : udp_pdu QCheck.arbitrary

    (*$*)

    val arbitrary_pdu : pdu QCheck.arbitrary
  end

  module Tree_base_test : sig
    val run_tests_qcheck : unit -> unit

    val run_tests_ounit2 : unit -> unit
  end

  module Tree_base_extended_test : sig
    val run_tests_qcheck : unit -> unit

    val run_tests_ounit2 : unit -> unit
  end
end

module Make (T : Firewall_tree.S) :
  S
  (*$ let words =
      [ "Ether", "ether", "ether_frame", ["addr"]
      ; "IPv4", "ipv4", "ipv4_pkt", ["addr"]
      ; "IPv6", "ipv6", "ipv6_pkt", ["addr"]
      ; "ICMPv4", "icmpv4", "icmpv4_pkt", ["type"]
      ; "ICMPv6", "icmpv6", "icmpv6_pkt", ["type"]
      ; "TCP", "tcp", "tcp_pdu", []
      ; "UDP", "udp", "udp_pdu", []
      ]
    in
    print_endline "with";
    for i=2 to 4 do
      Printf.printf {|
          type layer%d_pdu := T.PDU.layer%d_pdu
        |}
        i i;
      print_endline "and"
    done;
    List.iter
      (fun (m, proto, pdu, fields) ->
          List.iter
            (fun field ->
              Printf.printf {|
                  type %s_%s := T.%s.%s_%s and
                |}
                proto field m proto field;
            )
            fields;

          Printf.printf {|
              type %s_header := T.%s.%s_header and
            |}
            proto m proto;

          Printf.printf {|
              type %s_payload := T.PDU.%s_payload and
            |}
            proto proto;

          Printf.printf {|
              type %s := T.PDU.%s and
            |}
            pdu pdu;
      )
      words;

      print_endline "type pdu := T.PDU.pdu"
  *)
  with type layer2_pdu := T.PDU.layer2_pdu
   and type layer3_pdu := T.PDU.layer3_pdu
   and type layer4_pdu := T.PDU.layer4_pdu
   and type ether_addr := T.Ether.ether_addr
   and type ether_header := T.Ether.ether_header
   and type ether_payload := T.PDU.ether_payload
   and type ether_frame := T.PDU.ether_frame
   and type ipv4_addr := T.IPv4.ipv4_addr
   and type ipv4_header := T.IPv4.ipv4_header
   and type ipv4_payload := T.PDU.ipv4_payload
   and type ipv4_pkt := T.PDU.ipv4_pkt
   and type ipv6_addr := T.IPv6.ipv6_addr
   and type ipv6_header := T.IPv6.ipv6_header
   and type ipv6_payload := T.PDU.ipv6_payload
   and type ipv6_pkt := T.PDU.ipv6_pkt
   and type icmpv4_type := T.ICMPv4.icmpv4_type
   and type icmpv4_header := T.ICMPv4.icmpv4_header
   and type icmpv4_payload := T.PDU.icmpv4_payload
   and type icmpv4_pkt := T.PDU.icmpv4_pkt
   and type icmpv6_type := T.ICMPv6.icmpv6_type
   and type icmpv6_header := T.ICMPv6.icmpv6_header
   and type icmpv6_payload := T.PDU.icmpv6_payload
   and type icmpv6_pkt := T.PDU.icmpv6_pkt
   and type tcp_header := T.TCP.tcp_header
   and type tcp_payload := T.PDU.tcp_payload
   and type tcp_pdu := T.PDU.tcp_pdu
   and type udp_header := T.UDP.udp_header
   and type udp_payload := T.PDU.udp_payload
   and type udp_pdu := T.PDU.udp_pdu
   and type pdu := T.PDU.pdu
   (*$*)
   and type tcp_port := int
   and type udp_port := int = struct
  module PDU_QCheck_components = struct
    open T
    open T.PDU
    open T.Ether
    open T.IPv4
    open T.IPv6
    open T.ICMPv4
    open T.ICMPv6
    open T.TCP
    open T.UDP

    type tcp_port = int

    type udp_port = int

    let ether_addr_byte_count = 6

    let ipv4_addr_byte_count = 4

    let ipv6_addr_byte_count = 16

    let payload_string_gen = QCheck.Gen.(string_size (int_bound 100))

    (* TCP *)
    let tcp_port_gen : tcp_port QCheck.Gen.t = QCheck.Gen.(int_bound 65535)

    let tcp_header_gen : tcp_header QCheck.Gen.t =
      let open QCheck.Gen in
      map3
        (fun src_port dst_port (syn, ack, (fin, rst)) ->
           make_dummy_tcp_header ()
           |> update_tcp_header ~src_port ~dst_port ~ack ~rst ~syn ~fin )
        tcp_port_gen tcp_port_gen
        (triple bool bool (pair bool bool))

    let tcp_payload_gen : tcp_payload QCheck.Gen.t =
      let open QCheck.Gen in
      map
        (fun s -> TCP_payload_raw (byte_string_to_tcp_payload_raw s))
        payload_string_gen

    let tcp_pdu_gen : tcp_pdu QCheck.Gen.t =
      let open QCheck.Gen in
      map2
        (fun header payload -> TCP_pdu {header; payload})
        tcp_header_gen tcp_payload_gen

    (* UDP *)
    let udp_port_gen : udp_port QCheck.Gen.t = QCheck.Gen.(int_bound 65535)

    let udp_header_gen : udp_header QCheck.Gen.t =
      let open QCheck.Gen in
      map2
        (fun src_port dst_port ->
           make_dummy_udp_header () |> update_udp_header ~src_port ~dst_port )
        udp_port_gen udp_port_gen

    let udp_payload_gen : udp_payload QCheck.Gen.t =
      let open QCheck.Gen in
      map
        (fun s -> UDP_payload_raw (byte_string_to_udp_payload_raw s))
        payload_string_gen

    let udp_pdu_gen : udp_pdu QCheck.Gen.t =
      let open QCheck.Gen in
      map2
        (fun header payload -> UDP_pdu {header; payload})
        udp_header_gen udp_payload_gen

    (* Layer 4 *)
    let layer4_pdu_gen : layer4_pdu QCheck.Gen.t =
      let open QCheck.Gen in
      oneof [map (fun p -> TCP p) tcp_pdu_gen; map (fun p -> UDP p) udp_pdu_gen]

    (* ICMPv4 *)
    let icmpv4_id_gen : string QCheck.Gen.t =
      QCheck.Gen.(string_size (return 2))

    let icmpv4_seq_gen : int QCheck.Gen.t = QCheck.Gen.(int_bound 65535)

    let icmpv4_type_gen : icmpv4_type QCheck.Gen.t =
      let open QCheck.Gen in
      oneof
        [ map2
            (fun id seq -> ICMPv4_Echo_reply {id; seq})
            icmpv4_id_gen icmpv4_seq_gen
        ; return ICMPv4_Destination_unreachable
        ; return ICMPv4_Source_quench
        ; return ICMPv4_Redirect
        ; map2
            (fun id seq -> ICMPv4_Echo_request {id; seq})
            icmpv4_id_gen icmpv4_seq_gen
        ; return ICMPv4_Time_exceeded
        ; return ICMPv4_Parameter_problem
        ; map2
            (fun id seq -> ICMPv4_Timestamp_request {id; seq})
            icmpv4_id_gen icmpv4_seq_gen
        ; map2
            (fun id seq -> ICMPv4_Timestamp_reply {id; seq})
            icmpv4_id_gen icmpv4_seq_gen
        ; map2
            (fun id seq -> ICMPv4_Information_request {id; seq})
            icmpv4_id_gen icmpv4_seq_gen
        ; map2
            (fun id seq -> ICMPv4_Information_reply {id; seq})
            icmpv4_id_gen icmpv4_seq_gen ]

    let icmpv4_header_gen : icmpv4_header QCheck.Gen.t =
      let open QCheck.Gen in
      map
        (fun icmpv4_type ->
           make_dummy_icmpv4_header () |> update_icmpv4_header ~icmpv4_type )
        icmpv4_type_gen

    let icmpv4_payload_gen : icmpv4_payload QCheck.Gen.t =
      let open QCheck.Gen in
      map
        (fun s -> ICMPv4_payload_raw (byte_string_to_icmpv4_payload_raw s))
        payload_string_gen

    let icmpv4_pkt_gen : icmpv4_pkt QCheck.Gen.t =
      let open QCheck.Gen in
      map2
        (fun header payload -> ICMPv4_pkt {header; payload})
        icmpv4_header_gen icmpv4_payload_gen

    (* ICMPv6 *)
    let icmpv6_type_gen : icmpv6_type QCheck.Gen.t =
      QCheck.Gen.(oneof [return ICMPv6_Echo_reply; return ICMPv6_Echo_request])

    let icmpv6_header_gen : icmpv6_header QCheck.Gen.t =
      let open QCheck.Gen in
      map
        (fun icmpv6_type ->
           make_dummy_icmpv6_header () |> update_icmpv6_header ~icmpv6_type )
        icmpv6_type_gen

    let icmpv6_payload_gen : icmpv6_payload QCheck.Gen.t =
      let open QCheck.Gen in
      map
        (fun s -> ICMPv6_payload_raw (byte_string_to_icmpv6_payload_raw s))
        payload_string_gen

    let icmpv6_pkt_gen : icmpv6_pkt QCheck.Gen.t =
      let open QCheck.Gen in
      map2
        (fun header payload -> ICMPv6_pkt {header; payload})
        icmpv6_header_gen icmpv6_payload_gen

    (* IPv4 *)
    let ipv4_addr_gen : ipv4_addr QCheck.Gen.t =
      let open QCheck.Gen in
      map byte_string_to_ipv4_addr (string_size (return ipv4_addr_byte_count))

    let ipv4_header_gen : ipv4_header QCheck.Gen.t =
      let open QCheck.Gen in
      map2
        (fun src_addr dst_addr ->
           make_dummy_ipv4_header () |> update_ipv4_header ~src_addr ~dst_addr
        )
        ipv4_addr_gen ipv4_addr_gen

    let ipv4_payload_gen : ipv4_payload QCheck.Gen.t =
      let open QCheck.Gen in
      frequency
        [ ( 1
          , map
              (fun s -> IPv4_payload_raw (byte_string_to_ipv4_payload_raw s))
              payload_string_gen )
        ; (1, map (fun p -> IPv4_payload_icmp p) icmpv4_pkt_gen)
        ; (2, map (fun p -> IPv4_payload_encap p) layer4_pdu_gen) ]

    let ipv4_pkt_gen : ipv4_pkt QCheck.Gen.t =
      let open QCheck.Gen in
      map2
        (fun header payload -> IPv4_pkt {header; payload})
        ipv4_header_gen ipv4_payload_gen

    (* IPv6 *)
    let ipv6_addr_gen : ipv6_addr QCheck.Gen.t =
      let open QCheck.Gen in
      map byte_string_to_ipv6_addr (string_size (return ipv6_addr_byte_count))

    let ipv6_header_gen : ipv6_header QCheck.Gen.t =
      let open QCheck.Gen in
      map2
        (fun src_addr dst_addr ->
           make_dummy_ipv6_header () |> update_ipv6_header ~src_addr ~dst_addr
        )
        ipv6_addr_gen ipv6_addr_gen

    let ipv6_payload_gen : ipv6_payload QCheck.Gen.t =
      let open QCheck.Gen in
      frequency
        [ ( 1
          , map
              (fun s -> IPv6_payload_raw (byte_string_to_ipv6_payload_raw s))
              payload_string_gen )
        ; (1, map (fun p -> IPv6_payload_icmp p) icmpv6_pkt_gen)
        ; (2, map (fun p -> IPv6_payload_encap p) layer4_pdu_gen) ]

    let ipv6_pkt_gen : ipv6_pkt QCheck.Gen.t =
      let open QCheck.Gen in
      map2
        (fun header payload -> IPv6_pkt {header; payload})
        ipv6_header_gen ipv6_payload_gen

    (* Layer 3 *)
    let layer3_pdu_gen : layer3_pdu QCheck.Gen.t =
      let open QCheck.Gen in
      oneof
        [map (fun p -> IPv4 p) ipv4_pkt_gen; map (fun p -> IPv6 p) ipv6_pkt_gen]

    (* Ethernet *)
    let ether_addr_gen : ether_addr QCheck.Gen.t =
      let open QCheck.Gen in
      map byte_string_to_ether_addr
        (string_size (return ether_addr_byte_count))

    let ether_header_gen : ether_header QCheck.Gen.t =
      let open QCheck.Gen in
      map2
        (fun src_addr dst_addr ->
           make_dummy_ether_header () |> update_ether_header ~src_addr ~dst_addr
        )
        ether_addr_gen ether_addr_gen

    let ether_payload_gen : ether_payload QCheck.Gen.t =
      let open QCheck.Gen in
      frequency
        [ ( 1
          , map
              (fun s -> Ether_payload_raw (byte_string_to_ether_payload_raw s))
              payload_string_gen )
        ; (2, map (fun p -> Ether_payload_encap p) layer3_pdu_gen) ]

    let ether_frame_gen : ether_frame QCheck.Gen.t =
      let open QCheck.Gen in
      map2
        (fun header payload -> Ether_frame {header; payload})
        ether_header_gen ether_payload_gen

    (* Layer 2 *)
    let layer2_pdu_gen : layer2_pdu QCheck.Gen.t =
      QCheck.Gen.(oneof [map (fun p -> Ether p) ether_frame_gen])

    let pdu_gen : pdu QCheck.Gen.t =
      let open QCheck.Gen in
      oneof
        [ map (fun p -> Layer2 p) layer2_pdu_gen
        ; map (fun p -> Layer3 p) layer3_pdu_gen
        ; map (fun p -> Layer4 p) layer4_pdu_gen ]

    (*$ let words =
            [ "TCP",      "tcp",     "tcp_port",    "tcp_pdu"
            ; "UDP",      "udp",     "udp_port",    "udp_pdu"
            ; "Layer 4",  "layer4",  "",            ""
            ; "IPv4",     "ipv4",    "ipv4_addr",   "ipv4_pkt"
            ; "IPv6",     "ipv6",    "ipv6_addr",   "ipv6_pkt"
            ; "ICMPv4",   "icmpv4",  "icmpv4_type", "icmpv4_pkt"
            ; "ICMPv6",   "icmpv6",  "icmpv6_type", "icmpv6_pkt"
            ; "Layer 3",  "layer3",  "",            ""
            ; "Ethernet", "ether",   "ether_addr",  "ether_frame"
            ; "Layer 2",  "layer2",  "",            ""
            ]
          in
          List.iter
            (fun (comment, proto_name, field_ty, pdu_ty) ->
               if List.mem proto_name ["layer2"; "layer3"; "layer4"] then
                 Printf.printf {|
                     (* %s *)
                     let arbitrary_%s_pdu : %s_pdu QCheck.arbitrary =
                       QCheck.make %s_pdu_gen ~print:To_debug_string.%s_pdu
                   |}
                   comment

                   proto_name proto_name
                   proto_name proto_name
               else
                 Printf.printf {|
                     (* %s *)
                     let arbitrary_%s : %s QCheck.arbitrary =
                       QCheck.make %s_gen ~print:To_debug_string.%s

                     let arbitrary_%s_header : %s_header QCheck.arbitrary =
                       QCheck.make %s_header_gen ~print:To_debug_string.%s_header

                     let arbitrary_%s_payload : %s_payload QCheck.arbitrary =
                       QCheck.make %s_payload_gen ~print:To_debug_string.%s_payload

                     let arbitrary_%s : %s QCheck.arbitrary =
                       QCheck.make %s_gen ~print:To_debug_string.%s
                   |}
                   comment

                   field_ty field_ty
                   field_ty field_ty

                   proto_name proto_name
                   proto_name proto_name

                   proto_name proto_name
                   proto_name proto_name

                   pdu_ty pdu_ty
                   pdu_ty pdu_ty
            )
            words
    *)
    (* TCP *)
    let arbitrary_tcp_port : tcp_port QCheck.arbitrary =
      QCheck.make tcp_port_gen ~print:To_debug_string.tcp_port

    let arbitrary_tcp_header : tcp_header QCheck.arbitrary =
      QCheck.make tcp_header_gen ~print:To_debug_string.tcp_header

    let arbitrary_tcp_payload : tcp_payload QCheck.arbitrary =
      QCheck.make tcp_payload_gen ~print:To_debug_string.tcp_payload

    let arbitrary_tcp_pdu : tcp_pdu QCheck.arbitrary =
      QCheck.make tcp_pdu_gen ~print:To_debug_string.tcp_pdu

    (* UDP *)
    let arbitrary_udp_port : udp_port QCheck.arbitrary =
      QCheck.make udp_port_gen ~print:To_debug_string.udp_port

    let arbitrary_udp_header : udp_header QCheck.arbitrary =
      QCheck.make udp_header_gen ~print:To_debug_string.udp_header

    let arbitrary_udp_payload : udp_payload QCheck.arbitrary =
      QCheck.make udp_payload_gen ~print:To_debug_string.udp_payload

    let arbitrary_udp_pdu : udp_pdu QCheck.arbitrary =
      QCheck.make udp_pdu_gen ~print:To_debug_string.udp_pdu

    (* Layer 4 *)
    let arbitrary_layer4_pdu : layer4_pdu QCheck.arbitrary =
      QCheck.make layer4_pdu_gen ~print:To_debug_string.layer4_pdu

    (* IPv4 *)
    let arbitrary_ipv4_addr : ipv4_addr QCheck.arbitrary =
      QCheck.make ipv4_addr_gen ~print:To_debug_string.ipv4_addr

    let arbitrary_ipv4_header : ipv4_header QCheck.arbitrary =
      QCheck.make ipv4_header_gen ~print:To_debug_string.ipv4_header

    let arbitrary_ipv4_payload : ipv4_payload QCheck.arbitrary =
      QCheck.make ipv4_payload_gen ~print:To_debug_string.ipv4_payload

    let arbitrary_ipv4_pkt : ipv4_pkt QCheck.arbitrary =
      QCheck.make ipv4_pkt_gen ~print:To_debug_string.ipv4_pkt

    (* IPv6 *)
    let arbitrary_ipv6_addr : ipv6_addr QCheck.arbitrary =
      QCheck.make ipv6_addr_gen ~print:To_debug_string.ipv6_addr

    let arbitrary_ipv6_header : ipv6_header QCheck.arbitrary =
      QCheck.make ipv6_header_gen ~print:To_debug_string.ipv6_header

    let arbitrary_ipv6_payload : ipv6_payload QCheck.arbitrary =
      QCheck.make ipv6_payload_gen ~print:To_debug_string.ipv6_payload

    let arbitrary_ipv6_pkt : ipv6_pkt QCheck.arbitrary =
      QCheck.make ipv6_pkt_gen ~print:To_debug_string.ipv6_pkt

    (* ICMPv4 *)
    let arbitrary_icmpv4_type : icmpv4_type QCheck.arbitrary =
      QCheck.make icmpv4_type_gen ~print:To_debug_string.icmpv4_type

    let arbitrary_icmpv4_header : icmpv4_header QCheck.arbitrary =
      QCheck.make icmpv4_header_gen ~print:To_debug_string.icmpv4_header

    let arbitrary_icmpv4_payload : icmpv4_payload QCheck.arbitrary =
      QCheck.make icmpv4_payload_gen ~print:To_debug_string.icmpv4_payload

    let arbitrary_icmpv4_pkt : icmpv4_pkt QCheck.arbitrary =
      QCheck.make icmpv4_pkt_gen ~print:To_debug_string.icmpv4_pkt

    (* ICMPv6 *)
    let arbitrary_icmpv6_type : icmpv6_type QCheck.arbitrary =
      QCheck.make icmpv6_type_gen ~print:To_debug_string.icmpv6_type

    let arbitrary_icmpv6_header : icmpv6_header QCheck.arbitrary =
      QCheck.make icmpv6_header_gen ~print:To_debug_string.icmpv6_header

    let arbitrary_icmpv6_payload : icmpv6_payload QCheck.arbitrary =
      QCheck.make icmpv6_payload_gen ~print:To_debug_string.icmpv6_payload

    let arbitrary_icmpv6_pkt : icmpv6_pkt QCheck.arbitrary =
      QCheck.make icmpv6_pkt_gen ~print:To_debug_string.icmpv6_pkt

    (* Layer 3 *)
    let arbitrary_layer3_pdu : layer3_pdu QCheck.arbitrary =
      QCheck.make layer3_pdu_gen ~print:To_debug_string.layer3_pdu

    (* Ethernet *)
    let arbitrary_ether_addr : ether_addr QCheck.arbitrary =
      QCheck.make ether_addr_gen ~print:To_debug_string.ether_addr

    let arbitrary_ether_header : ether_header QCheck.arbitrary =
      QCheck.make ether_header_gen ~print:To_debug_string.ether_header

    let arbitrary_ether_payload : ether_payload QCheck.arbitrary =
      QCheck.make ether_payload_gen ~print:To_debug_string.ether_payload

    let arbitrary_ether_frame : ether_frame QCheck.arbitrary =
      QCheck.make ether_frame_gen ~print:To_debug_string.ether_frame

    (* Layer 2 *)
    let arbitrary_layer2_pdu : layer2_pdu QCheck.arbitrary =
      QCheck.make layer2_pdu_gen ~print:To_debug_string.layer2_pdu

    (*$*)

    let arbitrary_pdu : pdu QCheck.arbitrary =
      QCheck.make pdu_gen ~print:To_debug_string.pdu
  end

  module Tree_base_test = struct
    open PDU_QCheck_components
    open QCheck_utils
    open T
    open T.Ether
    open T.IPv4
    open T.IPv6
    open T.ICMPv4
    open T.ICMPv6
    open T.TCP
    open T.UDP

    (*$ let words =
            [ "byte_string", "ipv4_addr"
            ; "byte_string", "ipv6_addr"
            ; "byte_string", "ether_addr"
            ]
          in
          List.iter
            (fun (ty1, ty2) ->
               Printf.printf {|
                   let qc_%s_to_%s_is_inverse_of_%s_to_%s =
                     make_qcheck_test ~name:"qc_%s_to_%s_is_inverse_of_%s_to_%s"
                       arbitrary_%s (fun x ->
                           %s_to_%s (%s_to_%s x) = x)
                 |}
                 ty1 ty2 ty2 ty1
                 ty1 ty2 ty2 ty1
                 ty2
                 ty1 ty2 ty2 ty1
            )
            words
    *)
    let qc_byte_string_to_ipv4_addr_is_inverse_of_ipv4_addr_to_byte_string =
      make_qcheck_test
        ~name:
          "qc_byte_string_to_ipv4_addr_is_inverse_of_ipv4_addr_to_byte_string"
        arbitrary_ipv4_addr (fun x ->
            byte_string_to_ipv4_addr (ipv4_addr_to_byte_string x) = x )

    let qc_byte_string_to_ipv6_addr_is_inverse_of_ipv6_addr_to_byte_string =
      make_qcheck_test
        ~name:
          "qc_byte_string_to_ipv6_addr_is_inverse_of_ipv6_addr_to_byte_string"
        arbitrary_ipv6_addr (fun x ->
            byte_string_to_ipv6_addr (ipv6_addr_to_byte_string x) = x )

    let qc_byte_string_to_ether_addr_is_inverse_of_ether_addr_to_byte_string =
      make_qcheck_test
        ~name:
          "qc_byte_string_to_ether_addr_is_inverse_of_ether_addr_to_byte_string"
        arbitrary_ether_addr (fun x ->
            byte_string_to_ether_addr (ether_addr_to_byte_string x) = x )

    (*$*)

    type tcp_header_param =
      { src_port : tcp_port
      ; src_port' : tcp_port
      ; dst_port : tcp_port
      ; dst_port' : tcp_port
      ; ack : bool
      ; ack' : bool
      ; rst : bool
      ; rst' : bool
      ; syn : bool
      ; syn' : bool
      ; fin : bool
      ; fin' : bool }

    type udp_header_param =
      { src_port : udp_port
      ; src_port' : udp_port
      ; dst_port : udp_port
      ; dst_port' : udp_port }

    let tcp_header_test_boilerplate ~name pred =
      make_qcheck_test ~name
        QCheck.(
          triple arbitrary_tcp_port arbitrary_tcp_port
            (triple bool bool (pair bool bool)))
        (fun (src_port, dst_port, (ack, rst, (syn, fin))) ->
           let header =
             make_dummy_tcp_header ()
             |> update_tcp_header ~src_port ~dst_port ~ack ~rst ~syn ~fin
           in
           let src_port' = tcp_header_to_src_port header in
           let dst_port' = tcp_header_to_dst_port header in
           let ack' = tcp_header_to_ack_flag header in
           let rst' = tcp_header_to_rst_flag header in
           let syn' = tcp_header_to_syn_flag header in
           let fin' = tcp_header_to_fin_flag header in
           pred
             { src_port
             ; src_port'
             ; dst_port
             ; dst_port'
             ; ack
             ; ack'
             ; rst
             ; rst'
             ; syn
             ; syn'
             ; fin
             ; fin' } )

    let udp_header_test_boilerplate ~name pred =
      make_qcheck_test ~name
        QCheck.(pair arbitrary_udp_port arbitrary_udp_port)
        (fun (src_port, dst_port) ->
           let header =
             make_dummy_udp_header () |> update_udp_header ~src_port ~dst_port
           in
           let src_port' = udp_header_to_src_port header in
           let dst_port' = udp_header_to_dst_port header in
           pred {src_port; src_port'; dst_port; dst_port'} )

    type 'a ip_header_param =
      { src_addr : 'a
      ; src_addr' : 'a
      ; dst_addr : 'a
      ; dst_addr' : 'a }

    type ('a, 'b) icmp_header_param =
      { ty : 'b
      ; ty' : 'b }

    let ipv4_header_test_boilerplate ~name pred =
      make_qcheck_test ~name
        QCheck.(pair arbitrary_ipv4_addr arbitrary_ipv4_addr)
        (fun (src_addr, dst_addr) ->
           let header =
             make_dummy_ipv4_header () |> update_ipv4_header ~src_addr ~dst_addr
           in
           let src_addr' = ipv4_header_to_src_addr header in
           let dst_addr' = ipv4_header_to_dst_addr header in
           pred {src_addr; src_addr'; dst_addr; dst_addr'} )

    let ipv6_header_test_boilerplate ~name pred =
      make_qcheck_test ~name
        QCheck.(pair arbitrary_ipv6_addr arbitrary_ipv6_addr)
        (fun (src_addr, dst_addr) ->
           let header =
             make_dummy_ipv6_header () |> update_ipv6_header ~src_addr ~dst_addr
           in
           let src_addr' = ipv6_header_to_src_addr header in
           let dst_addr' = ipv6_header_to_dst_addr header in
           pred {src_addr; src_addr'; dst_addr; dst_addr'} )

    let icmpv4_header_test_boilerplate ~name pred =
      make_qcheck_test ~name arbitrary_icmpv4_type (fun ty ->
          let header =
            make_dummy_icmpv4_header () |> update_icmpv4_header ~icmpv4_type:ty
          in
          let ty' = icmpv4_header_to_icmpv4_type header in
          pred {ty; ty'} )

    let icmpv6_header_test_boilerplate ~name pred =
      make_qcheck_test ~name arbitrary_icmpv6_type (fun ty ->
          let header =
            make_dummy_icmpv6_header () |> update_icmpv6_header ~icmpv6_type:ty
          in
          let ty' = icmpv6_header_to_icmpv6_type header in
          pred {ty; ty'} )

    let ether_header_test_boilerplate ~name pred =
      make_qcheck_test ~name
        QCheck.(pair arbitrary_ether_addr arbitrary_ether_addr)
        (fun (src_addr, dst_addr) ->
           let header =
             make_dummy_ether_header ()
             |> update_ether_header ~src_addr ~dst_addr
           in
           let src_addr' = ether_header_to_src_addr header in
           let dst_addr' = ether_header_to_dst_addr header in
           pred {src_addr; src_addr'; dst_addr; dst_addr'} )

    (*$ let words =
            [ "tcp", "src_port", "src_port"
            ; "tcp", "dst_port", "dst_port"
            ; "tcp", "ack_flag", "ack"
            ; "tcp", "rst_flag", "rst"
            ; "tcp", "syn_flag", "syn"
            ; "tcp", "fin_flag", "fin"
            ; "udp", "src_port", "src_port"
            ; "udp", "dst_port", "dst_port"
            ; "ipv4", "src_addr", "src_addr"
            ; "ipv4", "dst_addr", "dst_addr"
            ; "ipv6", "src_addr", "src_addr"
            ; "ipv6", "dst_addr", "dst_addr"
            ; "icmpv4", "icmpv4_type", "ty"
            ; "icmpv6", "icmpv6_type", "ty"
            ; "ether", "src_addr", "src_addr"
            ; "ether", "dst_addr", "dst_addr"
            ]
          in
          List.iter
            (fun (proto_name, field, param_field) ->
               Printf.printf {|
                   let qc_%s_header_to_%s =
                     %s_header_test_boilerplate ~name:"qc_%s_header_to_%s"
                       (fun {%s; %s'; _} -> %s = %s' )
                 |}
                 proto_name field
                 proto_name proto_name field
                 param_field param_field param_field param_field
            )
            words;
    *)
    let qc_tcp_header_to_src_port =
      tcp_header_test_boilerplate ~name:"qc_tcp_header_to_src_port"
        (fun {src_port; src_port'; _} -> src_port = src_port')

    let qc_tcp_header_to_dst_port =
      tcp_header_test_boilerplate ~name:"qc_tcp_header_to_dst_port"
        (fun {dst_port; dst_port'; _} -> dst_port = dst_port')

    let qc_tcp_header_to_ack_flag =
      tcp_header_test_boilerplate ~name:"qc_tcp_header_to_ack_flag"
        (fun {ack; ack'; _} -> ack = ack')

    let qc_tcp_header_to_rst_flag =
      tcp_header_test_boilerplate ~name:"qc_tcp_header_to_rst_flag"
        (fun {rst; rst'; _} -> rst = rst')

    let qc_tcp_header_to_syn_flag =
      tcp_header_test_boilerplate ~name:"qc_tcp_header_to_syn_flag"
        (fun {syn; syn'; _} -> syn = syn')

    let qc_tcp_header_to_fin_flag =
      tcp_header_test_boilerplate ~name:"qc_tcp_header_to_fin_flag"
        (fun {fin; fin'; _} -> fin = fin')

    let qc_udp_header_to_src_port =
      udp_header_test_boilerplate ~name:"qc_udp_header_to_src_port"
        (fun {src_port; src_port'; _} -> src_port = src_port')

    let qc_udp_header_to_dst_port =
      udp_header_test_boilerplate ~name:"qc_udp_header_to_dst_port"
        (fun {dst_port; dst_port'; _} -> dst_port = dst_port')

    let qc_ipv4_header_to_src_addr =
      ipv4_header_test_boilerplate ~name:"qc_ipv4_header_to_src_addr"
        (fun {src_addr; src_addr'; _} -> src_addr = src_addr')

    let qc_ipv4_header_to_dst_addr =
      ipv4_header_test_boilerplate ~name:"qc_ipv4_header_to_dst_addr"
        (fun {dst_addr; dst_addr'; _} -> dst_addr = dst_addr')

    let qc_ipv6_header_to_src_addr =
      ipv6_header_test_boilerplate ~name:"qc_ipv6_header_to_src_addr"
        (fun {src_addr; src_addr'; _} -> src_addr = src_addr')

    let qc_ipv6_header_to_dst_addr =
      ipv6_header_test_boilerplate ~name:"qc_ipv6_header_to_dst_addr"
        (fun {dst_addr; dst_addr'; _} -> dst_addr = dst_addr')

    let qc_icmpv4_header_to_icmpv4_type =
      icmpv4_header_test_boilerplate ~name:"qc_icmpv4_header_to_icmpv4_type"
        (fun {ty; ty'; _} -> ty = ty')

    let qc_icmpv6_header_to_icmpv6_type =
      icmpv6_header_test_boilerplate ~name:"qc_icmpv6_header_to_icmpv6_type"
        (fun {ty; ty'; _} -> ty = ty')

    let qc_ether_header_to_src_addr =
      ether_header_test_boilerplate ~name:"qc_ether_header_to_src_addr"
        (fun {src_addr; src_addr'; _} -> src_addr = src_addr')

    let qc_ether_header_to_dst_addr =
      ether_header_test_boilerplate ~name:"qc_ether_header_to_dst_addr"
        (fun {dst_addr; dst_addr'; _} -> dst_addr = dst_addr')

    (*$*)

    let qc_make_tcp_header =
      make_qcheck_test ~name:"qc_make_tcp_header"
        PDU_QCheck_components.arbitrary_tcp_header (fun header ->
            let src_port = tcp_header_to_src_port header in
            let dst_port = tcp_header_to_dst_port header in
            let ack = tcp_header_to_ack_flag header in
            let rst = tcp_header_to_rst_flag header in
            let syn = tcp_header_to_syn_flag header in
            let fin = tcp_header_to_fin_flag header in
            make_dummy_tcp_header ()
            |> update_tcp_header ~src_port ~dst_port ~ack ~rst ~syn ~fin
               = header )

    let qc_make_udp_header =
      make_qcheck_test ~name:"qc_make_udp_header"
        PDU_QCheck_components.arbitrary_udp_header (fun header ->
            let src_port = udp_header_to_src_port header in
            let dst_port = udp_header_to_dst_port header in
            make_dummy_udp_header ()
            |> update_udp_header ~src_port ~dst_port
               = header )

    let qc_make_ipv4_header =
      make_qcheck_test ~name:"qc_make_ipv4_header"
        PDU_QCheck_components.arbitrary_ipv4_header (fun header ->
            let src_addr = ipv4_header_to_src_addr header in
            let dst_addr = ipv4_header_to_dst_addr header in
            make_dummy_ipv4_header ()
            |> update_ipv4_header ~src_addr ~dst_addr
               = header )

    let qc_make_ipv6_header =
      make_qcheck_test ~name:"qc_make_ipv6_header"
        PDU_QCheck_components.arbitrary_ipv6_header (fun header ->
            let src_addr = ipv6_header_to_src_addr header in
            let dst_addr = ipv6_header_to_dst_addr header in
            make_dummy_ipv6_header ()
            |> update_ipv6_header ~src_addr ~dst_addr
               = header )

    let qc_make_icmpv4_header =
      make_qcheck_test ~name:"qc_make_icmpv4_header"
        PDU_QCheck_components.arbitrary_icmpv4_header (fun header ->
            let icmpv4_type = icmpv4_header_to_icmpv4_type header in
            make_dummy_icmpv4_header ()
            |> update_icmpv4_header ~icmpv4_type
               = header )

    let qc_make_icmpv6_header =
      make_qcheck_test ~name:"qc_make_icmpv6_header"
        PDU_QCheck_components.arbitrary_icmpv6_header (fun header ->
            let icmpv6_type = icmpv6_header_to_icmpv6_type header in
            make_dummy_icmpv6_header ()
            |> update_icmpv6_header ~icmpv6_type
               = header )

    let qc_make_ether_header =
      make_qcheck_test ~name:"qc_make_ether_header"
        PDU_QCheck_components.arbitrary_ether_header (fun header ->
            let src_addr = ether_header_to_src_addr header in
            let dst_addr = ether_header_to_dst_addr header in
            make_dummy_ether_header ()
            |> update_ether_header ~src_addr ~dst_addr
               = header )

    (*$ let inverse_test_words =
            [ "byte_string", "ipv4_addr"
            ; "byte_string", "ipv6_addr"
            ; "byte_string", "ether_addr"
            ]
          in

          let header_field_access_test_words =
            [ "tcp", "src_port"
            ; "tcp", "dst_port"
            ; "tcp", "ack_flag"
            ; "tcp", "rst_flag"
            ; "tcp", "syn_flag"
            ; "tcp", "fin_flag"
            ; "udp", "src_port"
            ; "udp", "dst_port"
            ; "ipv4", "src_addr"
            ; "ipv4", "dst_addr"
            ; "ipv6", "src_addr"
            ; "ipv6", "dst_addr"
            ; "icmpv4", "icmpv4_type"
            ; "icmpv6", "icmpv6_type"
            ; "ether", "src_addr"
            ; "ether", "dst_addr"
            ]
          in

          let make_header_test_words =
            [ "tcp"
            ; "udp"
            ; "ipv4"
            ; "ipv6"
            ; "icmpv4"
            ; "icmpv6"
            ; "ether"
            ]
          in

          print_endline "let tests = [";

          List.iter
            (fun (ty1, ty2) ->
               Printf.printf "qc_%s_to_%s_is_inverse_of_%s_to_%s;\n"
                 ty1 ty2 ty2 ty1
            )
            inverse_test_words;

          List.iter
            (fun (ty1, ty2) ->
               Printf.printf "qc_%s_header_to_%s;\n" ty1 ty2
            )
            header_field_access_test_words;

          List.iter
            (fun ty ->
               Printf.printf "qc_make_%s_header;\n" ty
            )
            make_header_test_words;

          print_endline "]"
    *)

    let tests =
      [ qc_byte_string_to_ipv4_addr_is_inverse_of_ipv4_addr_to_byte_string
      ; qc_byte_string_to_ipv6_addr_is_inverse_of_ipv6_addr_to_byte_string
      ; qc_byte_string_to_ether_addr_is_inverse_of_ether_addr_to_byte_string
      ; qc_tcp_header_to_src_port
      ; qc_tcp_header_to_dst_port
      ; qc_tcp_header_to_ack_flag
      ; qc_tcp_header_to_rst_flag
      ; qc_tcp_header_to_syn_flag
      ; qc_tcp_header_to_fin_flag
      ; qc_udp_header_to_src_port
      ; qc_udp_header_to_dst_port
      ; qc_ipv4_header_to_src_addr
      ; qc_ipv4_header_to_dst_addr
      ; qc_ipv6_header_to_src_addr
      ; qc_ipv6_header_to_dst_addr
      ; qc_icmpv4_header_to_icmpv4_type
      ; qc_icmpv6_header_to_icmpv6_type
      ; qc_ether_header_to_src_addr
      ; qc_ether_header_to_dst_addr
      ; qc_make_tcp_header
      ; qc_make_udp_header
      ; qc_make_ipv4_header
      ; qc_make_ipv6_header
      ; qc_make_icmpv4_header
      ; qc_make_icmpv6_header
      ; qc_make_ether_header ]

    (*$*)

    (*$ print_run_tests_qcheck ();

          print_run_tests_ounit2 "Tree_base tests"
    *)
    let run_tests_qcheck () =
      QCheck_utils.exit_if_not_zero
        (QCheck_runner.run_tests ~verbose:true tests)

    let run_tests_ounit2 () =
      QCheck_utils.run_qcheck_tests_as_ounit2_test ~name:"Tree_base tests"
        tests

    (*$*)
  end

  module Tree_base_extended_test = struct
    open T
    open T.PDU
    open T.Ether
    open T.IPv4
    open T.IPv6
    open T.ICMPv4
    open T.ICMPv6
    open T.TCP
    open T.UDP
    open T.Unsafe_flat_PDU
    open QCheck_utils

    let qc_unflatten_is_inverse_of_flatten =
      make_qcheck_test ~name:"qc_unflatten_is_inverse_of_flatten"
        PDU_QCheck_components.arbitrary_pdu (fun pdu ->
            unflatten (flatten pdu) = Some pdu )

    (*$ for i=2 to 4 do
            Printf.printf {|
                let qc_pdu_to_layer%d_pdu_same_as_unflatten_layer%d =
                  make_qcheck_test ~name:"qc_pdu_to_layer%d_pdu_same_as_unflatten_layer%d"
                  PDU_QCheck_components.arbitrary_pdu (fun pdu ->
                    unflatten_layer%d (flatten pdu) = PDU_to.layer%d_pdu pdu )

              |}
              i i
              i i
              i i
          done
    *)
    let qc_pdu_to_layer2_pdu_same_as_unflatten_layer2 =
      make_qcheck_test ~name:"qc_pdu_to_layer2_pdu_same_as_unflatten_layer2"
        PDU_QCheck_components.arbitrary_pdu (fun pdu ->
            unflatten_layer2 (flatten pdu) = PDU_to.layer2_pdu pdu )

    let qc_pdu_to_layer3_pdu_same_as_unflatten_layer3 =
      make_qcheck_test ~name:"qc_pdu_to_layer3_pdu_same_as_unflatten_layer3"
        PDU_QCheck_components.arbitrary_pdu (fun pdu ->
            unflatten_layer3 (flatten pdu) = PDU_to.layer3_pdu pdu )

    let qc_pdu_to_layer4_pdu_same_as_unflatten_layer4 =
      make_qcheck_test ~name:"qc_pdu_to_layer4_pdu_same_as_unflatten_layer4"
        PDU_QCheck_components.arbitrary_pdu (fun pdu ->
            unflatten_layer4 (flatten pdu) = PDU_to.layer4_pdu pdu )

    (*$*)

    (*$ for i=2 to 4 do
            Printf.printf {|
                let qc_pdu_to_layer%d_pdu =
                  make_qcheck_test ~name:"qc_pdu_to_layer%d_pdu"
                    PDU_QCheck_components.arbitrary_layer%d_pdu (fun p ->
                      PDU_to.layer%d_pdu (Layer%d p) = Some p)
              |}
              i
              i
              i
              i i
          done
    *)
    let qc_pdu_to_layer2_pdu =
      make_qcheck_test ~name:"qc_pdu_to_layer2_pdu"
        PDU_QCheck_components.arbitrary_layer2_pdu (fun p ->
            PDU_to.layer2_pdu (Layer2 p) = Some p )

    let qc_pdu_to_layer3_pdu =
      make_qcheck_test ~name:"qc_pdu_to_layer3_pdu"
        PDU_QCheck_components.arbitrary_layer3_pdu (fun p ->
            PDU_to.layer3_pdu (Layer3 p) = Some p )

    let qc_pdu_to_layer4_pdu =
      make_qcheck_test ~name:"qc_pdu_to_layer4_pdu"
        PDU_QCheck_components.arbitrary_layer4_pdu (fun p ->
            PDU_to.layer4_pdu (Layer4 p) = Some p )

    (*$*)

    (*$ let words =
            [ "ether", "ether_frame", "Layer2", "Ether", "Ether_frame"
            ; "ipv4", "ipv4_pkt", "Layer3", "IPv4", "IPv4_pkt"
            ; "ipv6", "ipv6_pkt", "Layer3", "IPv6", "IPv6_pkt"
            ; "tcp", "tcp_pdu", "Layer4", "TCP", "TCP_pdu"
            ; "udp", "udp_pdu", "Layer4", "UDP", "UDP_pdu"
            ]
          in
          List.iter
            (fun (proto_name, pdu_ty, layer_con, layer_pdu_con, pdu_con) ->
               Printf.printf {|
                   let qc_pdu_to_%s =
                     make_qcheck_test ~name:"qc_pdu_to_%s"
                       PDU_QCheck_components.arbitrary_%s (fun p ->
                           PDU_to.%s (%s (%s p)) = Some p )
                 |}
                 pdu_ty
                 pdu_ty
                 pdu_ty
                 pdu_ty layer_con layer_pdu_con;

               Printf.printf {|
                   let qc_pdu_to_%s_header =
                     make_qcheck_test ~name:"qc_pdu_to_%s_header"
                       QCheck.(
                         pair PDU_QCheck_components.arbitrary_%s_header
                           PDU_QCheck_components.arbitrary_%s_payload)
                             (fun (header, payload) ->
                                PDU_to.%s_header (%s (%s (%s {header; payload})))
                                = Some header )
                 |}
                 proto_name
                 proto_name
                 proto_name
                 proto_name
                 proto_name layer_con layer_pdu_con pdu_con;

               Printf.printf {|
                   let qc_pdu_to_%s_payload =
                     make_qcheck_test ~name:"qc_pdu_to_%s_payload"
                       QCheck.(
                         pair PDU_QCheck_components.arbitrary_%s_header
                           PDU_QCheck_components.arbitrary_%s_payload)
                             (fun (header, payload) ->
                                PDU_to.%s_payload (%s (%s (%s {header; payload})))
                                = Some payload )
                 |}
                 proto_name
                 proto_name
                 proto_name
                 proto_name
                 proto_name layer_con layer_pdu_con pdu_con;
            )
            words;
        let words =
          [ 4
          ; 6
          ]
        in
        List.iter
          (fun ver ->
             Printf.printf {|
                 let qc_pdu_to_icmpv%d_pkt =
                   make_qcheck_test ~name:"qc_pdu_to_icmpv%d_pkt"
                     QCheck.(pair
                         PDU_QCheck_components.arbitrary_ipv%d_header
                         PDU_QCheck_components.arbitrary_icmpv%d_pkt
                       )
                       (fun (header, p) ->
                       PDU_to.icmpv%d_pkt
                         (Layer3
                            (IPv%d
                               (IPv%d_pkt
                                  {header;
                                   payload = IPv%d_payload_icmp p}))) = Some p
                       )
               |}
               ver
               ver
               ver
               ver
               ver
               ver
               ver
               ver;

             Printf.printf {|
                 let qc_pdu_to_icmpv%d_header =
                   make_qcheck_test ~name:"qc_pdu_to_icmpv%d_header"
                     QCheck.(triple
                         PDU_QCheck_components.arbitrary_ipv%d_header
                         PDU_QCheck_components.arbitrary_icmpv%d_header
                         PDU_QCheck_components.arbitrary_icmpv%d_payload
                       )
                       (fun (ipv4_header, header, payload) ->
                       PDU_to.icmpv%d_header
                         (Layer3
                            (IPv%d
                               (IPv%d_pkt
                                  {header = ipv4_header;
                                   payload = IPv%d_payload_icmp (ICMPv%d_pkt {header; payload})}))) = Some header
                       )
               |}
               ver
               ver
               ver
               ver
               ver
               ver
               ver
               ver
               ver
               ver;

             Printf.printf {|
                 let qc_pdu_to_icmpv%d_payload =
                   make_qcheck_test ~name:"qc_pdu_to_icmpv%d_payload"
                     QCheck.(triple
                         PDU_QCheck_components.arbitrary_ipv%d_header
                         PDU_QCheck_components.arbitrary_icmpv%d_header
                         PDU_QCheck_components.arbitrary_icmpv%d_payload
                       )
                       (fun (ipv4_header, header, payload) ->
                       PDU_to.icmpv%d_payload
                         (Layer3
                            (IPv%d
                               (IPv%d_pkt
                                  {header = ipv4_header;
                                   payload = IPv%d_payload_icmp (ICMPv%d_pkt {header; payload})}))) = Some payload
                       )
               |}
               ver
               ver
               ver
               ver
               ver
               ver
               ver
               ver
               ver
               ver;
          )
          words
    *)
    let qc_pdu_to_ether_frame =
      make_qcheck_test ~name:"qc_pdu_to_ether_frame"
        PDU_QCheck_components.arbitrary_ether_frame (fun p ->
            PDU_to.ether_frame (Layer2 (Ether p)) = Some p )

    let qc_pdu_to_ether_header =
      make_qcheck_test ~name:"qc_pdu_to_ether_header"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_ether_header
            PDU_QCheck_components.arbitrary_ether_payload)
        (fun (header, payload) ->
           PDU_to.ether_header (Layer2 (Ether (Ether_frame {header; payload})))
           = Some header )

    let qc_pdu_to_ether_payload =
      make_qcheck_test ~name:"qc_pdu_to_ether_payload"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_ether_header
            PDU_QCheck_components.arbitrary_ether_payload)
        (fun (header, payload) ->
           PDU_to.ether_payload (Layer2 (Ether (Ether_frame {header; payload})))
           = Some payload )

    let qc_pdu_to_ipv4_pkt =
      make_qcheck_test ~name:"qc_pdu_to_ipv4_pkt"
        PDU_QCheck_components.arbitrary_ipv4_pkt (fun p ->
            PDU_to.ipv4_pkt (Layer3 (IPv4 p)) = Some p )

    let qc_pdu_to_ipv4_header =
      make_qcheck_test ~name:"qc_pdu_to_ipv4_header"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_ipv4_header
            PDU_QCheck_components.arbitrary_ipv4_payload)
        (fun (header, payload) ->
           PDU_to.ipv4_header (Layer3 (IPv4 (IPv4_pkt {header; payload})))
           = Some header )

    let qc_pdu_to_ipv4_payload =
      make_qcheck_test ~name:"qc_pdu_to_ipv4_payload"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_ipv4_header
            PDU_QCheck_components.arbitrary_ipv4_payload)
        (fun (header, payload) ->
           PDU_to.ipv4_payload (Layer3 (IPv4 (IPv4_pkt {header; payload})))
           = Some payload )

    let qc_pdu_to_ipv6_pkt =
      make_qcheck_test ~name:"qc_pdu_to_ipv6_pkt"
        PDU_QCheck_components.arbitrary_ipv6_pkt (fun p ->
            PDU_to.ipv6_pkt (Layer3 (IPv6 p)) = Some p )

    let qc_pdu_to_ipv6_header =
      make_qcheck_test ~name:"qc_pdu_to_ipv6_header"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_ipv6_header
            PDU_QCheck_components.arbitrary_ipv6_payload)
        (fun (header, payload) ->
           PDU_to.ipv6_header (Layer3 (IPv6 (IPv6_pkt {header; payload})))
           = Some header )

    let qc_pdu_to_ipv6_payload =
      make_qcheck_test ~name:"qc_pdu_to_ipv6_payload"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_ipv6_header
            PDU_QCheck_components.arbitrary_ipv6_payload)
        (fun (header, payload) ->
           PDU_to.ipv6_payload (Layer3 (IPv6 (IPv6_pkt {header; payload})))
           = Some payload )

    let qc_pdu_to_tcp_pdu =
      make_qcheck_test ~name:"qc_pdu_to_tcp_pdu"
        PDU_QCheck_components.arbitrary_tcp_pdu (fun p ->
            PDU_to.tcp_pdu (Layer4 (TCP p)) = Some p )

    let qc_pdu_to_tcp_header =
      make_qcheck_test ~name:"qc_pdu_to_tcp_header"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_tcp_header
            PDU_QCheck_components.arbitrary_tcp_payload)
        (fun (header, payload) ->
           PDU_to.tcp_header (Layer4 (TCP (TCP_pdu {header; payload})))
           = Some header )

    let qc_pdu_to_tcp_payload =
      make_qcheck_test ~name:"qc_pdu_to_tcp_payload"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_tcp_header
            PDU_QCheck_components.arbitrary_tcp_payload)
        (fun (header, payload) ->
           PDU_to.tcp_payload (Layer4 (TCP (TCP_pdu {header; payload})))
           = Some payload )

    let qc_pdu_to_udp_pdu =
      make_qcheck_test ~name:"qc_pdu_to_udp_pdu"
        PDU_QCheck_components.arbitrary_udp_pdu (fun p ->
            PDU_to.udp_pdu (Layer4 (UDP p)) = Some p )

    let qc_pdu_to_udp_header =
      make_qcheck_test ~name:"qc_pdu_to_udp_header"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_udp_header
            PDU_QCheck_components.arbitrary_udp_payload)
        (fun (header, payload) ->
           PDU_to.udp_header (Layer4 (UDP (UDP_pdu {header; payload})))
           = Some header )

    let qc_pdu_to_udp_payload =
      make_qcheck_test ~name:"qc_pdu_to_udp_payload"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_udp_header
            PDU_QCheck_components.arbitrary_udp_payload)
        (fun (header, payload) ->
           PDU_to.udp_payload (Layer4 (UDP (UDP_pdu {header; payload})))
           = Some payload )

    let qc_pdu_to_icmpv4_pkt =
      make_qcheck_test ~name:"qc_pdu_to_icmpv4_pkt"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_ipv4_header
            PDU_QCheck_components.arbitrary_icmpv4_pkt)
        (fun (header, p) ->
           PDU_to.icmpv4_pkt
             (Layer3 (IPv4 (IPv4_pkt {header; payload = IPv4_payload_icmp p})))
           = Some p )

    let qc_pdu_to_icmpv4_header =
      make_qcheck_test ~name:"qc_pdu_to_icmpv4_header"
        QCheck.(
          triple PDU_QCheck_components.arbitrary_ipv4_header
            PDU_QCheck_components.arbitrary_icmpv4_header
            PDU_QCheck_components.arbitrary_icmpv4_payload)
        (fun (ipv4_header, header, payload) ->
           PDU_to.icmpv4_header
             (Layer3
                (IPv4
                   (IPv4_pkt
                      { header = ipv4_header
                      ; payload =
                          IPv4_payload_icmp (ICMPv4_pkt {header; payload}) })))
           = Some header )

    let qc_pdu_to_icmpv4_payload =
      make_qcheck_test ~name:"qc_pdu_to_icmpv4_payload"
        QCheck.(
          triple PDU_QCheck_components.arbitrary_ipv4_header
            PDU_QCheck_components.arbitrary_icmpv4_header
            PDU_QCheck_components.arbitrary_icmpv4_payload)
        (fun (ipv4_header, header, payload) ->
           PDU_to.icmpv4_payload
             (Layer3
                (IPv4
                   (IPv4_pkt
                      { header = ipv4_header
                      ; payload =
                          IPv4_payload_icmp (ICMPv4_pkt {header; payload}) })))
           = Some payload )

    let qc_pdu_to_icmpv6_pkt =
      make_qcheck_test ~name:"qc_pdu_to_icmpv6_pkt"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_ipv6_header
            PDU_QCheck_components.arbitrary_icmpv6_pkt)
        (fun (header, p) ->
           PDU_to.icmpv6_pkt
             (Layer3 (IPv6 (IPv6_pkt {header; payload = IPv6_payload_icmp p})))
           = Some p )

    let qc_pdu_to_icmpv6_header =
      make_qcheck_test ~name:"qc_pdu_to_icmpv6_header"
        QCheck.(
          triple PDU_QCheck_components.arbitrary_ipv6_header
            PDU_QCheck_components.arbitrary_icmpv6_header
            PDU_QCheck_components.arbitrary_icmpv6_payload)
        (fun (ipv4_header, header, payload) ->
           PDU_to.icmpv6_header
             (Layer3
                (IPv6
                   (IPv6_pkt
                      { header = ipv4_header
                      ; payload =
                          IPv6_payload_icmp (ICMPv6_pkt {header; payload}) })))
           = Some header )

    let qc_pdu_to_icmpv6_payload =
      make_qcheck_test ~name:"qc_pdu_to_icmpv6_payload"
        QCheck.(
          triple PDU_QCheck_components.arbitrary_ipv6_header
            PDU_QCheck_components.arbitrary_icmpv6_header
            PDU_QCheck_components.arbitrary_icmpv6_payload)
        (fun (ipv4_header, header, payload) ->
           PDU_to.icmpv6_payload
             (Layer3
                (IPv6
                   (IPv6_pkt
                      { header = ipv4_header
                      ; payload =
                          IPv6_payload_icmp (ICMPv6_pkt {header; payload}) })))
           = Some payload )

    (*$*)

    (*$ let words =
            [ 2, "layer2_pdu"
            ; 3, "layer3_pdu"
            ; 4, "layer4_pdu"
            ; 2, "ether_frame"
            ; 2, "ether_header"
            ; 2, "ether_payload"
            ; 3, "ipv4_pkt"
            ; 3, "ipv4_header"
            ; 3, "ipv4_payload"
            ; 3, "ipv6_pkt"
            ; 3, "ipv6_header"
            ; 3, "ipv6_payload"
            ; 3, "icmpv4_pkt"
            ; 3, "icmpv4_header"
            ; 3, "icmpv4_payload"
            ; 3, "icmpv6_pkt"
            ; 3, "icmpv6_header"
            ; 3, "icmpv6_payload"
            ; 4, "tcp_pdu"
            ; 4, "tcp_header"
            ; 4, "tcp_payload"
            ; 4, "udp_pdu"
            ; 4, "udp_header"
            ; 4, "udp_payload"
            ]
          in
          let layer2_protos =
            [ "ether" ]
          in
          let layer3_protos =
            [ "ipv4"
            ; "ipv6"
            ; "icmpv4"
            ; "icmpv6"
            ]
          in
          List.iter
            (fun (layer, ty) ->
               Printf.printf {|
                   let qc_pdu_map_%s =
                     make_qcheck_test ~name:"qc_pdu_map_%s"
                     QCheck.(
                       pair PDU_QCheck_components.arbitrary_pdu
                         PDU_QCheck_components.arbitrary_%s)
                     (fun (pdu, %s) ->
                        let res =
                          PDU_map.%s (fun _ -> %s) pdu
                        in
                 |}
                 ty
                 ty
                 ty
                 ty
                 ty ty;

               if layer > 2 then
                 List.iter
                   (fun proto ->
                      Printf.printf {|
                          PDU_to.%s_header res = PDU_to.%s_header pdu
                          &&
                        |}
                      proto proto
                   )
                   layer2_protos;

               if layer > 3 then
                 List.iter
                   (fun proto ->
                      Printf.printf {|
                          PDU_to.%s_header res = PDU_to.%s_header pdu
                          &&
                        |}
                      proto proto
                   )
                   layer3_protos;

               Printf.printf {|
                      (match PDU_to.%s pdu with
                       | None -> PDU_to.%s res = None
                       | Some _ -> PDU_to.%s res = Some %s
                      )
                   )
                 |}
                 ty
                 ty
                 ty ty;
            )
            words;

          List.iter
            (fun (layer, ty) ->
               Printf.printf {|
                   let qc_pdu_replace_%s =
                     make_qcheck_test ~name:"qc_pdu_replace_%s"
                     QCheck.(
                       pair PDU_QCheck_components.arbitrary_pdu
                         PDU_QCheck_components.arbitrary_%s)
                     (fun (pdu, %s) ->
                        let res =
                          PDU_replace.%s %s pdu
                        in
                 |}
                 ty
                 ty
                 ty
                 ty
                 ty ty;

               if layer > 2 then
                 List.iter
                   (fun proto ->
                      Printf.printf {|
                          PDU_to.%s_header res = PDU_to.%s_header pdu
                          &&
                        |}
                      proto proto
                   )
                   layer2_protos;

               if layer > 3 then
                 List.iter
                   (fun proto ->
                      Printf.printf {|
                          PDU_to.%s_header res = PDU_to.%s_header pdu
                          &&
                        |}
                      proto proto
                   )
                   layer3_protos;

               Printf.printf {|
                      (match PDU_to.%s pdu with
                       | None -> PDU_to.%s res = None
                       | Some _ -> PDU_to.%s res = Some %s
                      )
                   )
                 |}
                 ty
                 ty
                 ty ty;
            )
            words;
    *)
    let qc_pdu_map_layer2_pdu =
      make_qcheck_test ~name:"qc_pdu_map_layer2_pdu"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_layer2_pdu)
        (fun (pdu, layer2_pdu) ->
           let res = PDU_map.layer2_pdu (fun _ -> layer2_pdu) pdu in
           match PDU_to.layer2_pdu pdu with
           | None ->
             PDU_to.layer2_pdu res = None
           | Some _ ->
             PDU_to.layer2_pdu res = Some layer2_pdu )

    let qc_pdu_map_layer3_pdu =
      make_qcheck_test ~name:"qc_pdu_map_layer3_pdu"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_layer3_pdu)
        (fun (pdu, layer3_pdu) ->
           let res = PDU_map.layer3_pdu (fun _ -> layer3_pdu) pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           &&
           match PDU_to.layer3_pdu pdu with
           | None ->
             PDU_to.layer3_pdu res = None
           | Some _ ->
             PDU_to.layer3_pdu res = Some layer3_pdu )

    let qc_pdu_map_layer4_pdu =
      make_qcheck_test ~name:"qc_pdu_map_layer4_pdu"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_layer4_pdu)
        (fun (pdu, layer4_pdu) ->
           let res = PDU_map.layer4_pdu (fun _ -> layer4_pdu) pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           && PDU_to.ipv4_header res = PDU_to.ipv4_header pdu
           && PDU_to.ipv6_header res = PDU_to.ipv6_header pdu
           && PDU_to.icmpv4_header res = PDU_to.icmpv4_header pdu
           && PDU_to.icmpv6_header res = PDU_to.icmpv6_header pdu
           &&
           match PDU_to.layer4_pdu pdu with
           | None ->
             PDU_to.layer4_pdu res = None
           | Some _ ->
             PDU_to.layer4_pdu res = Some layer4_pdu )

    let qc_pdu_map_ether_frame =
      make_qcheck_test ~name:"qc_pdu_map_ether_frame"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_ether_frame)
        (fun (pdu, ether_frame) ->
           let res = PDU_map.ether_frame (fun _ -> ether_frame) pdu in
           match PDU_to.ether_frame pdu with
           | None ->
             PDU_to.ether_frame res = None
           | Some _ ->
             PDU_to.ether_frame res = Some ether_frame )

    let qc_pdu_map_ether_header =
      make_qcheck_test ~name:"qc_pdu_map_ether_header"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_ether_header)
        (fun (pdu, ether_header) ->
           let res = PDU_map.ether_header (fun _ -> ether_header) pdu in
           match PDU_to.ether_header pdu with
           | None ->
             PDU_to.ether_header res = None
           | Some _ ->
             PDU_to.ether_header res = Some ether_header )

    let qc_pdu_map_ether_payload =
      make_qcheck_test ~name:"qc_pdu_map_ether_payload"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_ether_payload)
        (fun (pdu, ether_payload) ->
           let res = PDU_map.ether_payload (fun _ -> ether_payload) pdu in
           match PDU_to.ether_payload pdu with
           | None ->
             PDU_to.ether_payload res = None
           | Some _ ->
             PDU_to.ether_payload res = Some ether_payload )

    let qc_pdu_map_ipv4_pkt =
      make_qcheck_test ~name:"qc_pdu_map_ipv4_pkt"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_ipv4_pkt)
        (fun (pdu, ipv4_pkt) ->
           let res = PDU_map.ipv4_pkt (fun _ -> ipv4_pkt) pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           &&
           match PDU_to.ipv4_pkt pdu with
           | None ->
             PDU_to.ipv4_pkt res = None
           | Some _ ->
             PDU_to.ipv4_pkt res = Some ipv4_pkt )

    let qc_pdu_map_ipv4_header =
      make_qcheck_test ~name:"qc_pdu_map_ipv4_header"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_ipv4_header)
        (fun (pdu, ipv4_header) ->
           let res = PDU_map.ipv4_header (fun _ -> ipv4_header) pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           &&
           match PDU_to.ipv4_header pdu with
           | None ->
             PDU_to.ipv4_header res = None
           | Some _ ->
             PDU_to.ipv4_header res = Some ipv4_header )

    let qc_pdu_map_ipv4_payload =
      make_qcheck_test ~name:"qc_pdu_map_ipv4_payload"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_ipv4_payload)
        (fun (pdu, ipv4_payload) ->
           let res = PDU_map.ipv4_payload (fun _ -> ipv4_payload) pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           &&
           match PDU_to.ipv4_payload pdu with
           | None ->
             PDU_to.ipv4_payload res = None
           | Some _ ->
             PDU_to.ipv4_payload res = Some ipv4_payload )

    let qc_pdu_map_ipv6_pkt =
      make_qcheck_test ~name:"qc_pdu_map_ipv6_pkt"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_ipv6_pkt)
        (fun (pdu, ipv6_pkt) ->
           let res = PDU_map.ipv6_pkt (fun _ -> ipv6_pkt) pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           &&
           match PDU_to.ipv6_pkt pdu with
           | None ->
             PDU_to.ipv6_pkt res = None
           | Some _ ->
             PDU_to.ipv6_pkt res = Some ipv6_pkt )

    let qc_pdu_map_ipv6_header =
      make_qcheck_test ~name:"qc_pdu_map_ipv6_header"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_ipv6_header)
        (fun (pdu, ipv6_header) ->
           let res = PDU_map.ipv6_header (fun _ -> ipv6_header) pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           &&
           match PDU_to.ipv6_header pdu with
           | None ->
             PDU_to.ipv6_header res = None
           | Some _ ->
             PDU_to.ipv6_header res = Some ipv6_header )

    let qc_pdu_map_ipv6_payload =
      make_qcheck_test ~name:"qc_pdu_map_ipv6_payload"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_ipv6_payload)
        (fun (pdu, ipv6_payload) ->
           let res = PDU_map.ipv6_payload (fun _ -> ipv6_payload) pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           &&
           match PDU_to.ipv6_payload pdu with
           | None ->
             PDU_to.ipv6_payload res = None
           | Some _ ->
             PDU_to.ipv6_payload res = Some ipv6_payload )

    let qc_pdu_map_icmpv4_pkt =
      make_qcheck_test ~name:"qc_pdu_map_icmpv4_pkt"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_icmpv4_pkt)
        (fun (pdu, icmpv4_pkt) ->
           let res = PDU_map.icmpv4_pkt (fun _ -> icmpv4_pkt) pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           &&
           match PDU_to.icmpv4_pkt pdu with
           | None ->
             PDU_to.icmpv4_pkt res = None
           | Some _ ->
             PDU_to.icmpv4_pkt res = Some icmpv4_pkt )

    let qc_pdu_map_icmpv4_header =
      make_qcheck_test ~name:"qc_pdu_map_icmpv4_header"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_icmpv4_header)
        (fun (pdu, icmpv4_header) ->
           let res = PDU_map.icmpv4_header (fun _ -> icmpv4_header) pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           &&
           match PDU_to.icmpv4_header pdu with
           | None ->
             PDU_to.icmpv4_header res = None
           | Some _ ->
             PDU_to.icmpv4_header res = Some icmpv4_header )

    let qc_pdu_map_icmpv4_payload =
      make_qcheck_test ~name:"qc_pdu_map_icmpv4_payload"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_icmpv4_payload)
        (fun (pdu, icmpv4_payload) ->
           let res = PDU_map.icmpv4_payload (fun _ -> icmpv4_payload) pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           &&
           match PDU_to.icmpv4_payload pdu with
           | None ->
             PDU_to.icmpv4_payload res = None
           | Some _ ->
             PDU_to.icmpv4_payload res = Some icmpv4_payload )

    let qc_pdu_map_icmpv6_pkt =
      make_qcheck_test ~name:"qc_pdu_map_icmpv6_pkt"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_icmpv6_pkt)
        (fun (pdu, icmpv6_pkt) ->
           let res = PDU_map.icmpv6_pkt (fun _ -> icmpv6_pkt) pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           &&
           match PDU_to.icmpv6_pkt pdu with
           | None ->
             PDU_to.icmpv6_pkt res = None
           | Some _ ->
             PDU_to.icmpv6_pkt res = Some icmpv6_pkt )

    let qc_pdu_map_icmpv6_header =
      make_qcheck_test ~name:"qc_pdu_map_icmpv6_header"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_icmpv6_header)
        (fun (pdu, icmpv6_header) ->
           let res = PDU_map.icmpv6_header (fun _ -> icmpv6_header) pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           &&
           match PDU_to.icmpv6_header pdu with
           | None ->
             PDU_to.icmpv6_header res = None
           | Some _ ->
             PDU_to.icmpv6_header res = Some icmpv6_header )

    let qc_pdu_map_icmpv6_payload =
      make_qcheck_test ~name:"qc_pdu_map_icmpv6_payload"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_icmpv6_payload)
        (fun (pdu, icmpv6_payload) ->
           let res = PDU_map.icmpv6_payload (fun _ -> icmpv6_payload) pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           &&
           match PDU_to.icmpv6_payload pdu with
           | None ->
             PDU_to.icmpv6_payload res = None
           | Some _ ->
             PDU_to.icmpv6_payload res = Some icmpv6_payload )

    let qc_pdu_map_tcp_pdu =
      make_qcheck_test ~name:"qc_pdu_map_tcp_pdu"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_tcp_pdu)
        (fun (pdu, tcp_pdu) ->
           let res = PDU_map.tcp_pdu (fun _ -> tcp_pdu) pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           && PDU_to.ipv4_header res = PDU_to.ipv4_header pdu
           && PDU_to.ipv6_header res = PDU_to.ipv6_header pdu
           && PDU_to.icmpv4_header res = PDU_to.icmpv4_header pdu
           && PDU_to.icmpv6_header res = PDU_to.icmpv6_header pdu
           &&
           match PDU_to.tcp_pdu pdu with
           | None ->
             PDU_to.tcp_pdu res = None
           | Some _ ->
             PDU_to.tcp_pdu res = Some tcp_pdu )

    let qc_pdu_map_tcp_header =
      make_qcheck_test ~name:"qc_pdu_map_tcp_header"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_tcp_header)
        (fun (pdu, tcp_header) ->
           let res = PDU_map.tcp_header (fun _ -> tcp_header) pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           && PDU_to.ipv4_header res = PDU_to.ipv4_header pdu
           && PDU_to.ipv6_header res = PDU_to.ipv6_header pdu
           && PDU_to.icmpv4_header res = PDU_to.icmpv4_header pdu
           && PDU_to.icmpv6_header res = PDU_to.icmpv6_header pdu
           &&
           match PDU_to.tcp_header pdu with
           | None ->
             PDU_to.tcp_header res = None
           | Some _ ->
             PDU_to.tcp_header res = Some tcp_header )

    let qc_pdu_map_tcp_payload =
      make_qcheck_test ~name:"qc_pdu_map_tcp_payload"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_tcp_payload)
        (fun (pdu, tcp_payload) ->
           let res = PDU_map.tcp_payload (fun _ -> tcp_payload) pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           && PDU_to.ipv4_header res = PDU_to.ipv4_header pdu
           && PDU_to.ipv6_header res = PDU_to.ipv6_header pdu
           && PDU_to.icmpv4_header res = PDU_to.icmpv4_header pdu
           && PDU_to.icmpv6_header res = PDU_to.icmpv6_header pdu
           &&
           match PDU_to.tcp_payload pdu with
           | None ->
             PDU_to.tcp_payload res = None
           | Some _ ->
             PDU_to.tcp_payload res = Some tcp_payload )

    let qc_pdu_map_udp_pdu =
      make_qcheck_test ~name:"qc_pdu_map_udp_pdu"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_udp_pdu)
        (fun (pdu, udp_pdu) ->
           let res = PDU_map.udp_pdu (fun _ -> udp_pdu) pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           && PDU_to.ipv4_header res = PDU_to.ipv4_header pdu
           && PDU_to.ipv6_header res = PDU_to.ipv6_header pdu
           && PDU_to.icmpv4_header res = PDU_to.icmpv4_header pdu
           && PDU_to.icmpv6_header res = PDU_to.icmpv6_header pdu
           &&
           match PDU_to.udp_pdu pdu with
           | None ->
             PDU_to.udp_pdu res = None
           | Some _ ->
             PDU_to.udp_pdu res = Some udp_pdu )

    let qc_pdu_map_udp_header =
      make_qcheck_test ~name:"qc_pdu_map_udp_header"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_udp_header)
        (fun (pdu, udp_header) ->
           let res = PDU_map.udp_header (fun _ -> udp_header) pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           && PDU_to.ipv4_header res = PDU_to.ipv4_header pdu
           && PDU_to.ipv6_header res = PDU_to.ipv6_header pdu
           && PDU_to.icmpv4_header res = PDU_to.icmpv4_header pdu
           && PDU_to.icmpv6_header res = PDU_to.icmpv6_header pdu
           &&
           match PDU_to.udp_header pdu with
           | None ->
             PDU_to.udp_header res = None
           | Some _ ->
             PDU_to.udp_header res = Some udp_header )

    let qc_pdu_map_udp_payload =
      make_qcheck_test ~name:"qc_pdu_map_udp_payload"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_udp_payload)
        (fun (pdu, udp_payload) ->
           let res = PDU_map.udp_payload (fun _ -> udp_payload) pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           && PDU_to.ipv4_header res = PDU_to.ipv4_header pdu
           && PDU_to.ipv6_header res = PDU_to.ipv6_header pdu
           && PDU_to.icmpv4_header res = PDU_to.icmpv4_header pdu
           && PDU_to.icmpv6_header res = PDU_to.icmpv6_header pdu
           &&
           match PDU_to.udp_payload pdu with
           | None ->
             PDU_to.udp_payload res = None
           | Some _ ->
             PDU_to.udp_payload res = Some udp_payload )

    let qc_pdu_replace_layer2_pdu =
      make_qcheck_test ~name:"qc_pdu_replace_layer2_pdu"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_layer2_pdu)
        (fun (pdu, layer2_pdu) ->
           let res = PDU_replace.layer2_pdu layer2_pdu pdu in
           match PDU_to.layer2_pdu pdu with
           | None ->
             PDU_to.layer2_pdu res = None
           | Some _ ->
             PDU_to.layer2_pdu res = Some layer2_pdu )

    let qc_pdu_replace_layer3_pdu =
      make_qcheck_test ~name:"qc_pdu_replace_layer3_pdu"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_layer3_pdu)
        (fun (pdu, layer3_pdu) ->
           let res = PDU_replace.layer3_pdu layer3_pdu pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           &&
           match PDU_to.layer3_pdu pdu with
           | None ->
             PDU_to.layer3_pdu res = None
           | Some _ ->
             PDU_to.layer3_pdu res = Some layer3_pdu )

    let qc_pdu_replace_layer4_pdu =
      make_qcheck_test ~name:"qc_pdu_replace_layer4_pdu"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_layer4_pdu)
        (fun (pdu, layer4_pdu) ->
           let res = PDU_replace.layer4_pdu layer4_pdu pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           && PDU_to.ipv4_header res = PDU_to.ipv4_header pdu
           && PDU_to.ipv6_header res = PDU_to.ipv6_header pdu
           && PDU_to.icmpv4_header res = PDU_to.icmpv4_header pdu
           && PDU_to.icmpv6_header res = PDU_to.icmpv6_header pdu
           &&
           match PDU_to.layer4_pdu pdu with
           | None ->
             PDU_to.layer4_pdu res = None
           | Some _ ->
             PDU_to.layer4_pdu res = Some layer4_pdu )

    let qc_pdu_replace_ether_frame =
      make_qcheck_test ~name:"qc_pdu_replace_ether_frame"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_ether_frame)
        (fun (pdu, ether_frame) ->
           let res = PDU_replace.ether_frame ether_frame pdu in
           match PDU_to.ether_frame pdu with
           | None ->
             PDU_to.ether_frame res = None
           | Some _ ->
             PDU_to.ether_frame res = Some ether_frame )

    let qc_pdu_replace_ether_header =
      make_qcheck_test ~name:"qc_pdu_replace_ether_header"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_ether_header)
        (fun (pdu, ether_header) ->
           let res = PDU_replace.ether_header ether_header pdu in
           match PDU_to.ether_header pdu with
           | None ->
             PDU_to.ether_header res = None
           | Some _ ->
             PDU_to.ether_header res = Some ether_header )

    let qc_pdu_replace_ether_payload =
      make_qcheck_test ~name:"qc_pdu_replace_ether_payload"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_ether_payload)
        (fun (pdu, ether_payload) ->
           let res = PDU_replace.ether_payload ether_payload pdu in
           match PDU_to.ether_payload pdu with
           | None ->
             PDU_to.ether_payload res = None
           | Some _ ->
             PDU_to.ether_payload res = Some ether_payload )

    let qc_pdu_replace_ipv4_pkt =
      make_qcheck_test ~name:"qc_pdu_replace_ipv4_pkt"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_ipv4_pkt)
        (fun (pdu, ipv4_pkt) ->
           let res = PDU_replace.ipv4_pkt ipv4_pkt pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           &&
           match PDU_to.ipv4_pkt pdu with
           | None ->
             PDU_to.ipv4_pkt res = None
           | Some _ ->
             PDU_to.ipv4_pkt res = Some ipv4_pkt )

    let qc_pdu_replace_ipv4_header =
      make_qcheck_test ~name:"qc_pdu_replace_ipv4_header"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_ipv4_header)
        (fun (pdu, ipv4_header) ->
           let res = PDU_replace.ipv4_header ipv4_header pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           &&
           match PDU_to.ipv4_header pdu with
           | None ->
             PDU_to.ipv4_header res = None
           | Some _ ->
             PDU_to.ipv4_header res = Some ipv4_header )

    let qc_pdu_replace_ipv4_payload =
      make_qcheck_test ~name:"qc_pdu_replace_ipv4_payload"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_ipv4_payload)
        (fun (pdu, ipv4_payload) ->
           let res = PDU_replace.ipv4_payload ipv4_payload pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           &&
           match PDU_to.ipv4_payload pdu with
           | None ->
             PDU_to.ipv4_payload res = None
           | Some _ ->
             PDU_to.ipv4_payload res = Some ipv4_payload )

    let qc_pdu_replace_ipv6_pkt =
      make_qcheck_test ~name:"qc_pdu_replace_ipv6_pkt"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_ipv6_pkt)
        (fun (pdu, ipv6_pkt) ->
           let res = PDU_replace.ipv6_pkt ipv6_pkt pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           &&
           match PDU_to.ipv6_pkt pdu with
           | None ->
             PDU_to.ipv6_pkt res = None
           | Some _ ->
             PDU_to.ipv6_pkt res = Some ipv6_pkt )

    let qc_pdu_replace_ipv6_header =
      make_qcheck_test ~name:"qc_pdu_replace_ipv6_header"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_ipv6_header)
        (fun (pdu, ipv6_header) ->
           let res = PDU_replace.ipv6_header ipv6_header pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           &&
           match PDU_to.ipv6_header pdu with
           | None ->
             PDU_to.ipv6_header res = None
           | Some _ ->
             PDU_to.ipv6_header res = Some ipv6_header )

    let qc_pdu_replace_ipv6_payload =
      make_qcheck_test ~name:"qc_pdu_replace_ipv6_payload"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_ipv6_payload)
        (fun (pdu, ipv6_payload) ->
           let res = PDU_replace.ipv6_payload ipv6_payload pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           &&
           match PDU_to.ipv6_payload pdu with
           | None ->
             PDU_to.ipv6_payload res = None
           | Some _ ->
             PDU_to.ipv6_payload res = Some ipv6_payload )

    let qc_pdu_replace_icmpv4_pkt =
      make_qcheck_test ~name:"qc_pdu_replace_icmpv4_pkt"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_icmpv4_pkt)
        (fun (pdu, icmpv4_pkt) ->
           let res = PDU_replace.icmpv4_pkt icmpv4_pkt pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           &&
           match PDU_to.icmpv4_pkt pdu with
           | None ->
             PDU_to.icmpv4_pkt res = None
           | Some _ ->
             PDU_to.icmpv4_pkt res = Some icmpv4_pkt )

    let qc_pdu_replace_icmpv4_header =
      make_qcheck_test ~name:"qc_pdu_replace_icmpv4_header"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_icmpv4_header)
        (fun (pdu, icmpv4_header) ->
           let res = PDU_replace.icmpv4_header icmpv4_header pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           &&
           match PDU_to.icmpv4_header pdu with
           | None ->
             PDU_to.icmpv4_header res = None
           | Some _ ->
             PDU_to.icmpv4_header res = Some icmpv4_header )

    let qc_pdu_replace_icmpv4_payload =
      make_qcheck_test ~name:"qc_pdu_replace_icmpv4_payload"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_icmpv4_payload)
        (fun (pdu, icmpv4_payload) ->
           let res = PDU_replace.icmpv4_payload icmpv4_payload pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           &&
           match PDU_to.icmpv4_payload pdu with
           | None ->
             PDU_to.icmpv4_payload res = None
           | Some _ ->
             PDU_to.icmpv4_payload res = Some icmpv4_payload )

    let qc_pdu_replace_icmpv6_pkt =
      make_qcheck_test ~name:"qc_pdu_replace_icmpv6_pkt"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_icmpv6_pkt)
        (fun (pdu, icmpv6_pkt) ->
           let res = PDU_replace.icmpv6_pkt icmpv6_pkt pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           &&
           match PDU_to.icmpv6_pkt pdu with
           | None ->
             PDU_to.icmpv6_pkt res = None
           | Some _ ->
             PDU_to.icmpv6_pkt res = Some icmpv6_pkt )

    let qc_pdu_replace_icmpv6_header =
      make_qcheck_test ~name:"qc_pdu_replace_icmpv6_header"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_icmpv6_header)
        (fun (pdu, icmpv6_header) ->
           let res = PDU_replace.icmpv6_header icmpv6_header pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           &&
           match PDU_to.icmpv6_header pdu with
           | None ->
             PDU_to.icmpv6_header res = None
           | Some _ ->
             PDU_to.icmpv6_header res = Some icmpv6_header )

    let qc_pdu_replace_icmpv6_payload =
      make_qcheck_test ~name:"qc_pdu_replace_icmpv6_payload"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_icmpv6_payload)
        (fun (pdu, icmpv6_payload) ->
           let res = PDU_replace.icmpv6_payload icmpv6_payload pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           &&
           match PDU_to.icmpv6_payload pdu with
           | None ->
             PDU_to.icmpv6_payload res = None
           | Some _ ->
             PDU_to.icmpv6_payload res = Some icmpv6_payload )

    let qc_pdu_replace_tcp_pdu =
      make_qcheck_test ~name:"qc_pdu_replace_tcp_pdu"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_tcp_pdu)
        (fun (pdu, tcp_pdu) ->
           let res = PDU_replace.tcp_pdu tcp_pdu pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           && PDU_to.ipv4_header res = PDU_to.ipv4_header pdu
           && PDU_to.ipv6_header res = PDU_to.ipv6_header pdu
           && PDU_to.icmpv4_header res = PDU_to.icmpv4_header pdu
           && PDU_to.icmpv6_header res = PDU_to.icmpv6_header pdu
           &&
           match PDU_to.tcp_pdu pdu with
           | None ->
             PDU_to.tcp_pdu res = None
           | Some _ ->
             PDU_to.tcp_pdu res = Some tcp_pdu )

    let qc_pdu_replace_tcp_header =
      make_qcheck_test ~name:"qc_pdu_replace_tcp_header"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_tcp_header)
        (fun (pdu, tcp_header) ->
           let res = PDU_replace.tcp_header tcp_header pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           && PDU_to.ipv4_header res = PDU_to.ipv4_header pdu
           && PDU_to.ipv6_header res = PDU_to.ipv6_header pdu
           && PDU_to.icmpv4_header res = PDU_to.icmpv4_header pdu
           && PDU_to.icmpv6_header res = PDU_to.icmpv6_header pdu
           &&
           match PDU_to.tcp_header pdu with
           | None ->
             PDU_to.tcp_header res = None
           | Some _ ->
             PDU_to.tcp_header res = Some tcp_header )

    let qc_pdu_replace_tcp_payload =
      make_qcheck_test ~name:"qc_pdu_replace_tcp_payload"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_tcp_payload)
        (fun (pdu, tcp_payload) ->
           let res = PDU_replace.tcp_payload tcp_payload pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           && PDU_to.ipv4_header res = PDU_to.ipv4_header pdu
           && PDU_to.ipv6_header res = PDU_to.ipv6_header pdu
           && PDU_to.icmpv4_header res = PDU_to.icmpv4_header pdu
           && PDU_to.icmpv6_header res = PDU_to.icmpv6_header pdu
           &&
           match PDU_to.tcp_payload pdu with
           | None ->
             PDU_to.tcp_payload res = None
           | Some _ ->
             PDU_to.tcp_payload res = Some tcp_payload )

    let qc_pdu_replace_udp_pdu =
      make_qcheck_test ~name:"qc_pdu_replace_udp_pdu"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_udp_pdu)
        (fun (pdu, udp_pdu) ->
           let res = PDU_replace.udp_pdu udp_pdu pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           && PDU_to.ipv4_header res = PDU_to.ipv4_header pdu
           && PDU_to.ipv6_header res = PDU_to.ipv6_header pdu
           && PDU_to.icmpv4_header res = PDU_to.icmpv4_header pdu
           && PDU_to.icmpv6_header res = PDU_to.icmpv6_header pdu
           &&
           match PDU_to.udp_pdu pdu with
           | None ->
             PDU_to.udp_pdu res = None
           | Some _ ->
             PDU_to.udp_pdu res = Some udp_pdu )

    let qc_pdu_replace_udp_header =
      make_qcheck_test ~name:"qc_pdu_replace_udp_header"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_udp_header)
        (fun (pdu, udp_header) ->
           let res = PDU_replace.udp_header udp_header pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           && PDU_to.ipv4_header res = PDU_to.ipv4_header pdu
           && PDU_to.ipv6_header res = PDU_to.ipv6_header pdu
           && PDU_to.icmpv4_header res = PDU_to.icmpv4_header pdu
           && PDU_to.icmpv6_header res = PDU_to.icmpv6_header pdu
           &&
           match PDU_to.udp_header pdu with
           | None ->
             PDU_to.udp_header res = None
           | Some _ ->
             PDU_to.udp_header res = Some udp_header )

    let qc_pdu_replace_udp_payload =
      make_qcheck_test ~name:"qc_pdu_replace_udp_payload"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_pdu
            PDU_QCheck_components.arbitrary_udp_payload)
        (fun (pdu, udp_payload) ->
           let res = PDU_replace.udp_payload udp_payload pdu in
           PDU_to.ether_header res = PDU_to.ether_header pdu
           && PDU_to.ipv4_header res = PDU_to.ipv4_header pdu
           && PDU_to.ipv6_header res = PDU_to.ipv6_header pdu
           && PDU_to.icmpv4_header res = PDU_to.icmpv4_header pdu
           && PDU_to.icmpv6_header res = PDU_to.icmpv6_header pdu
           &&
           match PDU_to.udp_payload pdu with
           | None ->
             PDU_to.udp_payload res = None
           | Some _ ->
             PDU_to.udp_payload res = Some udp_payload )

    (*$*)

    (*$ let words =
            [ "ether",  [ "src_addr", None, "ether_addr", true
                        ; "dst_addr", None, "ether_addr", true
                        ]
            ; "ipv4",   [ "src_addr", None, "ipv4_addr", true
                        ; "dst_addr", None, "ipv4_addr", true
                        ]
            ; "ipv6",   [ "src_addr", None, "ipv6_addr", true
                        ; "dst_addr", None, "ipv6_addr", true
                        ]
            ; "icmpv4", [ "icmpv4_type", None, "icmpv4_type", false
                        ]
            ; "icmpv6", [ "icmpv6_type", None, "icmpv6_type", false
                        ]
            ; "tcp",    [ "src_port", None, "tcp_port", true
                        ; "dst_port", None, "tcp_port", true
                        ; "ack", Some "ack_flag", "bool", true
                        ; "rst", Some "rst_flag", "bool", true
                        ; "syn", Some "syn_flag", "bool", true
                        ; "fin", Some "fin_flag", "bool", true
                        ]
            ; "udp",    [ "src_port", None, "udp_port", true
                        ; "dst_port", None, "udp_port", true
                        ]
            ]
          in
          List.iter
            (fun (proto, fields) ->
               Printf.printf {|
                   let qc_update_%s_header =
                     make_qcheck_test ~name:"qc_update_%s_header"
                     QCheck.(
                       pair PDU_QCheck_components.arbitrary_%s_header
                         PDU_QCheck_components.arbitrary_%s_header
                     )
                     (fun (h1, h2) ->
                 |}
                 proto
                 proto
                 proto
                 proto;

              List.iter
                (fun (field_name, field_accessor_name, _, _) ->
                   Printf.printf {|
                       let %s = %s_header_to_%s h2 in
                     |}
                     field_name proto (match field_accessor_name with | Some x -> x | None -> field_name);
                )
                fields;

              Printf.printf {|
                  let changed_h1 = update_%s_header
                |}
                proto;

              List.iter
                (fun (field_name, _, _, _) ->
                   Printf.printf {|
                       ~%s
                     |}
                     field_name;
                )
                fields;

              Printf.printf {|
                    h1
                  in
                  let unchanged_h1 = update_%s_header h1 in
                  unchanged_h1 = h1
                  && changed_h1 = h2
                  )
                |}
                proto;
            )
            words
    *)
    let qc_update_ether_header =
      make_qcheck_test ~name:"qc_update_ether_header"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_ether_header
            PDU_QCheck_components.arbitrary_ether_header)
        (fun (h1, h2) ->
           let src_addr = ether_header_to_src_addr h2 in
           let dst_addr = ether_header_to_dst_addr h2 in
           let changed_h1 = update_ether_header ~src_addr ~dst_addr h1 in
           let unchanged_h1 = update_ether_header h1 in
           unchanged_h1 = h1 && changed_h1 = h2 )

    let qc_update_ipv4_header =
      make_qcheck_test ~name:"qc_update_ipv4_header"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_ipv4_header
            PDU_QCheck_components.arbitrary_ipv4_header)
        (fun (h1, h2) ->
           let src_addr = ipv4_header_to_src_addr h2 in
           let dst_addr = ipv4_header_to_dst_addr h2 in
           let changed_h1 = update_ipv4_header ~src_addr ~dst_addr h1 in
           let unchanged_h1 = update_ipv4_header h1 in
           unchanged_h1 = h1 && changed_h1 = h2 )

    let qc_update_ipv6_header =
      make_qcheck_test ~name:"qc_update_ipv6_header"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_ipv6_header
            PDU_QCheck_components.arbitrary_ipv6_header)
        (fun (h1, h2) ->
           let src_addr = ipv6_header_to_src_addr h2 in
           let dst_addr = ipv6_header_to_dst_addr h2 in
           let changed_h1 = update_ipv6_header ~src_addr ~dst_addr h1 in
           let unchanged_h1 = update_ipv6_header h1 in
           unchanged_h1 = h1 && changed_h1 = h2 )

    let qc_update_icmpv4_header =
      make_qcheck_test ~name:"qc_update_icmpv4_header"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_icmpv4_header
            PDU_QCheck_components.arbitrary_icmpv4_header)
        (fun (h1, h2) ->
           let icmpv4_type = icmpv4_header_to_icmpv4_type h2 in
           let changed_h1 = update_icmpv4_header ~icmpv4_type h1 in
           let unchanged_h1 = update_icmpv4_header h1 in
           unchanged_h1 = h1 && changed_h1 = h2 )

    let qc_update_icmpv6_header =
      make_qcheck_test ~name:"qc_update_icmpv6_header"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_icmpv6_header
            PDU_QCheck_components.arbitrary_icmpv6_header)
        (fun (h1, h2) ->
           let icmpv6_type = icmpv6_header_to_icmpv6_type h2 in
           let changed_h1 = update_icmpv6_header ~icmpv6_type h1 in
           let unchanged_h1 = update_icmpv6_header h1 in
           unchanged_h1 = h1 && changed_h1 = h2 )

    let qc_update_tcp_header =
      make_qcheck_test ~name:"qc_update_tcp_header"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_tcp_header
            PDU_QCheck_components.arbitrary_tcp_header)
        (fun (h1, h2) ->
           let src_port = tcp_header_to_src_port h2 in
           let dst_port = tcp_header_to_dst_port h2 in
           let ack = tcp_header_to_ack_flag h2 in
           let rst = tcp_header_to_rst_flag h2 in
           let syn = tcp_header_to_syn_flag h2 in
           let fin = tcp_header_to_fin_flag h2 in
           let changed_h1 =
             update_tcp_header ~src_port ~dst_port ~ack ~rst ~syn ~fin h1
           in
           let unchanged_h1 = update_tcp_header h1 in
           unchanged_h1 = h1 && changed_h1 = h2 )

    let qc_update_udp_header =
      make_qcheck_test ~name:"qc_update_udp_header"
        QCheck.(
          pair PDU_QCheck_components.arbitrary_udp_header
            PDU_QCheck_components.arbitrary_udp_header)
        (fun (h1, h2) ->
           let src_port = udp_header_to_src_port h2 in
           let dst_port = udp_header_to_dst_port h2 in
           let changed_h1 = update_udp_header ~src_port ~dst_port h1 in
           let unchanged_h1 = update_udp_header h1 in
           unchanged_h1 = h1 && changed_h1 = h2 )

    (*$*)

    (*$ let words =
            [ "layer2_pdu"
            ; "layer3_pdu"
            ; "layer4_pdu"
            ; "ether_frame"
            ; "ether_header"
            ; "ether_payload"
            ; "ipv4_pkt"
            ; "ipv4_header"
            ; "ipv4_payload"
            ; "ipv6_pkt"
            ; "ipv6_header"
            ; "ipv6_payload"
            ; "icmpv4_pkt"
            ; "icmpv4_header"
            ; "icmpv4_payload"
            ; "icmpv6_pkt"
            ; "icmpv6_header"
            ; "icmpv6_payload"
            ; "tcp_pdu"
            ; "tcp_header"
            ; "tcp_payload"
            ; "udp_pdu"
            ; "udp_header"
            ; "udp_payload"
            ]
          in

          print_endline "let tests = [";

          print_endline "qc_unflatten_is_inverse_of_flatten;";
          for i=2 to 4 do
            Printf.printf "qc_pdu_to_layer%d_pdu_same_as_unflatten_layer%d;\n" i i
          done;
          List.iter
            (fun ty ->
               Printf.printf "qc_pdu_to_%s;\n" ty
            )
            words;
          List.iter
            (fun ty ->
               Printf.printf "qc_pdu_map_%s;\n" ty
            )
            words;
          List.iter
            (fun ty ->
               Printf.printf "qc_pdu_replace_%s;\n" ty
            )
            words;

          let words =
            [ "ether"
            ; "ipv4"
            ; "ipv6"
            ; "icmpv4"
            ; "icmpv6"
            ; "tcp"
            ; "udp"
            ]
          in

          List.iter
            (fun proto ->
               Printf.printf "qc_update_%s_header;\n" proto
            )
            words;

          print_endline "]"
    *)

    let tests =
      [ qc_unflatten_is_inverse_of_flatten
      ; qc_pdu_to_layer2_pdu_same_as_unflatten_layer2
      ; qc_pdu_to_layer3_pdu_same_as_unflatten_layer3
      ; qc_pdu_to_layer4_pdu_same_as_unflatten_layer4
      ; qc_pdu_to_layer2_pdu
      ; qc_pdu_to_layer3_pdu
      ; qc_pdu_to_layer4_pdu
      ; qc_pdu_to_ether_frame
      ; qc_pdu_to_ether_header
      ; qc_pdu_to_ether_payload
      ; qc_pdu_to_ipv4_pkt
      ; qc_pdu_to_ipv4_header
      ; qc_pdu_to_ipv4_payload
      ; qc_pdu_to_ipv6_pkt
      ; qc_pdu_to_ipv6_header
      ; qc_pdu_to_ipv6_payload
      ; qc_pdu_to_icmpv4_pkt
      ; qc_pdu_to_icmpv4_header
      ; qc_pdu_to_icmpv4_payload
      ; qc_pdu_to_icmpv6_pkt
      ; qc_pdu_to_icmpv6_header
      ; qc_pdu_to_icmpv6_payload
      ; qc_pdu_to_tcp_pdu
      ; qc_pdu_to_tcp_header
      ; qc_pdu_to_tcp_payload
      ; qc_pdu_to_udp_pdu
      ; qc_pdu_to_udp_header
      ; qc_pdu_to_udp_payload
      ; qc_pdu_map_layer2_pdu
      ; qc_pdu_map_layer3_pdu
      ; qc_pdu_map_layer4_pdu
      ; qc_pdu_map_ether_frame
      ; qc_pdu_map_ether_header
      ; qc_pdu_map_ether_payload
      ; qc_pdu_map_ipv4_pkt
      ; qc_pdu_map_ipv4_header
      ; qc_pdu_map_ipv4_payload
      ; qc_pdu_map_ipv6_pkt
      ; qc_pdu_map_ipv6_header
      ; qc_pdu_map_ipv6_payload
      ; qc_pdu_map_icmpv4_pkt
      ; qc_pdu_map_icmpv4_header
      ; qc_pdu_map_icmpv4_payload
      ; qc_pdu_map_icmpv6_pkt
      ; qc_pdu_map_icmpv6_header
      ; qc_pdu_map_icmpv6_payload
      ; qc_pdu_map_tcp_pdu
      ; qc_pdu_map_tcp_header
      ; qc_pdu_map_tcp_payload
      ; qc_pdu_map_udp_pdu
      ; qc_pdu_map_udp_header
      ; qc_pdu_map_udp_payload
      ; qc_pdu_replace_layer2_pdu
      ; qc_pdu_replace_layer3_pdu
      ; qc_pdu_replace_layer4_pdu
      ; qc_pdu_replace_ether_frame
      ; qc_pdu_replace_ether_header
      ; qc_pdu_replace_ether_payload
      ; qc_pdu_replace_ipv4_pkt
      ; qc_pdu_replace_ipv4_header
      ; qc_pdu_replace_ipv4_payload
      ; qc_pdu_replace_ipv6_pkt
      ; qc_pdu_replace_ipv6_header
      ; qc_pdu_replace_ipv6_payload
      ; qc_pdu_replace_icmpv4_pkt
      ; qc_pdu_replace_icmpv4_header
      ; qc_pdu_replace_icmpv4_payload
      ; qc_pdu_replace_icmpv6_pkt
      ; qc_pdu_replace_icmpv6_header
      ; qc_pdu_replace_icmpv6_payload
      ; qc_pdu_replace_tcp_pdu
      ; qc_pdu_replace_tcp_header
      ; qc_pdu_replace_tcp_payload
      ; qc_pdu_replace_udp_pdu
      ; qc_pdu_replace_udp_header
      ; qc_pdu_replace_udp_payload
      ; qc_update_ether_header
      ; qc_update_ipv4_header
      ; qc_update_ipv6_header
      ; qc_update_icmpv4_header
      ; qc_update_icmpv6_header
      ; qc_update_tcp_header
      ; qc_update_udp_header ]

    (*$*)

    (*$ print_run_tests_qcheck ();

          print_run_tests_ounit2 "Tree_base_extended tests"
    *)
    let run_tests_qcheck () =
      QCheck_utils.exit_if_not_zero
        (QCheck_runner.run_tests ~verbose:true tests)

    let run_tests_ounit2 () =
      QCheck_utils.run_qcheck_tests_as_ounit2_test
        ~name:"Tree_base_extended tests" tests

    (*$*)
  end
end
